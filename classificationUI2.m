%% AI(omics)^n - a versatile tool for assessing data set characteristics, data-fusion and multi-method modelling of large data sets
% Version 2.0
%
% Developed by Felix Ruell under the supervision of Stephan Schwarzinger
% North Bavarian NMR-Centre at the University of Bayreuth
% Universitaetsstrasse 30, 95447 Bayreuth, Germany
% Contact:  s.schwarzinger@uni-bayreuth.de
% With contributions by Peter Kolb, Stefan Bindereif, Simon Steidele,
% Sandra Haupt and kind support by Birk Schuetz (Bruker BioSpin).
% (c) 2023, 2024 by the authors.
%
% License: Creative Commons CC BY-NC-SA; in case of loophole GNU-GPL for non-commercial
% applications only and with the extension that reference must be made to the original work 
% and any resulting work is relicensed alike. 
% For any commercial application a license must be obtained. For this purpose, please contact NBNC@uni-bayreuth.de
%
% The development of the software and the associated demonstration data set was in part supported by funds 
% of the Federal Ministry of Food and Agriculture (BMEL) based on a decision of the Parliament of the Federal Republic of Germany 
% via the Federal Office for Agriculture and Food (BLE) under the innovation support program (Reference number: 2816502414). 
%
% Script for classification User Interface

%% Adding toolboxes
addpath('./aiomicsn_toolbox2');

%% Collecting data
fprintf("Collecting data\r\n");
if exist('firstRun') ~= 1
    firstRun = true;
end

if exist('pathTableGroups') ~= 1
    pathTableGroups = input('Path to table with group assignments:\n', 's');
end
if exist('cloumnGroups') ~= 1
    cloumnGroups = input('Column number with group assignments:\n');
end
if exist('columnGroupsBarcode') ~= 1
    columnGroupsBarcode = input('Column number with barcode for group assignments:\n');
end

if firstRun
    sources = dataCollect();
    firstRun = false;
end

if exist('binaryBoolean') ~= 1
    binaryBoolean = input('Perform binary classification?\n');
end
if binaryBoolean && exist('binaryTrue') ~= 1
    binaryTrue = input('Group value for true:\n', 's');
end

if exist('textBoolean') ~= 1
    textBoolean = input('Show label of data points?\n');
end
if textBoolean
    textString = 'text';
else
    textString = 'keinText';
end

if exist('classificationMethod') ~= 1
    classificationMethod = input('Classification method (1: PCA/EDC, 2: PCA/LDA, 3: PCA/QDA, 4: PCA/KNN, 5: PLS-DA, 6: OPLS-DA, 7: EDC, 8: LDA, 9: QDA, 10: KNN, 11: try all)\n');
end

if classificationMethod == 11 && exist('selectionMethod') ~= 1
    selectionMethod = input('Selection method for best model (1: accuracy, 2: Matthews correlation coefficient, 3: Cohen''s kappa, 4: FP Fraction, 5: FN Fraction)\n');
end

%% Reading data
fprintf("Reading data\r\n");
[barcodeGroups,groups] = groupsRead(pathTableGroups,cloumnGroups,columnGroupsBarcode);
[origin,barcodeData,data,barcodeDataTest,dataTest,originInterval] = dataRead(sources);

%% Cleaning data
fprintf("Cleaning data\r\n");
[groupsCleaned,dataCleaned,barcodeDataCleaned] = groupsClean(groups,barcodeGroups,data,barcodeData);
[dataTestCleaned,barcodeDataTestCleaned] = dataClean(dataTest,barcodeDataTest);

%% Perform binary classification
if binaryBoolean
    groupsCleaned = string(groupsCleaned == binaryTrue);
end

%% Initialize stats;
stats = struct;

%% Apply preprocessing
[dataProcessed, stats] = preProcessing(dataCleaned, stats);
[dataTestProcessed, stats] = preProcessing(dataTestCleaned, stats);

%% Loop for removal of outliers
continueBoolean = true;
clearvars statsOld;

while continueBoolean
    %% Classification
    fprintf("Performing classification\r\n");
    switch classificationMethod
        case 1
            fprintf("Method: PCA/EDC\r\n");
            [stats,model] = classificationPca(dataProcessed,groupsCleaned,stats,'plot','edc',textString);
            [groupsTest,groupsTestProbability,stats] = classificationPcaPredict(model,stats,dataTestProcessed);
            fprintf("Method: PCA/EDC\r\n");
        case 2
            fprintf("Method: PCA/LDA\r\n");
            [stats,model] = classificationPca(dataProcessed,groupsCleaned,stats,'plot','lda',textString);
            [groupsTest,groupsTestProbability,stats] = classificationPcaPredict(model,stats,dataTestProcessed);
            fprintf("Method: PCA/LDA\r\n");
        case 3
            fprintf("Method: PCA/QDA\r\n");
            [stats,model] = classificationPca(dataProcessed,groupsCleaned,stats,'plot','qda',textString);
            [groupsTest,groupsTestProbability,stats] = classificationPcaPredict(model,stats,dataTestProcessed);
            fprintf("Method: PCA/QDA\r\n");
        case 4
            fprintf("Method: PCA/KNN\r\n");
            [stats,model] = classificationPca(dataProcessed,groupsCleaned,stats,'plot','knn',textString);
            [groupsTest,groupsTestProbability,stats] = classificationPcaPredict(model,stats,dataTestProcessed);
            fprintf("Method: PCA/KNN\r\n"); 
        case 5
            fprintf("Method: PLS-DA\r\n");
            [stats,model] = classificationPlsDa(dataProcessed,groupsCleaned,stats,'plot',textString);
            [groupsTest,groupsTestProbability,stats] = classificationPlsDaPredict(model,stats,dataTestProcessed);
            fprintf("Method: PLS-DA\r\n");
        case 6
            fprintf("Method: OPLS-DA\r\n");
            [stats,model] = classificationPlsDa(dataProcessed,groupsCleaned,stats,'plot','opls',textString);
            [groupsTest,groupsTestProbability,stats] = classificationPlsDaPredict(model,stats,dataTestProcessed);
            fprintf("Method: OPLS-DA\r\n");
        case 7
            fprintf("Method: EDC (raw data)\r\n");
            [stats,model] = classificationRaw(dataProcessed,groupsCleaned,stats,'plot','edc',textString);
            [groupsTest,groupsTestProbability,stats] = classificationRawPredict(model,stats,dataTestProcessed);
            fprintf("Method: EDC (raw data)\r\n");
        case 8
            fprintf("Method: LDA\r\n");
            [stats,model] = classificationRaw(dataProcessed,groupsCleaned,stats,'plot','lda',textString);
            [groupsTest,groupsTestProbability,stats] = classificationRawPredict(model,stats,dataTestProcessed);
            fprintf("Method: LDA\r\n");
        case 9
            fprintf("Method: QDA\r\n");
            [stats,model] = classificationRaw(dataProcessed,groupsCleaned,stats,'plot','qda',textString);
            [groupsTest,groupsTestProbability,stats] = classificationRawPredict(model,stats,dataTestProcessed);
            fprintf("Method: QDA\r\n");
        case 10
            fprintf("Method: KNN\r\n");
            [stats,model] = classificationRaw(dataProcessed,groupsCleaned,stats,'plot','knn',textString);
            [groupsTest,groupsTestProbability,stats] = classificationRawPredict(model,stats,dataTestProcessed);
            fprintf("Method: KNN\r\n");
        case 11
            % Try all models and save to cell
            models = cell(10,2);
            for j = 1:10
                fprintf("*********************************** Starting METHOD: " + num2str(j));
                switch j
                    case 1 
                        [stats,model] = classificationPca(dataProcessed,groupsCleaned,stats,textString);
                    case 2
                        [stats,model] = classificationPca(dataProcessed,groupsCleaned,stats,'lda',textString);
                    case 3
                        [stats,model] = classificationPca(dataProcessed,groupsCleaned,stats,'qda',textString);
                    case 4
                        [stats,model] = classificationPca(dataProcessed,groupsCleaned,stats,'knn',textString);
                    case 5
                        [stats,model] = classificationPlsDa(dataProcessed,groupsCleaned,stats,textString);
                    case 6
                        [stats,model] = classificationPlsDa(dataProcessed,groupsCleaned,stats,'opls',textString);
                    case 7
                        [stats,model] = classificationRaw(dataProcessed,groupsCleaned,stats,textString);
                    case 8
                        [stats,model] = classificationRaw(dataProcessed,groupsCleaned,stats,'lda',textString);
                    case 9
                        [stats,model] = classificationRaw(dataProcessed,groupsCleaned,stats,'qda',textString);
                    case 10
                        [stats,model] = classificationRaw(dataProcessed,groupsCleaned,stats,'knn',textString);
                end
                models{j,1} = stats;
                models{j,2} = model;
                fprintf("*********************************** End METHOD: " + num2str(j));
            end
            
            % Find best model according to false assignments
            bestModel = zeros(10,1);
            for j = 1:10
                switch selectionMethod
                    case 1
                        bestModel(j) = sum(diag(cell2mat(models{j,1}.confusionMatrix(2:end,2:end))))/sum(sum(cell2mat(models{j,1}.confusionMatrix(2:end,2:end))));
                    case 2
                        bestModel(j) = models{j,1}.crossvalidation.crossvalidationMatthewsCorrelationCoefficient(1);
                    case 3
                        bestModel(j) = models{j,1}.crossvalidation.crossvalidationCohensKappa(1);
                    case 4
                        bestModel(j) = models{j,1}.crossvalidation.crossvalidationFpMean(1);
                    case 5
                        bestModel(j) = models{j,1}.crossvalidation.crossvalidationFnMean(1);
                end
            end
            if selectionMethod == 4 | selectionMethod == 5
                [~,bestModel] = min(bestModel);
            else
                [~,bestModel] = max(bestModel);
            end
            fprintf("> Best Model obtained by method: " + num2str(bestModel));
            
            %Perform prediction using best model
            model = models{bestModel,2};
            stats = models{bestModel,1};
            
            model.plotBoolean = true;
            
            switch model.type
                case 'pca'
                    [groupsTest,groupsTestProbability,stats] = classificationPcaPredict(model,stats,dataTestProcessed);
                case 'pls-da'
                    [groupsTest,groupsTestProbability,stats] = classificationPlsDaPredict(model,stats,dataTestProcessed);
                case 'raw'
                    [groupsTest,groupsTestProbability,stats] = classificationRawPredict(model,stats,dataTestProcessed);
            end
    end

    %% Adjust stats
    stats.data.nTotal = size(dataCleaned, 1) + size(dataTestCleaned, 1);
    stats.data.nTest = size(dataTestCleaned, 1);
    if stats.data.nTest > 0 
        stats.attributeSampling.reliabilityTest = exp(log((100 - stats.attributeSampling.confidence) / 100) / stats.data.nTest) * 100;
        stats.attributeSampling.reliabilityTestPerGroup = exp(log((100 - stats.attributeSampling.confidence) / 100) / (stats.data.nTest/stats.data.groupsN))*100;
    else
        stats.attributeSampling.reliabilityTest = NaN;
        stats.attributeSampling.reliabilityTestPerGroup = NaN;
    end
    
    %% Calculate prediction limits
    stats = calcPredictionLimits(model, stats);
    
    %% Output
    fprintf("Generating output format\r\n");
    if size(dataTestCleaned,1) > 0
        output = cell(size(dataTestCleaned,1)+1,2);
        output(1,:) = {'Barcode','Predicted group'};
        output(2:end,1) = num2cell(barcodeDataTestCleaned);
        output(2:end,2) = groupsTest;
    end
    
    %% Removing outliers
    if input('Remove outliers?\n')
        stats.barcodeOutliers = barcodeDataCleaned(stats.outliers);
        
        if ~exist('statsOld')
            statsOld = stats;
        else
            statsOld = [statsOld;stats];
        end
        
        groupsCleaned = groupsCleaned(setxor((1:size(barcodeDataCleaned,1))',stats.outliers));
        if input('Predict outliers?\n')
            dataTestProcessed = [dataTestProcessed;dataProcessed(stats.outliers,:)];
            barcodeDataTestCleaned = [barcodeDataTestCleaned;barcodeDataCleaned(stats.outliers)];
        end
        dataProcessed = dataProcessed(setxor((1:size(barcodeDataCleaned,1))',stats.outliers),:);
        barcodeDataCleaned = barcodeDataCleaned(setxor((1:size(barcodeDataCleaned,1))',stats.outliers));
    else
        continueBoolean = false;
    end
end

%% Save
if input('Save?\n')
    saveName = input('File name?\n','s');
    savePath = input('Path to parent directory?\n','s');
    
    if isfolder(savePath)
        save(strcat(savePath,'\',saveName,'.mat'))
        figs = findobj(allchild(0), 'flat', 'Type', 'figure');
        for j = 1:length(figs)
            figHandle = figs(j);
            figName   = get(figHandle, 'Name');
            figName = strrep(figName,':','');
            savefig(figHandle, strcat(savePath,'\',saveName,'_',figName,'.fig'));
        end
    end
end