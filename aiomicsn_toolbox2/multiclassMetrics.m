function [TP,TN,FP,FN,sensitivity,specitivity,precision,accuracy,prevalenceThreshold,varargout] = multiclassMetrics(confusionMatrix,varargin)
% Function for the calculation of multiclass statistical metrics
%   confusionMatrix:    nxn confusion matrix with n groups
%
% part of the AI(omics)n distribution 
% https://gitlab.com/ai-omics/ai-omics
% (c) 2023 by the authors
% distributed under the conditions listed in the file
% classificationUI2.m and the documentation

if length(varargin) > 0
    confusionMatrixStd = varargin{1};
end

% Number of groups
groupsCount = size(confusionMatrix,1);

% Initialize variables
TP = zeros(groupsCount,1); % true positive
TN = zeros(groupsCount,1); % true negative
FP = zeros(groupsCount,1); % false positive
FN = zeros(groupsCount,1); % false negative
sensitivity = zeros(groupsCount,1); % sensitivity, recall, hit rate, true positive rate (TPR)
specitivity = zeros(groupsCount,1); % specificity, selectivity, true negative rate (TNR)
precision = zeros(groupsCount,1); % precision, positive predictive value (PPV)
accuracy = zeros(groupsCount,1); % accuracy (ACC)
prevalenceThreshold = zeros(groupsCount,1); % prevalence threshold (PT)

% Standard deviations
if length(varargin) > 0
    TPStd = zeros(groupsCount,1); % true positive
    TNStd = zeros(groupsCount,1); % true negative
    FPStd = zeros(groupsCount,1); % false positive
    FNStd = zeros(groupsCount,1); % false negative
    sensitivityStd = zeros(groupsCount,1);
    specitivityStd = zeros(groupsCount,1);
    precisionStd = zeros(groupsCount,1);
    accuracyStd = zeros(groupsCount,1);
    prevalenceThresholdStd = zeros(groupsCount,1);

end

% Iterate through groups
for j = 1:groupsCount
    groupsWithoutJ = setdiff([1:groupsCount],j);
    TP(j) = confusionMatrix(j,j);
    TN(j) = sum(confusionMatrix(groupsWithoutJ,groupsWithoutJ),'all');
    FP(j) = sum(confusionMatrix(j,groupsWithoutJ),'all');
    FN(j) = sum(confusionMatrix(groupsWithoutJ,j),'all');
    
    sensitivity(j) = TP(j)/(TP(j)+FN(j));
    specitivity(j) = TN(j)/(TN(j)+FP(j));
    precision(j) = TP(j) /(TP(j)+FP(j));
    accuracy(j) = TP(j) + TN(j);
   
    % Calculate prevalence threshold according to Jacques Balayla:
    % Prevalence Threshold and the Geometry of Screening Curves (arXiv:2006.00398)
    prevalenceThreshold(j) = (sqrt(sensitivity(j)*(-specitivity(j)+1))+specitivity(j)-1)/(sensitivity(j)+specitivity(j)-1);
    
    if length(varargin) > 0
        TPStd(j) = confusionMatrixStd(j,j);
        TNStd(j) = sqrt(sum(confusionMatrixStd(groupsWithoutJ,groupsWithoutJ).^2,'all'));
        FPStd(j) = sqrt(sum(confusionMatrixStd(j,groupsWithoutJ).^2,'all'));
        FNStd(j) = sqrt(sum(confusionMatrixStd(groupsWithoutJ,j).^2,'all'));
        
        sensitivityStd(j) = sqrt((FN(j)/(TP(j)+FN(j))^2*TPStd(j))^2+(-TP(j)/(TP(j)+FN(j))^2*FNStd(j))^2);
        specitivityStd(j) = sqrt((FP(j)/(TN(j)+FP(j))^2*TNStd(j))^2+(-TN(j)/(TN(j)+FP(j))^2*FPStd(j))^2);
        precisionStd(j) = sqrt((FP(j)/(TP(j)+FP(j))^2*TPStd(j))^2+(-TP(j)/(TP(j)+FP(j))^2*FPStd(j))^2);
        accuracyStd(j) = sqrt((1*TPStd(j))^2+(1*TNStd(j))^2);
        
        prevalenceThresholdStd(j) = sqrt((((1-specitivity(j))/(2*sqrt((1-specitivity(j))*sensitivity(j))*(specitivity(j)+sensitivity(j)-1))-(sqrt((1-specitivity(j))*sensitivity(j))+specitivity(j)+1)/(specitivity(j)+sensitivity(j)-1)^2)*sensitivityStd(j))^2+(((1-sensitivity(j)/(2*sqrt(sensitivity(j)*(1-specitivity(j)))))/(sensitivity(j)+specitivity(j)-1)-(sqrt(sensitivity(j)*(1-specitivity(j)))+specitivity(j)-1)/(sensitivity(j)+specitivity(j)-1)^2)*specitivityStd(j))^2);
    end
end
    
% Output
if length(varargin) > 0
    varargout{1} = TPStd;
    varargout{2} = TNStd;
    varargout{3} = FPStd;
    varargout{4} = FNStd;
    varargout{5} = sensitivityStd;
    varargout{6} = specitivityStd;
    varargout{7} = precisionStd;
    varargout{8} = accuracyStd;
    varargout{9} = prevalenceThresholdStd;
end
end

