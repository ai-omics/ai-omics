function [barcode,groups] = groupsRead(pathTable,columnGroups,columnBarcode)
% Function for reading groups information from an Excel Table
%
% part of the AI(omics)n distribution 
% https://gitlab.com/ai-omics/ai-omics
% (c) 2023 by the authors
% distributed under the conditions listed in the file
% classificationUI2.m and the documentation

[~,~,table] = xlsread(strrep(pathTable,'"',''));

groups = table(2:size(table,1),columnGroups);
groups = cellfun(@num2str,groups,'un',0);
groups = string(groups);

barcode = barcodeRead(table,columnBarcode);

barcode = barcode(groups ~= "NaN");
groups = groups(groups ~= "NaN");
end

