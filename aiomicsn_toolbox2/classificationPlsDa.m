function [stats,model] = classificationPlsDa(data,groups,stats,varargin)
%% AI(omics)^n - a versatile tool for assessing data set characteristics, data-fusion and multi-method modelling of large data sets
% 
% Developed by Felix Ruell under the supervision of Stephan Schwarzinger
% North Bavarian NMR-Centre at the University of Bayreuth
% Universitaetsstrasse 30, 95447 Bayreuth, Germany
% Contact:  s.schwarzinger@uni-bayreuth.de
% With contributions by Peter Kolb, Stefan Bindereif, Simon Steidele,
% Sandra Haupt and kind support by Birk Schuetz (Bruker BioSpin).
% (c) 2023 by the authors.
%
% License: Creative Commons CC BY-NC-SA; in case of loophole GNU-GPL for non-commercial
% applications only and with the extension that reference must be made to the original work 
% and any resulting work is relicensed alike. 
% For any commercial application a license must be obtained. For this purpose, please contact NBNC@uni-bayreuth.de
%
% The development of the software and the associated demonstration data set was in part supported by funds 
% of the Federal Ministry of Food and Agriculture (BMEL) based on a decision of the Parliament of the Federal Republic of Germany 
% via the Federal Office for Agriculture and Food (BLE) under the innovation support program (Reference number: 2816502414). 
%
% Script for classifications via (O)PLS-DA

%% Model
model = {};
model.type = 'pls-da';
model.groups = groups;

%% Parese varargin
fprintf("Parsing arguments\r\n");
plotBoolean=false;
if ismember('plot',varargin)
    plotBoolean = true;
end
oplsBoolean = false;
if ismember('opls',varargin)
    oplsBoolean = true;
end
textBoolean = false;
if ismember('text',varargin)
    textBoolean = true;
end

model.plotBoolean = plotBoolean;
model.oplsBoolean = oplsBoolean;
model.textBoolean = textBoolean;

%% Set parameters
fprintf("Setting parameters\r\n");
enableFeatureSelect = 1;
methodFeatureSelect = 1;
settingFeatureSelectNearestNeighbors = 10;
dimensionsFeatureSelect = 50;
dimensionsPcaMin = 3;
dimensionsPcaMax = 18;
dimensionsPcaVar = 0.95;
enableMahalanobis = 1;
crossvalidationMethod = 2;
crossvalidationStepsClassification = 100;
crossvalidationShareClassification = 10;
predictOutliersDescriptive = 95;
predictOutliersPredictive = 99;
attributeSamplingConfidence = 95;
sigmaEllipsoid = 2;
displayCalibration = 1;

if isfile('settings.ini')
    settingsIni = textread('settings.ini','%s','commentstyle','shell');
    settingsIni = strrep(settingsIni,':','');
    enableFeatureSelect = settingsIniRead(settingsIni,enableFeatureSelect);
    methodFeatureSelect = settingsIniRead(settingsIni,methodFeatureSelect);
    settingFeatureSelectNearestNeighbors = settingsIniRead(settingsIni,settingFeatureSelectNearestNeighbors);
    dimensionsFeatureSelect = settingsIniRead(settingsIni,dimensionsFeatureSelect);
    dimensionsPcaMin = settingsIniRead(settingsIni,dimensionsPcaMin);
    dimensionsPcaMax = settingsIniRead(settingsIni,dimensionsPcaMax);
    dimensionsPcaVar = settingsIniRead(settingsIni,dimensionsPcaVar);
    enableMahalanobis = settingsIniRead(settingsIni,enableMahalanobis);
    crossvalidationMethod = settingsIniRead(settingsIni,crossvalidationMethod);
    crossvalidationStepsClassification = settingsIniRead(settingsIni,crossvalidationStepsClassification);
    crossvalidationShareClassification = settingsIniRead(settingsIni,crossvalidationShareClassification);
    predictOutliersDescriptive = settingsIniRead(settingsIni,predictOutliersDescriptive);
    predictOutliersPredictive = settingsIniRead(settingsIni,predictOutliersPredictive);
    attributeSamplingConfidence = settingsIniRead(settingsIni,attributeSamplingConfidence);
    sigmaEllipsoid = settingsIniRead(settingsIni,sigmaEllipsoid);
    displayCalibration = settingsIniRead(settingsIni,displayCalibration);
end

if dimensionsFeatureSelect > size(data,2)
    dimensionsFeatureSelect = size(data,2);
end

if(enableMahalanobis ~= 1)
    error("Calculation of Mahalanobis distances cannot be disabled for PLS methods");
end

%% Compile inforamtion about groups
groupsDistinct = tabulate(groups);
[~,groupsOrder] = sort(groupsDistinct(:,1));
groupsDistinct = groupsDistinct(groupsOrder,:);
model.groupsDistinct = groupsDistinct;
stats.data.groupsDistinct = groupsDistinct;
stats.data.groupsN = size(groupsDistinct, 1);
stats.data.groupsSize = cell2mat(groupsDistinct(:, 2));
stats.data.groupsSizeMin = min(stats.data.groupsSize);
stats.data.groupsSizeMax = max(stats.data.groupsSize);

%% Numerical groups
fprintf("Generating logical groups\r\n");
groupsNumeric = false(size(groups,1),size(groupsDistinct,1));
for j=1:size(groupsDistinct,1)
    groupsNumeric(:,j) = ismember(groups,groupsDistinct(j,1));
end
model.groupsNumeric = groupsNumeric;

%% Setting PLS components
fprintf("Determine the number of required PLS components\r\n");
plsComponents = 50; 
if(min(size(data)) <= plsComponents)
    plsComponents = min(size(data))-1;
end


% There is a problem with the parameters required for PLS calculation. Unfortunately, not all 
% versions of MatLab could be tested. We tested Versions R2019a, R2021a and R2023a. If you 
% find a problem with other versions the following section needs to be adapted accordingly!
if isMATLABReleaseOlderThan("R2023b","release")
    % Use this line for Matlab 2019 and 2021 (ML2022 not tested)
    % Does not work with Matlab 2023 due to error in parsing arguments in plsregress.m
    [plsXL,plsyl,plsXS,plsYS,plsBeta,plsVariance,plsMSE,plsStats] = plsregress(data,groupsNumeric,plsComponents,'cv',10,'MCReps',10); 
else
    % Use the following line for MatLab 2023b and newer
    [plsXL,plsyl,plsXS,plsYS,plsBeta,plsVariance,plsMSE,plsStats] = plsregress(data,groupsNumeric,plsComponents,'cv',10,'Intercept',false,'Options',statset('UseParallel',true)); % This version runs in ML2023b!!! But mcreps is only 1 in this case!!! Not tested for versiosn 2021b/2022a/b, 2023a
end

% plots regarding variable selection:
figure('Name','Determining PLS-Dimension - Explained Variance');
plot(1:plsComponents,cumsum(100*plsVariance(2,:)),'-bo');
xlabel('Number of PLS components');
ylabel('Percent Variance Explained in y');

plsW0 = plsStats.W ./ sqrt(sum(plsStats.W.^2,1)); % normalizing PLS weights
p_pls = size(plsXL,1); % Calculate the VIP scores for plsComponent components.
plsSumSq = sum(plsXS.^2,1).*sum(plsyl.^2,1);
plsVipScore = sqrt(p_pls* sum(plsSumSq.*(plsW0.^2),2) ./ sum(plsSumSq,2));
plsIndVIP = find(plsVipScore >= 1); %Find variables with a VIP score greater than or equal to 1
figure('Name','PLS-Variable Selection - VIP Scores');
hold on;
scatter(1:length(plsVipScore),plsVipScore,'o'); % Plot the VIP scores

scatter(plsIndVIP,plsVipScore(plsIndVIP),'rx');
plot([1 length(plsVipScore)],[1 1],'--k');
hold off;
axis tight;
xlabel('Predictor Variables');
ylabel('VIP Scores');

% Ende Modifikation StS

plsComponents = plsVariance(2,2:end) > plsVariance(2,1:end-1);
plsComponents = find(plsComponents(4:end));
if isempty(plsComponents)
    plsComponents = dimensionsPcaMax-3;
end
plsComponents = plsComponents(1)+3;

if plsComponents > dimensionsPcaMax
    plsComponents = dimensionsPcaMax;
end

if plsComponents < dimensionsPcaMin
    plsComponents = dimensionsPcaMin;
end

if(size(data,2)<=plsComponents)
    plsComponents = size(data,2)-1;
end

if plsComponents < 3
    plsComponents = 3;
end

stats.plsComponents = plsComponents;

%% writing settings
stats.settings.enableMahalanobis = enableMahalanobis;
stats.settings.enableFeatureSelect = enableFeatureSelect;
stats.settings.dimensionsFeatureSelect = dimensionsFeatureSelect;
stats.settings.methodFeatureSelect = methodFeatureSelect;
stats.settings.settingFeatureSelectNearestNeighbors = settingFeatureSelectNearestNeighbors;
stats.settings.dimensionsPcaMin = dimensionsPcaMin;
stats.settings.dimensionsPcaMax = dimensionsPcaMax;
stats.settings.dimensionsPcaVar = dimensionsPcaVar;
stats.settings.crossvalidationMethod = crossvalidationMethod;
stats.settings.crossvalidationStepsClassification = crossvalidationStepsClassification;
stats.settings.crossvalidationShareClassification = crossvalidationShareClassification;
stats.settings.predictOutliersDescriptive = predictOutliersDescriptive;
stats.settings.predictOutliersPredictive = predictOutliersPredictive;
stats.settings.attributeSamplingConfidence = attributeSamplingConfidence;
stats.settings.displayCalibration = displayCalibration;
stats.settings.sigmaEllipsoid = sigmaEllipsoid

%% Error when not enough data
for j = 1:size(groupsDistinct,1)
    % For the necessary calculation of a covariance matrix further in the
    % process the dataset must contain at least as many samples (in the 
    % reduced crossvalidation datasaet) as dimensions for each group.
    if (crossvalidationMethod == 1 && groupsDistinct{j,2} - 1 < plsComponents) || (crossvalidationMethod > 1 && floor((1 - crossvalidationShareClassification/100) * groupsDistinct{j,2}) < plsComponents)
        error("Not enough data points for selected dimensions and cross-validation method. Possible cause: Number of PLS-Components (dimensionsPcaMax) exceeds size of smallest group minus samples for CV.")
    end
end

%% Feature Selection
if methodFeatureSelect == 4   % mrmr    
        fprintf("Mrmr - feature selection \r\n");
    elseif methodFeatureSelect == 3  % Chi2
        fprintf("Chi2 - feature selection \r\n");
    elseif methodFeatureSelect == 2   % relieff   
        fprintf("Relieff - feature selection \r\n");
    elseif methodFeatureSelect == 1  % Kruskal Wallis Selection as default
        fprintf("Kruskal-Wallis - feature selection \r\n");
    else 
        fprintf("No feature selection - all features used! \r\n");
end
if enableFeatureSelect > 0
    featureSelectionScore = featureSelectP(data,groups,methodFeatureSelect);
    [~,b] = sort(featureSelectionScore);
    data = data(:,b(1:dimensionsFeatureSelect));
    stats.featureSelectionSorting = b(1:dimensionsFeatureSelect);
    stats.featureSelectionScore = featureSelectionScore(b(1:dimensionsFeatureSelect));

    figure('Name',strcat('Scores of Selected Features: Method ', num2str(methodFeatureSelect)));
    hold on
    plot(1:dimensionsFeatureSelect,stats.featureSelectionScore,'-bo');
    xlabel('Feature');
    ylabel('FS Score');
    hold off;
else
    stats.featureSelectionSorting = [1:size(data,2)]';
    stats.featureSelectionScore = zeros(size(data,2),1);
end

%% Cross validation
fprintf("Beginning with cross validation\r\n");

% Leave one out
if crossvalidationMethod == 1
    crossvalidationStepsClassification = length(data(:,1));
    crossvalidationShareClassification = 1/crossvalidationStepsClassification;
    testLength = 1;
else
    testLength = round(length(data(:,1))*crossvalidationShareClassification/100);
end

% Output
fprintf("Steps: %3.0f, share of test spectra: %3.0f percent\r\n", crossvalidationStepsClassification, crossvalidationShareClassification);

% Set up test sets
testSets = cell(crossvalidationStepsClassification,1);
switch crossvalidationMethod
    % Leave one out
    case 1
        for j = 1:crossvalidationStepsClassification
            testSets{j} = j;
        end
    % Monte Carlo random
    case 2
        for j = 1:crossvalidationStepsClassification
            testSets{j} = randperm(length(data(:,1)),testLength);
        end
    % Monte Carlo pseudo-random
    case 3
        testSet = [1:length(data(:,1))];
        for j = 1:crossvalidationStepsClassification
            if length(testSet) > testLength
                testSets{j} = testSet(randperm(length(testSet),testLength));
                testSet = setdiff(testSet,testSets{j});
            else
                testSets{j} = testSet;
                testSet = randperm(length(data(:,1)),testLength - length(testSets{j}));
                testSets{j} = [testSets{j}, testSet];
                testSet = setdiff(1:length(data(:,1)), testSet);
            end
        end
    % Monte Carlo block-wise
    case 4
        testSet = [1:length(data(:,1))];
        for j = 1:crossvalidationStepsClassification
            if length(testSet) > testLength
                testSets{j} = testSet(1:testLength);
                testSet = testSet(testLength+1:end);
            else
                testSets{j} = [testSet, 1:testLength-length(testSet)];
                testSet = [testLength-length(testSet)+1:length(data(:,1))];
            end
        end
end

% Save information about cross validation
stats.data.nModelTotal = size(data, 1);
stats.data.nCvModelTesting = testLength;
stats.data.nCvModelBuilding = size(data, 1) - testLength;

% Attribute sampling: estimation of reliability based on number of samples
% used for crossvalidation for the entire dataset
stats.attributeSampling.confidence = attributeSamplingConfidence;
stats.attributeSampling.reliabilityCv = exp(log((100 - attributeSamplingConfidence) / 100) / stats.data.nCvModelTesting) * 100;
% Assuming that CV samples are equally destributed gives the best case of
% reliability for smallest/largest group, resepectively, or for the case of 
% Leave-one-out validation (then only depending on attributeSamplingConfidence)
stats.attributeSampling.reliabilityCvMinGroup = exp(log((100 - attributeSamplingConfidence) / 100) / (stats.data.nCvModelTesting * (stats.data.groupsSizeMin / stats.data.nModelTotal))) * 100;
stats.attributeSampling.reliabilityCvMaxGroup = exp(log((100 - attributeSamplingConfidence) / 100) / (stats.data.nCvModelTesting * (stats.data.groupsSizeMax / stats.data.nModelTotal))) * 100;
stats.attributeSampling.relaiabilityLoo = exp(log((100 - attributeSamplingConfidence) / 100)) * 100; 

result = cell(testLength*crossvalidationStepsClassification,3+size(groupsDistinct,1));
cvXScores = zeros(size(data,1),plsComponents);
cvXScoresCount = zeros(size(data,1),1);

reverseStr = '';
for crossvalidationStep=1:crossvalidationStepsClassification
    msg = sprintf('Step %3.0f/%3.0f', crossvalidationStep, crossvalidationStepsClassification);
    fprintf([reverseStr, msg]);
    reverseStr = repmat(sprintf('\b'), 1, length(msg));
    
    % Generate model and test dataset
    testSet = testSets{crossvalidationStep};
    modelSet = setdiff(1:length(data(:,1)),testSet);
    
    modelData = data(modelSet(:),:);
    modelGroups = groups(modelSet(:));
    modelGroupsNumeric = groupsNumeric(modelSet(:),:);
    testData = data(testSet(:),:);
    testGroups = groups(testSet(:));

    % Unit-variance scaling
    s = std(modelData);
    modelData = modelData ./repmat(s,size(modelData,1),1);
    
    % Meancentering
    m = mean(modelData);
    modelData = modelData - repmat(m,size(modelData,1),1);
    
    % OPLS
    if oplsBoolean
        [~,P_O,W_O,modelData] = oplsDo(modelData,modelGroupsNumeric);
    end
    
    % Generate PLS model
    [modelXLoadings,modelYLoadings,modelXScores,modelYScores,modelPls] = plsregress(modelData,modelGroupsNumeric,plsComponents);
    
    % unit-variance scaling
    testData = testData ./ repmat(s,size(testData,1),1);
    
    % Meancentering
    testData = testData - repmat(m,size(testData,1),1);
    
    % OPLS
    if oplsBoolean
        testData = oplsApply(testData,P_O,W_O);
    end

    % Apply PLS model
    testXScores = testData/modelXLoadings';
    
    % Preparations for Q2
    cvXScores(testSet,:) = cvXScores(testSet,:) + testXScores;
    cvXScoresCount(testSet) = cvXScoresCount(testSet) + 1;
    
    % Calculation numeric groups
    modelPredictionNumeric = [ones(size(modelData,1),1),modelData]*modelPls;
    testPredictionNumeric = [ones(size(testData,1),1),testData]*modelPls;
    
    % Predict testset
    [~,testPrediction] = max(testPredictionNumeric,[],2);
    
    % Calculate probabilites
    modelPredictionMean = zeros(1,size(groupsDistinct,1));
    modelPredictionStd = zeros(1,size(groupsDistinct,1));
    for j = 1:size(groupsDistinct,1)
        modelPredictionMean(j) = mean(modelPredictionNumeric(modelGroupsNumeric(:,j),j));
        modelPredictionStd(j) = std(modelPredictionNumeric(modelGroupsNumeric(:,j),j));
    end
    
    testPredictionProbaility = zeros(size(testData,1),size(groupsDistinct,1));
    for j = 1:size(groupsDistinct,1)
        testPredictionProbaility(:,j) = normcdf(testPredictionNumeric(:,j),modelPredictionMean(j),modelPredictionStd(j));
    end
    
    % Compare real group vs. predicted group
    result((crossvalidationStep-1)*testLength+1:crossvalidationStep*testLength,:) = [cellstr(testGroups) groupsDistinct(testPrediction,1) num2cell(testSet') num2cell(testPredictionProbaility)];
end
fprintf("\r\n");

% Evaluation of cross validation
fprintf("Begin evaluation of cross validation\r\n");

% Probability of correct assignment
crossvalidationEvaluation = zeros(crossvalidationStepsClassification,length(groupsDistinct(:,1))*2);
for i = 1:crossvalidationStepsClassification
    for ii = 1:length(groupsDistinct(:,1))
        for iii = 1+testLength*(i-1):testLength*i
            if strcmp(result(iii,1),groupsDistinct(ii,1))
                crossvalidationEvaluation(i,1+(ii-1)*2)=crossvalidationEvaluation(i,1+(ii-1)*2) + 1;
            end
            if strcmp(result(iii,1),groupsDistinct(ii,1)) && strcmp(result(iii,2),groupsDistinct(ii,1))
                crossvalidationEvaluation(i,2+(ii-1)*2)=crossvalidationEvaluation(i,2+(ii-1)*2) + 1;
            end
        end
    end
end

crossvalidationEvaluationRelative = zeros(crossvalidationStepsClassification,length(groupsDistinct(:,1)));
for i = 1:crossvalidationStepsClassification
    for ii = 1:length(groupsDistinct(:,1))
        crossvalidationEvaluationRelative(i,ii)=crossvalidationEvaluation(i,2+(ii-1)*2)/crossvalidationEvaluation(i,1+(ii-1)*2);
        if crossvalidationEvaluation(i,1+(ii-1)*2) == 0
            crossvalidationEvaluationRelative(i,ii) = 1;
        end
    end
end

crossvalidationEvaluationStatistics = cell(3,length(groupsDistinct(:,1)));
for i = 1:length(groupsDistinct(:,1))
    crossvalidationEvaluationStatistics(1,i) = groupsDistinct(i,1);
    crossvalidationEvaluationStatistics(2,i) = num2cell(mean(crossvalidationEvaluationRelative(:,i)));
    crossvalidationEvaluationStatistics(3,i) = num2cell(std(crossvalidationEvaluationRelative(:,i)));
end

% Probabilities per sample
crossvalidationProbabilites = cell(size(data,1),1);

for j = 1:size(result,1)
    tempProbabilites = crossvalidationProbabilites{result{j,3},1};
    if size(tempProbabilites,1) == 0
        crossvalidationProbabilites{result{j,3},1} = cell2mat(result(j,4:3+size(groupsDistinct,1)));
    else
        tempProbabilites(size(tempProbabilites,1)+1,:) = cell2mat(result(j,4:3+size(groupsDistinct,1)));
        crossvalidationProbabilites{result{j,3},1} = tempProbabilites;
    end
end

crossvalidationProbabilitesStd = cell(size(data,1),1);
for j = 1:size(data,1)
    if isempty(crossvalidationProbabilites{j})
        error(['Sample ', num2str(j), ' not predicted during crossvalidation']);
    end
    
    crossvalidationProbabilitesStd{j} = std(crossvalidationProbabilites{j},0,1);
    crossvalidationProbabilites{j} = mean(crossvalidationProbabilites{j},1);
end
crossvalidationProbabilitesStd = cell2mat(crossvalidationProbabilitesStd);
crossvalidationProbabilites = cell2mat(crossvalidationProbabilites);

% Probabilities full table
crossvalidationProbabilitesFull = cell(crossvalidationStepsClassification,1);
for i = 1:crossvalidationStepsClassification
    crossvalidationProbabilitesFullTemp = NaN(size(data,1),size(groupsDistinct,1));
    for k = 1:testLength
        crossvalidationProbabilitesFullTemp(result{(i-1)*testLength+k,3},:) = cell2mat(result((i-1)*testLength+k,4:end));
    end
    crossvalidationProbabilitesFull{i} = crossvalidationProbabilitesFullTemp;
end

% Confusion matrix
crossvalidationConfusionMatrix = zeros(size(groupsDistinct,1),size(groupsDistinct,1),crossvalidationStepsClassification);
for i = 1:crossvalidationStepsClassification
    resultTemp = result(1+testLength*(i-1):testLength*i,:);
    for ii = 1:size(groupsDistinct,1)
        n = ismember(resultTemp(:,1),groupsDistinct(ii,1));
        for iii = 1:size(groupsDistinct,1)
            crossvalidationConfusionMatrix(iii,ii,i) = sum(ismember(resultTemp(n,2),groupsDistinct(iii,1)));
        end
    end
    crossvalidationConfusionMatrix(:,:,i) = normalizeConfusionMatrix(squeeze(crossvalidationConfusionMatrix(:,:,i)),2);
end

crossvalidationConfusionMatrixMean = mean(crossvalidationConfusionMatrix, 3, 'omitnan').*cell2mat(groupsDistinct(:,2))';
crossvalidationConfusionMatrixStd = std(crossvalidationConfusionMatrix, 0 , 3, 'omitnan').*cell2mat(groupsDistinct(:,2))';
[crossvalidationMatthewsCorrelationCoefficient(1),crossvalidationMatthewsCorrelationCoefficient(2)] = mcmcc(crossvalidationConfusionMatrixMean,crossvalidationConfusionMatrixStd);
[crossvalidationCohensKappa(1),crossvalidationCohensKappa(2)] = cohensKappa(crossvalidationConfusionMatrixMean,crossvalidationConfusionMatrixStd);

crossvalidationConfusionMatrixMean = crossvalidationConfusionMatrixMean./size(data,1);
crossvalidationConfusionMatrixStd = crossvalidationConfusionMatrixStd./size(data,1);

crossvalidationConfusionMatrix = cell(1+size(groupsDistinct,1));
for j = 1:size(groupsDistinct,1)
    crossvalidationConfusionMatrix{1,j+1} = ['True ',groupsDistinct{j,1}];
    crossvalidationConfusionMatrix{1+j,1} = ['Predicted ',groupsDistinct{j,1}];
    
    for k = 1:size(groupsDistinct,1)
        crossvalidationConfusionMatrix{1+j,1+k} = [crossvalidationConfusionMatrixMean(j,k);crossvalidationConfusionMatrixStd(j,k)];
    end
end

[TP,TN,FP,FN,crossvalidationSensitivityValues,crossvalidationSpecitivityValues,crossvalidationPrecisionValues,crossvalidationAccuracyValues,crossvalidationPrevalenceThresholdValues,~,~,~,~,crossvalidationSensitivityStd,crossvalidationSpecitivityStd,crossvalidationPrecisionStd,crossvalidationAccuracyStd,crossvalidationPrevalenceThresholdStd] = multiclassMetrics(crossvalidationConfusionMatrixMean,crossvalidationConfusionMatrixStd);

% False Positive Fraction = FPF = FP, for selection criterion for model
crossvalidationFp = cell(size(groupsDistinct,1),2);
crossvalidationFpMean = 1;
for j = 1:size(groupsDistinct,1)
    crossvalidationFp{j,1} = groupsDistinct{j,1};
    crossvalidationFp{j,2} = FP(j);  % will allow selection to minimize FP for paricular group.
    crossvalidationFpMean = mean(FP); % will allow seletion for overall smallest FP
end

% False Negative Fraction = FNF = FN; for selection critierion for model
crossvalidationFn = cell(size(groupsDistinct,1),2);
crossvalidationFnMean = 1;
for j = 1:size(groupsDistinct,1)
    crossvalidationFn{j,1} = groupsDistinct{j,1};
    crossvalidationFn{j,2} = FN(j);
   crossvalidationFnMean = mean(FN);
end

% Sensitivity = recall = hit rate = true positve rate TPR
crossvalidationSensitivity = cell(size(groupsDistinct,1),3);
for j = 1:size(groupsDistinct,1)
    crossvalidationSensitivity{j,1} = groupsDistinct{j,1};
    crossvalidationSensitivity{j,2} = crossvalidationSensitivityValues(j);
    crossvalidationSensitivity{j,3} = crossvalidationSensitivityStd(j);
end

% Specificity = selectivity = true negative rate TNR
crossvalidationSpecitivity = cell(size(groupsDistinct,1),3);
for j = 1:size(groupsDistinct,1)
    crossvalidationSpecitivity{j,1} = groupsDistinct{j,1};
    crossvalidationSpecitivity{j,2} = crossvalidationSpecitivityValues(j);
    crossvalidationSpecitivity{j,3} = crossvalidationSpecitivityStd(j);
end

% Precision = positive predictive value PPV
crossvalidationPrecision = cell(size(groupsDistinct,1),3);
for j = 1:size(groupsDistinct,1)
    crossvalidationPrecision{j,1} = groupsDistinct{j,1};
    crossvalidationPrecision{j,2} = crossvalidationPrecisionValues(j);
    crossvalidationPrecision{j,3} = crossvalidationPrecisionStd(j);
end

% Accuracy ACC
crossvalidationAccuracy = cell(size(groupsDistinct,1),3);
for j = 1:size(groupsDistinct,1)
    crossvalidationAccuracy{j,1} = groupsDistinct{j,1};
    crossvalidationAccuracy{j,2} = crossvalidationAccuracyValues(j);
    crossvalidationAccuracy{j,3} = crossvalidationAccuracyStd(j);
end

% Prevalence Threshold PT
crossvalidationPrevalenceThreshold = cell(size(groupsDistinct,1),3);
for j = 1:size(groupsDistinct,1)
    crossvalidationPrevalenceThreshold{j,1} = groupsDistinct{j,1};
    crossvalidationPrevalenceThreshold{j,2} = crossvalidationPrevalenceThresholdValues(j);
    crossvalidationPrevalenceThreshold{j,3} = crossvalidationPrevalenceThresholdStd(j);
end
    
% ROC
groupsNumber = zeros(length(groups),1);
for i = 1:length(groups)
    groupsNumber(i) = find(strcmp(groups(i),groupsDistinct(:,1)));
end
crossvalidationRoc = mcroc(crossvalidationProbabilites,groupsNumber);

% Misallocation
crossvalidationMisallocation = result(~strcmp(result(:,1),result(:,2)),3);
if size(crossvalidationMisallocation,1) > 0
    crossvalidationMisallocation = tabulate(cell2mat(crossvalidationMisallocation));
    crossvalidationMisallocation = crossvalidationMisallocation(crossvalidationMisallocation(:,2)>0,:);
else
    crossvalidationMisallocation = num2cell(zeros(1,3));
end

fprintf("cvMCC\n");
disp(crossvalidationMatthewsCorrelationCoefficient);

stats.crossvalidation.crossvalidationEvaluationStatistics = crossvalidationEvaluationStatistics;
stats.crossvalidation.crossvalidationConfusionMatrix = crossvalidationConfusionMatrix';
stats.crossvalidation.crossvalidationConfusionMatrixMean = crossvalidationConfusionMatrixMean';
stats.crossvalidation.crossvalidationConfusionMatrixStd = crossvalidationConfusionMatrixStd';
stats.crossvalidation.crossvalidationMatthewsCorrelationCoefficient = crossvalidationMatthewsCorrelationCoefficient;
stats.crossvalidation.crossvalidationCohensKappa = crossvalidationCohensKappa;
stats.crossvalidation.crossvalidationRoc = crossvalidationRoc;
stats.crossvalidation.crossvalidationMisallocation = crossvalidationMisallocation;
stats.crossvalidation.crossvalidationSensitivity = crossvalidationSensitivity;
stats.crossvalidation.crossvalidationSpecitivity = crossvalidationSpecitivity;
stats.crossvalidation.crossvalidationPrecision = crossvalidationPrecision;
stats.crossvalidation.crossvalidationAccuracy = crossvalidationAccuracy;
stats.crossvalidation.crossvalidationPrevalenceThreshold = crossvalidationPrevalenceThreshold;
stats.crossvalidation.crossvalidationProbabilites = crossvalidationProbabilites;
stats.crossvalidation.crossvalidationProbabilitesStd = crossvalidationProbabilitesStd;
stats.crossvalidation.crossvalidationProbabilitesFull = crossvalidationProbabilitesFull;


stats.crossvalidation.crossvalidationFp = crossvalidationFp;
stats.crossvalidation.crossvalidationFpMean = crossvalidationFpMean;
stats.crossvalidation.crossvalidationFn = crossvalidationFn;
stats.crossvalidation.crossvalidationFnMean = crossvalidationFnMean;

% in case of not more than 2 groups calculate additional benchmarks
if size(groupsDistinct,1) == 2
    [negativePredictiveValue,missRate,fallOut,falseDiscoveryRate,falseOmissonRate,positiveLikelyhoodRatio,negativeLikelyhoodRatio,threatScore,f1Score,fowlkesMellowsIndex,informedness,markedness,diagnosticOddsRatio,relativeRisc] = binaryMetrics(crossvalidationConfusionMatrixMean);
    stats.crossvalidation.crossvalidationMeanNPV = negativePredictiveValue;
    stats.crossvalidation.crossvalidationMeanMissRate = missRate;
    stats.crossvalidation.crossvalidationMeanFallOut = fallOut;
    stats.crossvalidation.crossvalidationMeanFDR = falseDiscoveryRate;
    stats.crossvalidation.crossvalidationMeanFOR = falseOmissonRate;
    stats.crossvalidation.crossvalidationMeanPLR = positiveLikelyhoodRatio;
    stats.crossvalidation.crossvalidationMeanNLR = negativeLikelyhoodRatio;
    stats.crossvalidation.crossvalidationMeanThreatScore = threatScore;
    stats.crossvalidation.crossvalidationMeanF1 = f1Score;
    stats.crossvalidation.crossvalidationMeanFMI = fowlkesMellowsIndex;
    stats.crossvalidation.crossvalidationMeanInformedness = informedness;
    stats.crossvalidation.crossvalidationMeanMarkedness = markedness;
    stats.crossvalidation.crossvalidationMeanDOR = diagnosticOddsRatio;
    stats.crossvalidation.crossvalidationMeanRelativeRisc = relativeRisc; 
end

%% (O)PLS-DA predictive

fprintf("Generating predictive model\r\n");

% Unit-variance scaling
s = std(data);
data = data ./repmat(s,size(data,1),1);
model.s = s;

% PCA, Get mean of model data, meancentering
m = mean(data);
data = data - repmat(m,size(data,1),1);
model.m = m;

% OPLS
if oplsBoolean
    [~,P_O,W_O,data] = oplsDo(data,groupsNumeric);
    model.P_O = P_O;
    model.W_O = W_O;
end

% Generate PLS model
[modelXLoadings,modelYLoadings,modelXScores,modelYScores,modelPls,~,~,~] = plsregress(data,groupsNumeric,plsComponents);
model.modelXScores = modelXScores;
model.modelPlotScore = modelXScores;
model.modelXLoadings = modelXLoadings;
model.modelPls = modelPls;

% Statistics
stats.R2X = cumsum(diag(modelXScores'*modelXScores*modelXLoadings'*modelXLoadings)/sum(sum(data.^2)));
stats.R2Y = cumsum(diag(modelXScores'*modelXScores*modelYLoadings'*modelYLoadings)/sum(sum(groupsNumeric.^2)));

for j = 1:size(data,1)
    cvXScores(j,:) = cvXScores(j,:)./cvXScoresCount(j);
end
model.cvXScores = cvXScores;
model.cvPlotScore = cvXScores;
stats.Q2X = zeros(plsComponents,1);
for j = 1:plsComponents
    stats.Q2X(j) = 1-sum(sum((data - cvXScores(:,1:j)*modelXLoadings(:,1:j)').^2))/sum(sum(data.^2));
end

% VIP scores
vipScores = vipScore(modelXLoadings',[stats.R2X(1);diff(stats.R2X)]);
[vipScoresSorted,vipScoresIndex] = sort(vipScores,'descend'); % Sort in descending order
stats.vipScores = [vipScoresSorted;vipScoresIndex];

% Calculation numeric groups
modelPredictionNumeric = [ones(size(data,1),1),data]*modelPls;

% Predict testset
[~,modelPrediction] = max(modelPredictionNumeric,[],2);

% Calculate probabilites
modelPredictionMean = zeros(1,size(groupsDistinct,1));
modelPredictionStd = zeros(1,size(groupsDistinct,1));
for j = 1:size(groupsDistinct,1)
    modelPredictionMean(j) = mean(modelPredictionNumeric(groupsNumeric(:,j),j));
    modelPredictionStd(j) = std(modelPredictionNumeric(groupsNumeric(:,j),j));
end
model.modelPredictionMean = modelPredictionMean;
model.modelPredictionStd = modelPredictionStd;

modelPredictionProbaility = zeros(size(data,1),size(groupsDistinct,1));
for j = 1:size(groupsDistinct,1)
    modelPredictionProbaility(:,j) = normcdf(modelPredictionNumeric(:,j),modelPredictionMean(j),modelPredictionStd(j));
end
    
% Means
modelMean = NaN(size(groupsDistinct,1),size(modelXScores,2));
cvMean = NaN(size(groupsDistinct,1),size(cvXScores,2));

for i=1:size(groupsDistinct,1)
    modelMean(i,:) = mean(modelXScores(ismember(groups,groupsDistinct{i,1}),:));
    cvMean(i,:) = mean(cvXScores(ismember(groups,groupsDistinct{i,1}),:));
end
model.modelMean = modelMean;
model.cvMean = cvMean;

% Test for outliers
% Descriptive
fprintf("Test for outliers\r\n");
modelDistributionEuclidean = zeros(size(modelXScores,1),1);
modelDistributionMahalanobis = zeros(size(modelXScores,1),1);
for i=1:size(modelXScores,1)
    ii = find(ismember(groupsDistinct(:,1),groups(i)));
    modelDistributionEuclidean(i) = pdist2(modelXScores(i,:),modelMean(ii,:),'euclidean');
    ii = find(ismember(groups,groupsDistinct{ii,1}));
    modelDistributionMahalanobis(i) = mahal(modelXScores(i,:),modelXScores(ii,:));
end
modelDistributionMahalanobisIntervals = zeros(size(groupsDistinct,1),1);
modelDistributionEuclideanIntervals = zeros(size(groupsDistinct,1),1);
for i=1:size(groupsDistinct,1)
    ii = find(ismember(groups,groupsDistinct{i,1}));
    modelDistributionMahalanobisIntervals(i) = prctile(modelDistributionMahalanobis(ii),predictOutliersDescriptive);
    modelDistributionEuclideanIntervals(i) = prctile(modelDistributionEuclidean(ii),predictOutliersDescriptive);
end
modelOutliers = [];
for i=1:size(modelXScores,1)
    ii = find(ismember(groupsDistinct(:,1),groups(i)));
    if (abs(modelDistributionMahalanobis(i)) > modelDistributionMahalanobisIntervals(ii)) | (abs(modelDistributionEuclidean(i)) > modelDistributionEuclideanIntervals(ii))
        modelOutliers = [modelOutliers;i];
    end
end
stats.outliers = modelOutliers;

% Predictive
cvDistributionEuclidean = zeros(size(cvXScores,1),1);
cvDistributionMahalanobis = zeros(size(cvXScores,1),1);
for i=1:size(cvXScores,1)
    ii = find(ismember(groupsDistinct(:,1),groups(i)));
    cvDistributionEuclidean(i) = pdist2(cvXScores(i,:),cvMean(ii,:),'euclidean');
    ii = find(ismember(groups,groupsDistinct{ii,1}));
    cvDistributionMahalanobis(i) = mahal(cvXScores(i,:),cvXScores(ii,:));
end
cvDistributionMahalanobisIntervals = zeros(size(groupsDistinct,1),1);
cvDistributionEuclideanIntervals = zeros(size(groupsDistinct,1),1);
for i=1:size(groupsDistinct,1)
    ii = find(ismember(groups,groupsDistinct{i,1}));
    cvDistributionMahalanobisIntervals(i) = prctile(cvDistributionMahalanobis(ii),predictOutliersPredictive);
    cvDistributionEuclideanIntervals(i) = prctile(cvDistributionEuclidean(ii),predictOutliersPredictive);
end

if enableMahalanobis == 1
    stats.outliers = modelOutliers;
    % Descriptive
    stats.distances.mahalanobis = modelDistributionMahalanobis;
    stats.distances.mahalanobisMean = mean(modelDistributionMahalanobis);
    stats.distances.mahalanobisStandardDeviation = std(modelDistributionMahalanobis);
    stats.distances.mahalanobisMedian = median(modelDistributionMahalanobis);
    stats.distances.mahalanobis90Percentile = prctile(modelDistributionMahalanobis,90);
    stats.distances.mahalanobis95Percentile = prctile(modelDistributionMahalanobis,95);
    stats.distances.mahalanobis975Percentile = prctile(modelDistributionMahalanobis,97.5);
    stats.distances.mahalanobis99Percentile = prctile(modelDistributionMahalanobis,99);
    stats.distances.euclidean = modelDistributionEuclidean;
    stats.distances.euclideanMean = mean(modelDistributionEuclidean);
    stats.distances.euclideanStandardDeviation = std(modelDistributionEuclidean);
    stats.distances.euclideanMedian = median(modelDistributionEuclidean);
    stats.distances.euclidean90Percentile = prctile(modelDistributionEuclidean,90);
    stats.distances.euclidean95Percentile = prctile(modelDistributionEuclidean,95);
    stats.distances.euclidean975Percentile = prctile(modelDistributionEuclidean,97.5);
    stats.distances.euclidean99Percentile = prctile(modelDistributionEuclidean,99);
    % Predictive
    stats.crossvalidation.distances.mahalanobis = cvDistributionMahalanobis;
    stats.crossvalidation.distances.mahalanobisMean = mean(cvDistributionMahalanobis);
    stats.crossvalidation.distances.mahalanobisStandardDeviation = std(cvDistributionMahalanobis);
    stats.crossvalidation.distances.mahalanobisMedian = median(cvDistributionMahalanobis);
    stats.crossvalidation.distances.mahalanobis90Percentile = prctile(cvDistributionMahalanobis,90);
    stats.crossvalidation.distances.mahalanobis95Percentile = prctile(cvDistributionMahalanobis,95);
    stats.crossvalidation.distances.mahalanobis975Percentile = prctile(cvDistributionMahalanobis,97.5);
    stats.crossvalidation.distances.mahalanobis99Percentile = prctile(cvDistributionMahalanobis,99);
    stats.crossvalidation.distances.euclidean = cvDistributionEuclidean;
    stats.crossvalidation.distances.euclideanMean = mean(cvDistributionEuclidean);
    stats.crossvalidation.distances.euclideanStandardDeviation = std(cvDistributionEuclidean);
    stats.crossvalidation.distances.euclideanMedian = median(cvDistributionEuclidean);
    stats.crossvalidation.distances.euclidean90Percentile = prctile(cvDistributionEuclidean,90);
    stats.crossvalidation.distances.euclidean95Percentile = prctile(cvDistributionEuclidean,95);
    stats.crossvalidation.distances.euclidean975Percentile = prctile(cvDistributionEuclidean,97.5);
    stats.crossvalidation.distances.euclidean99Percentile = prctile(cvDistributionEuclidean,99);
else
    stats.outliers = NaN;
    % Descriptive
    stats.distances.mahalanobis = NaN;
    stats.distances.mahalanobisMean = NaN;
    stats.distances.mahalanobisStandardDeviation = NaN;
    stats.distances.mahalanobisMedian = NaN;
    stats.distances.mahalanobis90Percentile = NaN;
    stats.distances.mahalanobis95Percentile = NaN;
    stats.distances.mahalanobis975Percentile = NaN;
    stats.distances.mahalanobis99Percentile = NaN;
    stats.distances.euclidean = NaN;
    stats.distances.euclideanMean = NaN;
    stats.distances.euclideanStandardDeviation = NaN;
    stats.distances.euclideanMedian = NaN;
    stats.distances.euclidean90Percentile = NaN;
    stats.distances.euclidean95Percentile = NaN;
    stats.distances.euclidean975Percentile = NaN;
    stats.distances.euclidean99Percentile = NaN;
    % Predictive
    stats.outliers = NaN;
    stats.crossvalidation.distances.mahalanobis = NaN;
    stats.crossvalidation.distances.mahalanobisMean = NaN;
    stats.crossvalidation.distances.mahalanobisStandardDeviation = NaN;
    stats.crossvalidation.distances.mahalanobisMedian = NaN;
    stats.crossvalidation.distances.mahalanobis90Percentile = NaN;
    stats.crossvalidation.distances.mahalanobis95Percentile = NaN;
    stats.crossvalidation.distances.mahalanobis975Percentile = NaN;
    stats.crossvalidation.distances.mahalanobis99Percentile = NaN;
    stats.crossvalidation.distances.euclidean = NaN;
    stats.crossvalidation.distances.euclideanMean = NaN;
    stats.crossvalidation.distances.euclideanStandardDeviation = NaN;
    stats.crossvalidation.distances.euclideanMedian = NaN;
    stats.crossvalidation.distances.euclidean90Percentile = NaN;
    stats.crossvalidation.distances.euclidean95Percentile = NaN;
    stats.crossvalidation.distances.euclidean975Percentile = NaN;
    stats.crossvalidation.distances.euclidean99Percentile = NaN;
end

% Quality of separation
modelQuality = NaN(size(groupsDistinct,1));

modelDistributionMahalanobisMeans = zeros(size(groupsDistinct,1),1);
for j = 1:size(groupsDistinct,1)
    modelDistributionMahalanobisMeans(j) = mean(modelDistributionMahalanobis(groupsNumeric(:,j)));
end

for j = 1:size(groupsDistinct,1)
    for k = 1:j
        modelQuality(j,k)= (mahal(modelMean(k,:),modelXScores(groupsNumeric(:,j),:))+mahal(modelMean(j,:),modelXScores(groupsNumeric(:,k),:)))/(modelDistributionMahalanobisMeans(j)+modelDistributionMahalanobisMeans(k));
    end
end

stats.quality = cell(size(groupsDistinct,1)+1);
for j = 1:size(groupsDistinct,1)
    stats.quality(j+1,1)=groupsDistinct(j,1);
    stats.quality(1,j+1)=groupsDistinct(j,1);
end
stats.quality(2:end,2:end) = num2cell(modelQuality);

% Confusion matrix
for j = 1:size(groupsDistinct,1)
    confusionMatrix(1,j+1) = groupsDistinct(j,1);
    confusionMatrix(j+1,1) = groupsDistinct(j,1);
end
for j = 1:size(groupsDistinct,1)
    n = find(ismember(groups,groupsDistinct(j,1)));
    for jj = 1:size(groupsDistinct,1)
        confusionMatrix(1+jj,1+j) = num2cell(sum(ismember(groupsDistinct(modelPrediction(n),1),groupsDistinct(jj,1))));
    end
end
stats.confusionMatrix = confusionMatrix;

% Matthews corrleation coefficient
stats.matthewsCorrelationCoefficient = mcmcc(cell2mat(confusionMatrix(2:size(groupsDistinct,1)+1,2:size(groupsDistinct,1)+1)));

% Cohen's Kappa
stats.cohensKappa = cohensKappa(cell2mat(confusionMatrix(2:size(groupsDistinct,1)+1,2:size(groupsDistinct,1)+1)));

%% 95% Intervals
ellipsoidValues = ellipsoidValuesCalculate(modelXScores,groups,sigmaEllipsoid);
cvEllipsoidValues = ellipsoidValuesCalculate(cvXScores,groups,sigmaEllipsoid);
model.ellipsoidValues = ellipsoidValues;
model.cvEllipsoidValues = cvEllipsoidValues;

%% Plot
if plotBoolean
    CM = lines(size(model.groupsDistinct,1));
    
    fprintf("Plotting test for outliers\r\n");
    for i=1:size(groupsDistinct,1)
        figure('Name',strcat('Calibration - Outliers test: group ',num2str(i),' (descriptive)'));
        hold on
        ii = find(ismember(groups,groupsDistinct(i,1)));
        patch([log(min(modelDistributionMahalanobis(ii))),log(modelDistributionMahalanobisIntervals(i)),log(modelDistributionMahalanobisIntervals(i)),log(min(modelDistributionMahalanobis(ii)))],[log(min(modelDistributionEuclidean(ii))),log(min(modelDistributionEuclidean(ii))),log(modelDistributionEuclideanIntervals(i)),log(modelDistributionEuclideanIntervals(i))],'b','FaceAlpha',0.5,'EdgeColor','none','LineStyle','none');
        patch([log(min(modelDistributionMahalanobis(ii))),log(modelDistributionMahalanobisIntervals(i)),log(modelDistributionMahalanobisIntervals(i)),log(min(modelDistributionMahalanobis(ii)))],[log(modelDistributionEuclideanIntervals(i)),log(modelDistributionEuclideanIntervals(i)),log(max(modelDistributionEuclidean(ii))),log(max(modelDistributionEuclidean(ii)))],'r','FaceAlpha',0.5,'EdgeColor','none','LineStyle','none');
        patch([log(modelDistributionMahalanobisIntervals(i)),log(max(modelDistributionMahalanobis(ii))),log(max(modelDistributionMahalanobis(ii))),log(modelDistributionMahalanobisIntervals(i))],[log(min(modelDistributionEuclidean(ii))),log(min(modelDistributionEuclidean(ii))),log(modelDistributionEuclideanIntervals(i)),log(modelDistributionEuclideanIntervals(i))],'r','FaceAlpha',0.5,'EdgeColor','none','LineStyle','none');
        patch([log(modelDistributionMahalanobisIntervals(i)),log(max(modelDistributionMahalanobis(ii))),log(max(modelDistributionMahalanobis(ii))),log(modelDistributionMahalanobisIntervals(i))],[log(modelDistributionEuclideanIntervals(i)),log(modelDistributionEuclideanIntervals(i)),log(max(modelDistributionEuclidean(ii))),log(max(modelDistributionEuclidean(ii)))],'r','FaceAlpha',0.5,'EdgeColor','none','LineStyle','none');
        line([log(modelDistributionMahalanobisIntervals(i)),log(modelDistributionMahalanobisIntervals(i));log(modelDistributionMahalanobisIntervals(i)),log(min(modelDistributionMahalanobis(ii)))],[log(min(modelDistributionEuclidean(ii))),log(modelDistributionEuclideanIntervals(i));log(modelDistributionEuclideanIntervals(i)),log(modelDistributionEuclideanIntervals(i))],'Color','k','LineStyle','-');
        plot(log(modelDistributionMahalanobis(ii)),log(modelDistributionEuclidean(ii)),'ko','MarkerFaceColor',CM(i,:));
        if textBoolean
            text(log(modelDistributionMahalanobis(ii)),log(modelDistributionEuclidean(ii)),num2str(ii));
        end
        xlim([log(min(modelDistributionMahalanobis(ii))),log(max(modelDistributionMahalanobis(ii)))]);
        ylim([log(min(modelDistributionEuclidean(ii))),log(max(modelDistributionEuclidean(ii)))]);
        hold off;
    end
    for i=1:size(groupsDistinct,1)
        figure('Name',strcat('Calibration - Outliers test: group ',num2str(i),' (predictive)'));
        hold on
        ii = find(ismember(groups,groupsDistinct(i,1)));
        patch([log(min(cvDistributionMahalanobis(ii))),log(cvDistributionMahalanobisIntervals(i)),log(cvDistributionMahalanobisIntervals(i)),log(min(cvDistributionMahalanobis(ii)))],[log(min(cvDistributionEuclidean(ii))),log(min(cvDistributionEuclidean(ii))),log(cvDistributionEuclideanIntervals(i)),log(cvDistributionEuclideanIntervals(i))],'g','FaceAlpha',0.5,'EdgeColor','none','LineStyle','none');
        patch([log(min(cvDistributionMahalanobis(ii))),log(cvDistributionMahalanobisIntervals(i)),log(cvDistributionMahalanobisIntervals(i)),log(min(cvDistributionMahalanobis(ii)))],[log(cvDistributionEuclideanIntervals(i)),log(cvDistributionEuclideanIntervals(i)),log(max(cvDistributionEuclidean(ii))),log(max(cvDistributionEuclidean(ii)))],'y','FaceAlpha',0.5,'EdgeColor','none','LineStyle','none');
        patch([log(cvDistributionMahalanobisIntervals(i)),log(max(cvDistributionMahalanobis(ii))),log(max(cvDistributionMahalanobis(ii))),log(cvDistributionMahalanobisIntervals(i))],[log(min(cvDistributionEuclidean(ii))),log(min(cvDistributionEuclidean(ii))),log(cvDistributionEuclideanIntervals(i)),log(cvDistributionEuclideanIntervals(i))],'y','FaceAlpha',0.5,'EdgeColor','none','LineStyle','none');
        patch([log(cvDistributionMahalanobisIntervals(i)),log(max(cvDistributionMahalanobis(ii))),log(max(cvDistributionMahalanobis(ii))),log(cvDistributionMahalanobisIntervals(i))],[log(cvDistributionEuclideanIntervals(i)),log(cvDistributionEuclideanIntervals(i)),log(max(cvDistributionEuclidean(ii))),log(max(cvDistributionEuclidean(ii)))],'y','FaceAlpha',0.5,'EdgeColor','none','LineStyle','none');
        line([log(cvDistributionMahalanobisIntervals(i)),log(cvDistributionMahalanobisIntervals(i));log(cvDistributionMahalanobisIntervals(i)),log(min(cvDistributionMahalanobis(ii)))],[log(min(cvDistributionEuclidean(ii))),log(cvDistributionEuclideanIntervals(i));log(cvDistributionEuclideanIntervals(i)),log(cvDistributionEuclideanIntervals(i))],'Color','k','LineStyle','-');
        plot(log(cvDistributionMahalanobis(ii)),log(cvDistributionEuclidean(ii)),'ko','MarkerFaceColor',CM(i,:));
        if textBoolean
            text(log(cvDistributionMahalanobis(ii)),log(cvDistributionEuclidean(ii)),num2str(ii));
        end
        xlim([log(min(cvDistributionMahalanobis(ii))),log(max(cvDistributionMahalanobis(ii)))]);
        ylim([log(min(cvDistributionEuclidean(ii))),log(max(cvDistributionEuclidean(ii)))]);
        hold off;
    end
    
    fprintf("Plotting prediction\r\n");
    figure('Name','Calibration - Model (descriptive)');
    hold on;
    CM = lines(size(groupsDistinct,1));
    
    for j=1:size(groupsDistinct,1)
        jj = find(ismember(groups,groupsDistinct(j,1)));
        plot3(modelXScores(jj,1),modelXScores(jj,2),modelXScores(jj,3),'ko','MarkerFaceColor',CM(j,:));
    end
    for j=1:size(groupsDistinct,1)
        surf(squeeze(ellipsoidValues(j,1,:,:)),squeeze(ellipsoidValues(j,2,:,:)),squeeze(ellipsoidValues(j,3,:,:)),'EdgeColor',CM(j,:),'LineStyle',':','FaceColor',CM(j,:),'FaceAlpha',0.3);
    end
    if textBoolean
        text(modelXScores(:,1),modelXScores(:,2),modelXScores(:,3),num2str([1:1:size(modelXScores,1)]'));
    end
    
    axis vis3d;
    box on;
    grid on;
    legend(groupsDistinct(:,1));
    xlabel('Latent variable 1');
    ylabel('Latent variable 2');
    zlabel('Latent variable 3');
    view(30,10);
    hold off;
    
    figure('Name','Calibration - Model (predictive)');
    hold on;
    CM = lines(size(groupsDistinct,1));
    
    for j=1:size(groupsDistinct,1)
        jj = find(ismember(groups,groupsDistinct(j,1)));
        plot3(cvXScores(jj,1),cvXScores(jj,2),cvXScores(jj,3),'ko','MarkerFaceColor',CM(j,:));
    end
    for j=1:size(groupsDistinct,1)
        surf(squeeze(cvEllipsoidValues(j,1,:,:)),squeeze(cvEllipsoidValues(j,2,:,:)),squeeze(cvEllipsoidValues(j,3,:,:)),'EdgeColor',CM(j,:),'LineStyle',':','FaceColor',CM(j,:),'FaceAlpha',0.3);
    end
    if textBoolean
        text(cvXScores(:,1),cvXScores(:,2),cvXScores(:,3),num2str([1:1:size(cvXScores,1)]'));
    end
    
    axis vis3d;
    box on;
    grid on;
    legend(groupsDistinct(:,1));
    xlabel('Latent variable 1');
    ylabel('Latent variable 2');
    zlabel('Latent variable 3');
    view(30,10);
    hold off;

    fprintf("Plotting ROC graphs\r\n");
    for i=1:size(groupsDistinct,1)
        figure('Name',strcat('Calibration - ROC: group ',num2str(i)));
        hold on
        
        plot(crossvalidationRoc(i).roc(:,2),crossvalidationRoc(i).roc(:,1),'-','MarkerFaceColor',CM(i,:),'LineWidth',2);
        
        xlabel('False positive rate');
        ylabel('True positive rate');
        xlim([0,1]);
        ylim([0,1]);
        hold off;
    end

    fprintf("Plotting Confusion Table (from cross-validation) \r\n");
    plotConfusion(groupsDistinct,stats.crossvalidation.crossvalidationConfusionMatrixMean,stats.data.nModelTotal,stats.settings.crossvalidationStepsClassification,stats.settings.crossvalidationShareClassification,1);
end
end