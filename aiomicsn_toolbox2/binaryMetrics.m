function [TP,TN,FP,FN,negativePredictiveValue,missRate,fallOut,falseDiscoveryRate,falseOmissonRate,positiveLikelyhoodRatio,negativeLikelyhoodRatio,threatScore,f1Score,fowlkesMellowsIndex,informedness,markedness,diagnosticOddsRatio,relativeRisc] = binaryMetrics(confusionMatrix)
%function [negativePredictiveValue,missRate,fallOut,falseDiscoveryRate,falseOmissonRate,positiveLikelyhoodRatio,negativeLikelyhoodRatio,threatScore,f1Score,fowlkesMellowsIndex,informedness,markedness,diagnosticOddsRatio,relativeRisc,varargout] = binaryMetrics(confusionMatrix,varargin)
%Function for the calculation of multiclass statistical metrics
%   confusionMatrix:    nxn confusion matrix with n groups
%
% part of the AI(omics)n distribution 
% https://gitlab.com/ai-omics/ai-omics
% (c) 2023 by the authors
% distributed under the conditions listed in the file
% classificationUI2.m and the documentation

% Number of groups
if size(confusionMatrix,1) > 2
    return
end
    
% Initialize variables
    TP = 0; % zeros(groupsCount,1); % true positive
    TN = 0; %zeros(groupsCount,1); % true negative
    FP = 0; %zeros(groupsCount,1); % false positive
    FN = 0; %zeros(groupsCount,1); % false negative
    sensitivity = 0; %zeros(groupsCount,1); % sensitivity, recall, hit rate, true positive rate (TPR)
    specitivity = 0; %zeros(groupsCount,1); % specificity, selectivity, true negative rate (TNR)
    precision = 0; %zeros(groupsCount,1); % precision, positive predictive value (PPV)
    accuracy = 0; %zeros(groupsCount,1); % accuracy (ACC)
    prevalenceThreshold = 0; %zeros(groupsCount,1); % prevalence threshold (PT)
    negativePredictiveValue = 0; %zeros(groupsCount,1); % negative predictive value (NPV)
    missRate = 0; %eros(groupsCount,1); % miss rate or flase negative rate (FNR)
    fallOut = 0; %zeros(groupsCount,1); % fall out of false positive rate (FPR)
    falseDiscoveryRate = 0; %zeros(groupsCount,1); % false discovery rate (FDR)
    falseOmissonRate = 0; %zeros(groupsCount,1); % false omission rate (FOR)
    positiveLikelyhoodRatio = 0; %zeros(groupsCount,1); % positive likelyhood ratio (LR+)
    negativeLikelyhoodRatio = 0; %zeros(groupsCount,1); % negative likelyhood ratio (LR-)
    threatScore = 0; %zeros(groupsCount,1); % threat score (TS) or critical success index (CSI)
    f1Score = 0; %zeros(groupsCount,1); % F1 score (F1)
    fowlkesMellowsIndex = 0; %zeros(groupsCount,1); % Fowlkes-Mallows index (FM)
    informedness = 0; %zeros(groupsCount,1); % informedness or beeokmaker informedness (BM)
    markedness = 0; %zeros(groupsCount,1); % markedness (MK) or deltaP (dP)
    diagnosticOddsRatio = 0; %zeros(groupsCount,1); % diagnostic odds ratio (DOR)
    relativeRisc = 0; %zeros(groupsCount,1); % relative risk 
   
    TP = confusionMatrix(1,1);        
    TN = confusionMatrix(2,2);
    FP = confusionMatrix(1,2);
    FN = confusionMatrix(2,1);
    
    sensitivity = TP/(TP+FN);
    specitivity = TN/(TN+FP);
    precision = TP /(TP+FP);
    accuracy = TP + TN; 
    
    negativePredictiveValue = TN/(TN+TP);
    missRate = FN/(FN+TP);
    fallOut = FP/(FP+TN);
    falseDiscoveryRate = FP/(FP+TP);
    falseOmissonRate = FN/(FN+TN);
    positiveLikelyhoodRatio = sensitivity/fallOut;
    negativeLikelyhoodRatio = missRate/specitivity;
    threatScore = TP/(TP+FN+FP);
    f1Score = (2*TP)/(2*TP+FP+FN);
    fowlkesMellowsIndex = sqrt(precision*sensitivity);
    informedness = sensitivity+specitivity-1;
    markedness = precision+negativePredictiveValue-1;
    diagnosticOddsRatio = positiveLikelyhoodRatio/negativeLikelyhoodRatio;
    relativeRisc = (TP/(TP+FP)/(FP/(FP+TN)));
    
    
end
    