function X_newP = oplsApply(X_old,P_O,W_O)
% Function for applying oPLS
%
% part of the AI(omics)n distribution 
% https://gitlab.com/ai-omics/ai-omics
% (c) 2023 by the authors
% distributed under the conditions listed in the file
% classificationUI2.m and the documentation

X = X_old;
T_newO = [];

for i = 1:size(W_O,2)
    %13
    t_newO = (X*W_O(:,i))/(W_O(:,i)'*W_O(:,i));
    
    %15
    E_newO = X - t_newO*P_O(:,i)';
    
    %16
    T_newO = [T_newO,t_newO];
    X = E_newO;
end

X_newO = T_newO*P_O';
X_newP = X_old - X_newO;
end