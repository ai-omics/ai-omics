function bins = nmrBin1D(specs,varargin)
% Function for binning of NMR spectra
%
% part of the AI(omics)n distribution 
% https://gitlab.com/ai-omics/ai-omics
% (c) 2023 by the authors
% distributed under the conditions listed in the file
% classificationUI2.m and the documentation

%% Parse varargin
p = inputParser;
p.addParameter('ppm',[0.5 9.5]);
p.addParameter('bins',100);
p.addParameter('exclusions',[]);
p.addParameter('deleteExclusions',false);
p.addParameter('optimizedBinning',false);
p.parse(varargin{:});
pars = p.Results;

%% Generate bin list
fprintf("Generating bin list\r\n");
minPpm = min(pars.ppm);
maxPpm = max(pars.ppm);

binCount = pars.bins;
binSize = (maxPpm - minPpm)/(binCount - 1);

binList = zeros(2,binCount);
binPpm = zeros(1,binCount);
for j = 1:binCount
    binList(1,j) = (binSize*(j - 1)) + minPpm - binSize/2;
    binList(2,j) = (binSize*j) + minPpm - binSize/2;
    binPpm(j) = (binList(1,j) + binList(2,j))/2;
end

%% Test for smaller increment of bins than raw data
if specs(1).sw/specs(1).size > binSize
    error('Bins smaller than delta of raw data');
end

%% Extract spectra
specsCount = length(specs);
specsPpm = nmrPpm1D(specs(1));
specsData = zeros(specsCount,length(specs(1).Data));
for j = 1:specsCount
    specsData(j,:) = specs(j).Data;
end

%% Optimize bin list
if length(pars.optimizedBinning) > 1
    binList = pars.optimizedBinning;
    
    for j = 1:binCount
        binPpm(j) = (binList(1,j) + binList(2,j))/2;
    end
elseif pars.optimizedBinning ~= false
    binSlack = binSize*pars.optimizedBinning;
    specsMedian = median(specsData);
    
    for j = 2:binCount
        specsPpmIndex = find((specsPpm > binList(1,j)-binSlack) & (specsPpm < binList(1,j)+binSlack));
        [~,specsLocalMin] = min(specsMedian(specsPpmIndex));
        specsLocalMinPpm = specsPpm(specsPpmIndex(specsLocalMin));
        binList(1,j) = specsLocalMinPpm;
        binList(2,j-1) = specsLocalMinPpm;
    end
    
    for j = 1:binCount
        binPpm(j) = (binList(1,j) + binList(2,j))/2;
    end
end

%% Binning
data = zeros(specsCount,binCount);

reverseStr = '';

for j = 1:specsCount    
    for k = 1:binCount
        specsPpmBool = (specsPpm > binList(1,k)) & (specsPpm < binList(2,k));
        data(j,k) = sum(specsData(j,specsPpmBool))/sum(specsPpmBool);
    end
    
	percentDone = 100 * j / specsCount;
	msg = sprintf('Binning: %3.1f percent done', percentDone);
	fprintf([reverseStr, msg]);
	reverseStr = repmat(sprintf('\b'), 1, length(msg));
end
fprintf('\r\n');

%% Apply exclusions
exclusions = pars.exclusions;
if length(exclusions) > 0
    binsExluded = (binPpm >= exclusions(:,1)) & (binPpm <= exclusions(:,2));
    binsExluded = sum(binsExluded,1) > 0;
    
    if pars.deleteExclusions
        data = data(:,~binsExluded);
        binPpm = binPpm(~binsExluded);
    else
        data(:,binsExluded) = 0;
    end
end

%% Save
bins.DATA = data;
bins.PPM = binPpm;
bins.BinList = binList;
end

