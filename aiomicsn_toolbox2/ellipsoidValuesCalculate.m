function ellipsoidValues = ellipsoidValuesCalculate(score,groups,nStd)
% Function for calculation of elispoid values
% score: matrix with score values
% groups: vector with group allocations of corresponding score rows
% nStd: number of standard deviations for ellipsoid radii
%
% part of the AI(omics)n distribution 
% https://gitlab.com/ai-omics/ai-omics
% (c) 2023 by the authors
% distributed under the conditions listed in the file
% classificationUI2.m and the documentation

groupsDistinct = tabulate(groups);
[~,groupsOrder] = sort(groupsDistinct(:,1));
groupsDistinct = groupsDistinct(groupsOrder,:);
ellipsoidValues = zeros(size(groupsDistinct,1),3,101,101);

for i=1:size(groupsDistinct,1)
    ii = find(ismember(groups,groupsDistinct(i,1)));
    
    % Covariance matrix per group
    covMatrix = cov(score(ii,1:3));
    ellipsoidMean = mean(score(ii,1:3));
    
    % For N standard deviations spread of data, the radii of the eliipsoid will be given by N*SQRT(eigenvalues).
    [eigenvector,eigenvalue] = eig(covMatrix);
    ellipsoidRadius = nStd*sqrt(diag(eigenvalue));
    
    % generate data for "unrotated" ellipsoid
    [ellipsoidXc,ellipsoidYc,ellipsoidZc] = ellipsoid(0,0,0,ellipsoidRadius(1),ellipsoidRadius(2),ellipsoidRadius(3),100);
    
    % rotate data with orientation matrix U and center mu
    ellipsoidA = kron(eigenvector(:,1),ellipsoidXc);
    ellipsoidB = kron(eigenvector(:,2),ellipsoidYc);
    ellipsoidC = kron(eigenvector(:,3),ellipsoidZc);
    
    ellipsoidData = ellipsoidA+ellipsoidB+ellipsoidC;
    ellipsoidN = size(ellipsoidData,2);
    
    ellipsoidValues(i,1,:,:) = ellipsoidData(1:ellipsoidN,:)+ellipsoidMean(1);
    ellipsoidValues(i,2,:,:) = ellipsoidData(ellipsoidN+1:2*ellipsoidN,:)+ellipsoidMean(2);
    ellipsoidValues(i,3,:,:) = ellipsoidData(2*ellipsoidN+1:end,:)+ellipsoidMean(3);
end
end

