function paths = msFilelistGenerate(paths,pathSpectra)
% Generates Filelist from table
%
% part of the AI(omics)n distribution 
% https://gitlab.com/ai-omics/ai-omics
% (c) 2023 by the authors
% distributed under the conditions listed in the file
% classificationUI2.m and the documentation

pathSpectra = split(pathSpectra,',');
if size(pathSpectra,1) > 1
    if size(pathSpectra,2) == 1
        pathSpectra = pathSpectra';
    end
    for j = 1:size(pathSpectra,1)
        paths = strrep(paths,pathSpectra{j,1},pathSpectra{j,2});
    end
else
    paths = strrep(paths,'\\ALNUMED012\Screening\data',pathSpectra{1,1});
end
end