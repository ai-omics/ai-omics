function result = mcroc(probabilities,groupsNumber)
% Function for calculation of multiclass receiver operating characteristic
%
% part of the AI(omics)n distribution 
% https://gitlab.com/ai-omics/ai-omics
% (c) 2023 by the authors
% distributed under the conditions listed in the file
% classificationUI2.m and the documentation

samples = size(probabilities,1);
groups = size(probabilities,2);

% Iterate over groups
for j = 1:groups
    [~,probabilityOrder] = sort(probabilities(:,j),'descend');
    P = sum(groupsNumber == j);
    N = sum(groupsNumber ~= j);
    TPR = zeros(samples,1);
    FPR = zeros(samples,1);
    
    % Iterate over samples and calculate TPR and FPR
    for k = 1:samples
        TPR(k) = sum(groupsNumber(probabilityOrder(1:k)) == j)/P;
        FPR(k) = sum(groupsNumber(probabilityOrder(1:k)) ~= j)/N;
    end
    
    result(j).roc = [TPR,FPR];
end
end