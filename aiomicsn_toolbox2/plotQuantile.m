function plotQuantile(xData,yData,varargin)
% Function for plotting quantiles
%
% part of the AI(omics)n distribution 
% https://gitlab.com/ai-omics/ai-omics
% (c) 2023 by the authors
% distributed under the conditions listed in the file
% classificationUI2.m and the documentation

% Process input parameters
p = inputParser;
p.addParameter('Color',[1 0 0]);
p.addParameter('Mean',false);
p.addParameter('Median',false);
p.addParameter('Quantile',false);
p.addParameter('Interval',false);

p.parse(varargin{:});
pars = p.Results;

% Set parameters
quantileLimits = 0.025:0.025:0.975;

% Calculate quantiles
quantileValues = quantile(yData,quantileLimits);
quantileValues(isnan(quantileValues)) = 0;

% Interval values
if pars.Interval ~= false
    if pars.Interval == true
        intervalLimits = 1;
    else
        intervalLimits = pars.Interval;
    end
    intervalLimits = (1 - erf(intervalLimits/sqrt(2)))/2;
    intervalLimits = [intervalLimits,1-intervalLimits];
    
    intervalValues = quantile(yData,intervalLimits);
end

% Calculate parameters for plot
yLayers  = size(quantileValues,1);
yLayersHalf = (yLayers-rem(yLayers,2))/2;
alpha = 0.6/yLayersHalf;

% Plot
hold on;

if pars.Quantile
    for j = 1:yLayersHalf
        patch([xData fliplr(xData)],[quantileValues(j,:) fliplr(quantileValues(j+1,:))],pars.Color,'FaceAlpha',alpha*j,'EdgeColor','none','LineStyle','none');
        patch([xData fliplr(xData)],[quantileValues(yLayers-j+1,:) fliplr(quantileValues(yLayers-j,:))],pars.Color,'FaceAlpha',alpha*j,'EdgeColor','none','LineStyle','none');
    end
    
end
if pars.Mean
    plot(xData,mean(yData),'Color',pars.Color);
end
if pars.Median
    plot(xData,median(yData),'Color',pars.Color);
end
if pars.Interval
    plot(xData,intervalValues(1,:),':','Color',pars.Color);
    plot(xData,intervalValues(2,:),':','Color',pars.Color);
end
end

