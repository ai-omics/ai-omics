function [origin,barcodeData,data,barcodeDataTest,dataTest,originInterval] = dataRead(sources)
% Function for reading all data
%
% part of the AI(omics)n distribution 
% https://gitlab.com/ai-omics/ai-omics
% (c) 2023 by the authors
% distributed under the conditions listed in the file
% classificationUI2.m and the documentation

barcodeData = [];
data = [];
barcodeDataTest = [];
dataTest = [];
origin = {};
originInterval = [];

for j = 1:size(sources,1)
    switch sources{j,1}
        case 'NMR'
            [origin,barcodeData,data,barcodeDataTest,dataTest,originInterval] = nmrReadTable(origin,barcodeData,data,barcodeDataTest,dataTest,sources{j,2},originInterval);
        case 'MS'
            [origin,barcodeData,data,barcodeDataTest,dataTest,originInterval] = msReadTable(origin,barcodeData,data,barcodeDataTest,dataTest,sources{j,2},originInterval);
        case {'NIR','MIR'}
            [origin,barcodeData,data,barcodeDataTest,dataTest,originInterval] = irReadTable(origin,barcodeData,data,barcodeDataTest,dataTest,sources{j,2},sources{j,1},originInterval);
        case 'XY'
            [origin,barcodeData,data,barcodeDataTest,dataTest,originInterval] = xyReadTable(origin,barcodeData,data,barcodeDataTest,dataTest,sources{j,2},originInterval);
        case 'other'
            [origin,barcodeData,data,barcodeDataTest,dataTest,originInterval] = otherReadTable(origin,barcodeData,data,barcodeDataTest,dataTest,sources{j,2},originInterval);
    end
end
end