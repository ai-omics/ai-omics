function barcode = barcodeRead(table,columnBarcode)
% Function for reading barcodes from table
%
% part of the AI(omics)n distribution 
% https://gitlab.com/ai-omics/ai-omics
% (c) 2023 by the authors
% distributed under the conditions listed in the file
% classificationUI2.m and the documentation

barcode = table(2:end,columnBarcode);
for j = 1:size(barcode,1)
    if ~isnumeric(barcode{j})
        if length(barcode{j}) > 6
            barcodeTemp = barcode{j};
            barcode{j} = barcodeTemp(1:6);
        end
        
        barcode{j} = str2num(barcode{j});
    end
end

barcode = cell2mat(barcode);
end

