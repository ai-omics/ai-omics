function [stats] = plotConfusion(groupsDistinct,cvConfMean,nData,cvRuns,cvShare,AverageFlag)
% Function generating a confusion matrix plot from the cross validation
% data
%
% part of the AI(omics)n distribution 
% https://gitlab.com/ai-omics/ai-omics
% (c) 2023 by the authors
% distributed under the conditions listed in the file
% classificationUI2.m and the documentation

% Only use labels
groupsDistinct = groupsDistinct(:,1);

figure('Name','Calibration - Confusion Matrix');

if AverageFlag == 0
    % AverageFlag = 0 returns the product of Number of CV-Runs * CVShare * 
    % fraction of Confusion Matrix
    cvConfPlot = (round(cvConfMean.*nData.*(cvShare/100).*cvRuns));
    cm = confusionchart(cvConfPlot, groupsDistinct);
    cm.NormalizedValues;
    stats.cm.normalizedValues = cm.NormalizedValues;
    cm.Title = 'Cross-validation Confusion Matrix in rounded absolute numbers over all cross-validation runs';
    stats.cm.title = cm.Title;
    cm.RowSummary = 'row-normalized';
    stats.cm.rowNorm = cm.RowSummary;
    cm.ColumnSummary = 'column-normalized';
    stats.cm.colNorm = cm.ColumnSummary; 
elseif AverageFlag == 1
    % AverageFlag = 1 returns the sample number per field averaged over all
    % cross-validation runs
    cvConfPlot = (round(cvConfMean.*nData));
    cm = confusionchart(cvConfPlot, groupsDistinct);
    cm.NormalizedValues;
    stats.cm.normalizedValues = cm.NormalizedValues;
    cm.Title = 'Cross-validation Confusion Matrix in rounded averaged absolute numbers';
    stats.cm.title = cm.Title;
    cm.RowSummary = 'row-normalized';
    stats.cm.rowNorm = cm.RowSummary;
    cm.ColumnSummary = 'column-normalized';
    stats.cm.colNorm = cm.ColumnSummary;
elseif AverageFlag == 2
    % AverageFlag = 2 returns the relative confusion matrix, where sum over
    % matrix is unity
    cvConfPlot = (round(cvConfMatMean.*100));
    cm = confusionchart(cvConfPlot, groupsDistinct);
    cm.NormalizedValues;
    stats.cm.normalizedValues = cm.NormalizedValues;
    cm.Title = 'Relative cross-validation Confusion Matrix in % (rounded, entire matrix = 100%)';
    stats.cm.title = cm.Title;
    cm.RowSummary = 'row-normalized';
    stats.cm.rowNorm = cm.RowSummary;
    cm.ColumnSummary = 'column-normalized';
    stats.cm.colNorm = cm.ColumnSummary;
else
    % Any other number of AverageFlag returns a relative confusion matrix, 
    % where each class (true) corresponds to unity. 
    cvConfPlot = (round(cvConfMatMean*100*length(groupsDistinct)));
    cm = confusionchart(cvConfPlot, groupsDistinct);
    cm.NormalizedValues;
    stats.cm.normalizedValues = cm.NormalizedValues;
    cm.Title = 'Relative cross-validation Confusion Matrix in % (rounded; each class (true) = 100%)';
    stats.cm.title = cm.Title;
    cm.RowSummary = 'row-normalized';
    stats.cm.rowNorm = cm.RowSummary;
    cm.ColumnSummary = 'column-normalized';
    stats.cm.colNorm = cm.ColumnSummary;
end
end