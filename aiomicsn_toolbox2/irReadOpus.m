function spec = irReadOpus(inputFile, spectrumType, spectrumTypeIndex)
% This function reads the intensities of corresponding wavenumbers of a 
% infrared spectrum from Bruker Opus file format.
%
% part of the AI(omics)n distribution 
% https://gitlab.com/ai-omics/ai-omics
% (c) 2024 by the authors
% distributed under the conditions listed in the file
% classificationUI2.m and the documentation
%
% Inspired by https://github.com/qedsoftware/brukeropusreader

%% Read file
fileId = fopen(inputFile,'rb');
if fileId<1
    error('File not found %s\n', inputFile);
else
    fileChars = fread(fileId,'*char','l')';
end
fclose(fileId);
fileBytes = unicode2native(fileChars);

%% Parse meta data
meta = parseAllMeta(fileBytes);

%% Parse file data
data = parseAllData(fileBytes, meta);

% Save to struct
spec = extractSpectrum(data, spectrumType, spectrumTypeIndex);
end

function meta = parseAllMeta(fileBytes)
headerLength = 504;
firstCursorPosition = 25;
metaBlockSize = 12;

meta = struct([]);
cursor = firstCursorPosition;
while cursor + metaBlockSize <= headerLength
    dataType = readDataType(fileBytes, cursor);
    channelType = readChannelType(fileBytes, cursor);
    textType = readTextType(fileBytes, cursor);
    chunkSize = readChunkSize(fileBytes, cursor);
    offset = readOffset(fileBytes, cursor);
    
    if offset <= 0
        break;
    end
    
    metaBlock = {};
    metaBlock.dataType = dataType;
    metaBlock.channelType = channelType;
    metaBlock.textType = textType;
    metaBlock.chunkSize = chunkSize;
    metaBlock.offset = offset;
    
    meta = [meta, metaBlock];
    
    nextOffset = offset + 4 * chunkSize;
    if nextOffset > length(fileBytes)
        break;
    end
    
    cursor = cursor + metaBlockSize;
end
end

function value = readDataType(fileBytes, cursor)
value = typecast(fileBytes(cursor),'uint8');
end

function value = readChannelType(fileBytes, cursor)
value = typecast(fileBytes(cursor + 1),'uint8');
end

function value = readTextType(fileBytes, cursor)
value = typecast(fileBytes(cursor + 2),'uint8');
end

function value = readChunkSize(fileBytes, cursor)
start = cursor + 4;
stop = cursor + 7;
value = typecast(fileBytes(start:stop),'int32');
end

function value = readOffset(fileBytes, cursor)
start = cursor + 8;
stop = cursor + 11;
value = typecast(fileBytes(start:stop),'int32');
end

function value = readChunk(fileBytes,  meta)
start = meta.offset + 1;
stop = start + 4 * meta.chunkSize - 1;
value = fileBytes(start:stop);
end

function data = parseAllData(fileBytes, meta)
data = struct([]);

for j = 1:length(meta)
    parsedData = parseData(fileBytes, meta(j));
    data = [data, parsedData];
end
end

function data = parseData(fileBytes, meta)
name = 'Unknown';
value = 0;

switch meta.dataType
    case 0
        switch meta.textType
            case 8
                name = 'Info Block';
                value = parseParam(fileBytes, meta);
            case 104
                name = 'History';
                value = parseText(fileBytes, meta);
            case 152
                name = 'Curve Fit';
                value = parseText(fileBytes, meta);
            case 168
                name = 'Signature';
                value = parseText(fileBytes, meta);
            case 240
                name = 'Integration method';
                value = parseText(fileBytes, meta);
            otherwise
                name = 'Text Information';
                value = parseText(fileBytes, meta);
        end
    case 7
        switch meta.channelType
            case 4
                name = 'ScSm';
            case 8
                name = 'IgSm';
            case 12
                name = 'PhSm';
            case 56
                name = 'PwSm';
        end
        value = parseSeries(fileBytes, meta);
    case 11
        switch meta.channelType
            case 4
                name = 'ScRf';
            case 8
                name = 'IgRf';
            case 12
                name = 'PhRf';
            case 56
                name = 'PwRf';
        end
        value = parseSeries(fileBytes, meta);
    case 15
        name = 'AB';
        value = parseSeries(fileBytes, meta);
    case 23
        switch meta.channelType
            case 4
                name = 'ScSm Data Parameter';
            case 8
                name = 'IgSm Data Parameter';
            case 12
                name = 'PhSm Data Parameter';
            case 56
                name = 'PwSm Data Parameter';
        end
        value = parseParam(fileBytes, meta);
    case 27
        switch meta.channelType
            case 4
                name = 'ScRf Data Parameter';
            case 8
                name = 'IgRf Data Parameter';
            case 12
                name = 'PhRf Data Parameter';
            case 56
                name = 'PwRf Data Parameter';
        end
        value = parseParam(fileBytes, meta);
    case 31
        name = 'AB Data Parameter';
        value = parseParam(fileBytes, meta);
    case 32
        name = 'Instrument';
        value = parseParam(fileBytes, meta);
    case 40
        name = 'Instrument (Rf)';
        value = parseParam(fileBytes, meta);
    case 48
        name = 'Acquisition';
        value = parseParam(fileBytes, meta);
    case 56
        name = 'Acquisition (Rf)';
        value = parseParam(fileBytes, meta);
    case 64
        name = 'Fourier Transformation';
        value = parseParam(fileBytes, meta);
    case 72
        name = 'Fourier Transformation (Rf)';
        value = parseParam(fileBytes, meta);
    case 96
        name = 'Optic';
        value = parseParam(fileBytes, meta);
    case 104
        name = 'Optic (Rf)';
        value = parseParam(fileBytes, meta);
    case 160
        name = 'Sample';
        value = parseParam(fileBytes, meta);
    otherwise
        error('Unknown Block Type');
end

data = {};
data.name = name;
data.value = value;
end

function params = parseParam(fileBytes, meta)
cursor = 1;
chunk = readChunk(fileBytes, meta);
params = struct([]);

while true
    paramName = native2unicode(chunk(cursor:cursor + 2), 'latin1');
    if strcmp(paramName, 'END')
        break;
    end
    
    typeIndex = typecast(chunk(cursor + 4:cursor + 5),'uint16');
    paramType = '';
    switch typeIndex
        case 0
            paramType = 'int';
        case 1
            paramType = 'float';
        case {2, 3, 4}
            paramType = 'str';
        otherwise
            error('Unknown parameter type');
    end
    paramSize = typecast(chunk(cursor + 6:cursor + 7),'uint16');
    paramBytes = chunk(cursor + 8:cursor + 2 * paramSize + 7);
    
    paramValue = 0;
    switch paramType
        case 'int'
            paramValue = typecast(paramBytes,'int32');
        case 'float'
            paramValue = typecast(paramBytes,'double');
        case 'str'
            paramValue = native2unicode(paramBytes, 'latin1');
    end
    
    param = {};
    param.name = paramName;
    param.value = paramValue;
    param.type = paramType;
    param.size = paramSize;
    
    cursor = cursor + 2 * paramSize + 8;
    params = [params, param];
end
end

function text = parseText(fileBytes, meta)
chunk = readChunk(fileBytes, meta);
text = native2unicode(chunk, 'latin1');
end

function series = parseSeries(fileBytes, meta)
chunk = readChunk(fileBytes, meta);
series = typecast(chunk,'single');
end

function result = extractSpectrum(data, spectrumIdentifier, spectrumIdentifierIndex)
% Find spectrum data and its meta data
spectrumIndex = false(1, length(data));
metaIndex = false(1, length(data));
for j = 1:length(data)
    pos = strfind(data(j).name, spectrumIdentifier);
    if(isempty(pos) || pos(1) ~= 1)
        continue;
    end
    if(length(data(j).name) == length(spectrumIdentifier))
        spectrumIndex(j) = true;
    else
        metaIndex(j) = true;
    end
end

spectrumIndex = find(spectrumIndex);
metaIndex = find(metaIndex);

if(isempty(spectrumIndex) || isempty(metaIndex))
    error('Spectrum identifier not found');
end

if spectrumIdentifierIndex >= 0
    tempSpectrumIdentifierIndex = spectrumIdentifierIndex;
else
    tempSpectrumIdentifierIndex = length(spectrumIndex) + 1 + spectrumIdentifierIndex;
end

if tempSpectrumIdentifierIndex < 0 || tempSpectrumIdentifierIndex > length(spectrumIndex)
    error(['No spectrum idetifier at position ', num2str(spectrumIdentifierIndex)]);
end

spectrum = data(spectrumIndex(tempSpectrumIdentifierIndex)).value;
meta = data(metaIndex(tempSpectrumIdentifierIndex)).value;

% Construct x-axis
fxv = 0;
lxv = 0;
npt = 0;

for j = 1:length(meta)
    if(strcmp(meta(j).name, 'FXV'))
        fxv = meta(j).value;
    end
    if(strcmp(meta(j).name, 'LXV'))
        lxv = meta(j).value;
    end
    if(strcmp(meta(j).name, 'NPT'))
        npt = meta(j).value;
    end
end

wavenumber = linspace(fxv, lxv, npt);

% Slice spectrum as AB can contain more zero values at the end
intensity = spectrum(1:npt);

result = {};
result.intensity = intensity;
result.wavenumber = wavenumber;
end