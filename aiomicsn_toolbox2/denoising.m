function [noisePositions, denoisedData] = denoising(data,noiseStart,noiseEnd,stddevMultiplier,maxMultiplier)
% Script for finding noise regions, i.e. regions containing no signals
% method 1.0: fill noise regions with zeros
% method 1.1: eliminate zeros
% method 2.0: in addition identify signal contianing regions which do not
% show variance exciding noise bandwidth
% method 2.1: eliminate also these regions
%
% part of the AI(omics)n distribution 
% https://gitlab.com/ai-omics/ai-omics
% (c) 2023,2024 by the authors
% distributed under the conditions listed in the file
% classificationUI2.m and the documentation

%% Settings and general definitions
% the algorithm assumes that data are baseline corrected with an average
% baseline at 0 magnitude. A reference region with noise data has to be
% defined. 

eliminateSignalsWithoutVarianceBoolean = 1;
% maxMultiplier = 2;
% stddevMultiplier = 3;
eliminateNoiseBoolean = 1;

% define noise region and noise properties
denoisedData = data; 
noiseRegionIndices = noiseStart:noiseEnd;                     % define noise region
denoisingMin = min(denoisedData(noiseRegionIndices));     % min for each column over noise region
denoisingMax = max(denoisedData(noiseRegionIndices));     % max for each column
denoisingMean = mean(denoisedData(noiseRegionIndices));         % mean of each column
denoisingStdDev = std(denoisedData(noiseRegionIndices));              % std deviation of each column

% future version: baselinecorrection
% denoisedData = baselinecorrect(denoisedData, parameters)

% correct for baseline offset - note: flat baseline assumed
denoisedData = denoisedData - denoisingMean;

% replace data in noise region with zeros
noisePosition = zeros(1:size(denoisedData,2);   % generate vector of length No of columsn in matrix, fill with Zeros
noisePosition(noiseRegionIndices) = ones;       % positions with noise will be flagged with "1", starting with noiseRegion
denoisedData(:,noiseRegionIndices) = zeros;     % fill noise region in date set with Zeros

%% Find noise data and (if selected) signals which do not exhibit a signifcantly larger variance than noise (and hence are unlikely to correlate with problem)
for i = 1:(noiseStart-1)      % first loop from data point 1 to noiseStart-1
    dataPointMax = max(denoisedData(i));
    if dataPointMax(i) < denoisingMax*maxMultiplier | dataPointMax(i) < (denoisingMean+denoisingStdDev*stddevMultiplier)
        noisePosition(i) = 1;
        denoisedData(:,i) = zeros;
    end
    if eliminateSignalsWithoutVarianceBoolean      % identify data points with signals that do not contain variance
        % exceeding the scaled variance of noise data
        dataPointMean = mean(denoisedData(i));
        dataPointMax = denoisedData(i)-dataPointMean(i);   % center datapoint at baseline
        dataPointStd = std(denoisedData(i));
        if (dataPointMax(i) < denoisingMax*maxMultiplier | dataPointMax(i) < (denoisingMean+denoisingStdDev*stddevMultiplier)) && dataPointStd < (stddevMultiplier-1)*denoisingStdDev
            noisePosition(i) = 2;                          % different flag allows discriminaton of noise and data without significant information
            denoisedData(:,i) = zeros;
        end
    end
end
    
for i = (noiseEnd+1):size(denoisedData,2)    % second loop from data point noiseend+1 to end of dataset
    dataPointMax = max(denoisedData(i));
    if dataPointMax(i) < denoisingMax*maxMultiplier | dataPointMax(i) < (denoisingMean+denoisingStdDev*stddevMultiplier)
        noisePosition(i) = 1;
        denoisedData(:,i) = zeros;
    end
    if eliminateSignalsWithoutVarianceBoolean      % identify data points with signals that do not contain variance
        % exceeding the scaled variance of noise data
        dataPointMean = mean(denoisedData(i));
        dataPointMax = denoisedData(i)-dataPointMean(i);   % center datapoint at baseline
        dataPointStd = std(denoisedData(i));
        if (dataPointMax(i) < denoisingMax*maxMultiplier | dataPointMax(i) < (denoisingMean+denoisingStdDev*stddevMultiplier)) && dataPointStd < (stddevMultiplier-1)*denoisingStdDev
            noisePosition(i) = 2;
            denoisedData(:,i) = zeros;
        end
    end
end    

%% Eliminate columns with zeros
if eliminateNoiseBoolean
    dataSetSize = size(denoisedData,2);
    noiseCounter = 0;
    for i = 0:(dataSetSize-1)
        if sum(denoisedData(:,(dataSetSize-i)) = 0
          denoisedData(:,(dataSetSize-i)) = [];         % start to reduce data matrix from the end
          noiseCounter = noiseCounter+1;
        end
    end
    fprintf("Noiseregion corresponds to ",size(noiseRegionIndices)," data points.\r\n");
    fprintf("Additional ",noiseCounter," data points without significant information were removed.\r\n");
    fprintf("In total, ",(size(noiseRegionIndices)+noiseCounter," data points were removed from originally ",size(data,2)," .\r\n");
    fprintf("The new data matrix now has ",size(denoisedData,2)," data points.\r\n");
end
end