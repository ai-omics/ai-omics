function [groupsTest,groupsTestProbability,stats] = classificationPlsDaPredict(model,stats,dataTest,varargin)
% Script for classificationv predictions via (O)PLS-DA
%
% part of the AI(omics)n distribution 
% https://gitlab.com/ai-omics/ai-omics
% (c) 2023 by the authors
% distributed under the conditions listed in the file
% classificationUI2.m and the documentation

%% Apply feature selection
dataTest = dataTest(:,stats.featureSelectionSorting);

%% (O)PLS-DA predictive
fprintf("Applying predictive model\r\n");

% Apply to testdata
% unit-variance scaling
dataTest = dataTest ./ repmat(model.s,size(dataTest,1),1);

% subtract mean
dataTest = dataTest - repmat(model.m,size(dataTest,1),1);

% OPLS
if model.oplsBoolean
    dataTest = oplsApply(dataTest,model.P_O,model.W_O);
end

% Apply PLS model
testXScores = dataTest/model.modelXLoadings';

% Predicting Test samples
fprintf("Predicting Test samples\r\n");
testPredictionNumeric = [ones(size(dataTest,1),1),dataTest]*model.modelPls;
[~,testPrediction] = max(testPredictionNumeric,[],2);
groupsTest = model.groupsDistinct(testPrediction,1);

groupsTestProbability = zeros(size(dataTest,1),size(model.groupsDistinct,1));
for j = 1:size(model.groupsDistinct,1)
    groupsTestProbability(:,j) = normcdf(testPredictionNumeric(:,j),model.modelPredictionMean(j),model.modelPredictionStd(j));
end
stats.prediction.groups = groupsTest;
stats.prediction.probabilities = groupsTestProbability;

% Calculate distances
testDistributionEuclidean = zeros(size(dataTest,1), size(model.groupsDistinct,1));
testDistributionMahalanobis = zeros(size(dataTest,1), size(model.groupsDistinct,1));
if stats.settings.enableMahalanobis == 1
    for j = 1:size(model.groupsDistinct,1)
        n = ismember(model.groups,model.groupsDistinct{j,1});
        testDistributionEuclidean(:, j) = pdist2(testXScores, mean(model.cvPlotScore(n, :), 1),'euclidean');
        testDistributionMahalanobis(:, j) = mahal(testXScores, model.cvPlotScore(n,:));
    end
end
stats.prediction.distributionEuclidean = testDistributionEuclidean;
stats.prediction.distributionMahalanobis = testDistributionMahalanobis;


%% Plot
if model.plotBoolean
    CM = lines(size(model.groupsDistinct,1));
    
    fprintf("Plotting prediction\r\n");
    figure('Name','Model - Prediction Testset');
    hold on;
    if stats.settings.displayCalibration == 1
      for i=1:size(model.groupsDistinct,1)
          ii = find(ismember(model.groups,model.groupsDistinct(i,1)));
          plot3(model.cvPlotScore(ii,1),model.cvPlotScore(ii,2),model.cvPlotScore(ii,3),'ko','MarkerFaceColor',CM(i,:));
      end
    end
    for i=1:size(model.groupsDistinct,1)
        surf(squeeze(model.cvEllipsoidValues(i,1,:,:)),squeeze(model.cvEllipsoidValues(i,2,:,:)),squeeze(model.cvEllipsoidValues(i,3,:,:)),'EdgeColor',CM(i,:),'LineStyle',':','FaceColor',CM(i,:),'FaceAlpha',0.3);
    end
    if model.textBoolean
        text(model.cvPlotScore(:,1),model.cvPlotScore(:,2),model.cvPlotScore(:,3),num2str([1:1:size(model.cvPlotScore,1)]'));
    end
    for i=1:size(model.groupsDistinct,1)
        ii = find(ismember(groupsTest,model.groupsDistinct(i,1)));
        plot3(testXScores(ii,1),testXScores(ii,2),testXScores(ii,3),'ks','MarkerFaceColor',CM(i,:),'MarkerSize',14);
    end
    if model.textBoolean
        text(testXScores(:,1),testXScores(:,2),testXScores(:,3),num2str([1:1:size(testXScores,1)]'));
    end
    
    axis vis3d;
    box on;
    grid on;
    legend(model.groupsDistinct(:,1));
    xlabel('Latent variable 1');
    ylabel('Latent variable 2');
    zlabel('Latent variable 3');
    view(30,10);
    hold off;
end
end