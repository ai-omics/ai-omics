function [origin,barcodeData,dataOld,barcodeDataTest,dataTestOld,originInterval] = dataAppend(origin,originTemp,barcodeData,dataOld,barcode,data,barcodeDataTest,dataTestOld,barcodeTest,dataTest,originInterval)
% Function for appending data
%
% part of the AI(omics)n distribution 
% https://gitlab.com/ai-omics/ai-omics
% (c) 2023 by the authors
% distributed under the conditions listed in the file
% classificationUI2.m and the documentation

% Find matches
[~,barcodeDataCommon,barcodeCommon] = intersect(barcodeData,barcode);
[~,barcodeDataTestCommon,barcodeTestCommon] = intersect(barcodeDataTest,barcodeTest);

dataSize = size(dataOld);
dataTestSize = size(dataTestOld);

barcodeDifferent = setxor([1:size(barcode,1)]',barcodeCommon);
barcodeTestDifferent = setxor([1:size(barcodeTest,1)]',barcodeTestCommon);

% Combine data
dataTemp = NaN(dataSize(1)+length(barcodeDifferent),dataSize(2)+size(data,2));
dataTestTemp = NaN(dataTestSize(1)+length(barcodeTestDifferent),dataTestSize(2)+size(dataTest,2));

dataTemp(1:dataSize(1),1:dataSize(2)) = dataOld;
dataTestTemp(1:dataTestSize(1),1:dataTestSize(2)) = dataTestOld;

dataOld = dataTemp;
dataTestOld = dataTestTemp;

dataOld(barcodeDataCommon,dataSize(2)+1:size(dataOld,2)) = data(barcodeCommon,:);
dataTestOld(barcodeDataTestCommon,dataTestSize(2)+1:size(dataTestOld,2)) = dataTest(barcodeTestCommon,:);

dataOld(dataSize(1)+1:size(dataOld,1),dataSize(2)+1:size(dataOld,2)) = data(barcodeDifferent,:);
dataTestOld(dataTestSize(1)+1:size(dataTestOld,1),dataTestSize(2)+1:size(dataTestOld,2)) = dataTest(barcodeTestDifferent,:);

barcodeData = [barcodeData;barcode(barcodeDifferent)];
barcodeDataTest = [barcodeDataTest;barcodeTest(barcodeTestDifferent)];

% Save origin
origin = [origin,originTemp];

% Save origin interval
originInterval = [originInterval;dataSize(2)+1,size(dataOld,2)];
end

