function confusionMatrix = normalizeConfusionMatrix(confusionMatrixInput,dimension)
% Fuction to normalize a confusion matrix
%
% part of the AI(omics)n distribution 
% https://gitlab.com/ai-omics/ai-omics
% (c) 2023 by the authors
% distributed under the conditions listed in the file
% classificationUI2.m and the documentation

% dim : {1, 2, 'all'}, default=2
%   Normalizes confusion matrix over the rows (1), columns (2)
%   or all the population.

%% Normalize
if strcmp(dimension,'all')
    n = sum(confusionMatrixInput,'all');
    confusionMatrix = confusionMatrixInput/n;
elseif dimension == 2
    n = sum(confusionMatrixInput,1);
    nNull = find(n==0);
    % Set column to NaN if no samples are present
    n(nNull) = NaN;
    confusionMatrix = confusionMatrixInput./n;
elseif dimension == 1
    n = sum(confusionMatrixInput,2);
    nNull = find(n==0);
    % Set row to NaN if no samples are present
    n(nNull) = NaN;
    confusionMatrix = confusionMatrixInput./n;
else
    error('normalizeConfusionMatrix: wrong dimension set')
end
end