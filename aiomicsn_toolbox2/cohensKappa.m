function [kappa,varargout] = cohensKappa(confusionMatrix,varargin)
% Function for calculation of Cohen's kappa 
%
% part of the AI(omics)n distribution 
% https://gitlab.com/ai-omics/ai-omics
% (c) 2023 by the authors
% distributed under the conditions listed in the file
% classificationUI2.m and the documentation
%
% according to 10.1177/001316446002000104

%% Set parameters
[m,n] = size(confusionMatrix);

if m~=n
    error('cohensKappa: matrix has to be square');
end

if length(varargin) > 0
    confusionMatrixStd = varargin{1};
end

%% Get p0
p0 = sum(diag(confusionMatrix))/sum(confusionMatrix,'all');

if length(varargin) > 0
    p0Std = sqrt(sum(diag(confusionMatrixStd).^2))/sum(confusionMatrix,'all');
end

%% Get horizontal totals
hH = sum(confusionMatrix,2);
if length(varargin) > 0
    hHStd = sqrt(sum(confusionMatrixStd.^2,2));
end

%% Get vertical totals
hV = sum(confusionMatrix,1);
if length(varargin) > 0
    hVStd = sqrt(sum(confusionMatrixStd.^2,1));
end

%% Get pC
pC = 0;
for j = 1:n
    pC = pC + hH(j)*hV(j);
end
pC = pC/sum(confusionMatrix,'all')^2;

if length(varargin) > 0
    pCStd = 0;
    for j = 1:n
        pCStd = pCStd + hV(j)^2*hHStd(j)^2 + hH(j)^2*hVStd(j)^2;
    end
    pCStd = sqrt(pCStd)/sum(confusionMatrix,'all')^2;
end

%% Get kappa
kappa = (p0 - pC)/(1 - pC);

if length(varargin) > 0
    kappaStd = sqrt((1/(1 - pC))^2*p0Std^2 + ((p0 - 1)/(1 - pC)^2)^2*pCStd^2);
    varargout{1} = kappaStd;
end
end