function [groupsTest,groupsTestProbability,stats] = classificationRawPredict(model,stats,dataTest,varargin)
% Script for classification predictions via raw values/LDA
%
% part of the AI(omics)n distribution 
% https://gitlab.com/ai-omics/ai-omics
% (c) 2023 by the authors
% distributed under the conditions listed in the file
% classificationUI2.m and the documentation
%% Predictive model
% Apply to testdata
% Kruskal-Wallis-selection of significant variables
dataTest = dataTest(:,stats.featureSelectionSorting);

if model.ldaBoolean | model.qdaBoolean | model.knnBoolean
% unit-variance scaling
dataTest = dataTest ./ repmat(model.s,size(dataTest,1),1);

% subtract mean
dataTest = dataTest - repmat(model.m,size(dataTest,1),1);
end

groupsTestProbability = {};

if model.edcBoolean
    % Predicting test samples
    fprintf("Predicting Test samples\r\n");
    testDistributionEuclidean = pdist2(dataTest,model.modelMean,'euclidean');
    [~,testPrediction]=min(testDistributionEuclidean,[],2);
    groupsTestProbability = NaN(size(dataTest,1),size(model.groupsDistinct,1));
    for i = 1:size(model.groupsDistinct,1)
        groupsTestProbability(:,i) = mvnpdf(dataTest,model.modelMean(i,:),model.modelMeanCov{i});
    end
    groupsTest = model.groupsDistinct(testPrediction,1);
    testPlotScore = dataTest;
end
if model.ldaBoolean | model.qdaBoolean | model.knnBoolean
    % Predict test samples
    fprintf("Predicting Test samples\r\n");
    [groupsTest,groupsTestProbability,~] = predict(model.modelDa,dataTest);
end
stats.prediction.groups = groupsTest;
stats.prediction.probabilities = groupsTestProbability;

if model.ldaBoolean
    testPlotScore = dataTest*model.ldaEigenvectors;
end
if model.qdaBoolean
    for j = 1:size(model.groupsDistinct,1)
        testPlotScore(ismember(groupsTest,model.groupsDistinct{j,1}),:) = dataTest(ismember(groupsTest,model.groupsDistinct{j,1}),:)*squeeze(model.ldaEigenvectors(:,:,j));
    end
end
if model.knnBoolean
    testPlotScore = dataTest;
end

% Calculate distances
testDistributionEuclidean = zeros(size(dataTest,1), size(model.groupsDistinct,1));
testDistributionMahalanobis = zeros(size(dataTest,1), size(model.groupsDistinct,1));
if stats.settings.enableMahalanobis == 1
    for j = 1:size(model.groupsDistinct,1)
        n = ismember(model.groups,model.groupsDistinct{j,1});
        testDistributionEuclidean(:, j) = pdist2(testPlotScore, mean(model.modelPlotScore(n, :), 1),'euclidean');
        testDistributionMahalanobis(:, j) = mahal(testPlotScore, model.modelPlotScore(n,:));
    end
end
stats.prediction.distributionEuclidean = testDistributionEuclidean;
stats.prediction.distributionMahalanobis = testDistributionMahalanobis;

%% Plot
if model.plotBoolean
    CM = lines(size(model.groupsDistinct,1));
    
    fprintf("Plotting prediction\r\n");
    figure('Name','Model - Prediction Testset');
    hold on;
    if stats.settings.displayCalibration == 1
      for i=1:size(model.groupsDistinct,1)
          ii = find(ismember(model.groups,model.groupsDistinct(i,1)));
          plot3(model.modelPlotScore(ii,1),model.modelPlotScore(ii,2),model.modelPlotScore(ii,3),'ko','MarkerFaceColor',CM(i,:));
      end
    end  
    for i=1:size(model.groupsDistinct,1)
        surf(squeeze(model.ellipsoidValues(i,1,:,:)),squeeze(model.ellipsoidValues(i,2,:,:)),squeeze(model.ellipsoidValues(i,3,:,:)),'EdgeColor',CM(i,:),'LineStyle',':','FaceColor',CM(i,:),'FaceAlpha',0.3);
    end
    if model.textBoolean
        text(model.modelPlotScore(:,1),model.modelPlotScore(:,2),model.modelPlotScore(:,3),num2str([1:1:size(model.modelPlotScore,1)]'));
    end
    for i=1:size(model.groupsDistinct,1)
        ii = find(ismember(groupsTest,model.groupsDistinct(i,1)));
        plot3(testPlotScore(ii,1),testPlotScore(ii,2),testPlotScore(ii,3),'ks','MarkerFaceColor',CM(i,:),'MarkerSize',14);
    end
    if model.textBoolean
        text(testPlotScore(:,1),testPlotScore(:,2),testPlotScore(:,3),num2str([1:1:size(testPlotScore,1)]'));
    end
    
    axis vis3d;
    box on;
    grid on;
    legend(model.groupsDistinct(:,1));
    xlabel('Dimension 1');
    ylabel('Dimension 2');
    zlabel('Dimension 3');
    view(30,10);
    hold off;
end
end