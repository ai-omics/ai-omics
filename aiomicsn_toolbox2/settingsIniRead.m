function parameter = settingsIniRead(settingsIni,parameter)
% Reads the parameter listed from the setting-Ini file
%
% part of the AI(omics)n distribution 
% https://gitlab.com/ai-omics/ai-omics
% (c) 2023 by the authors
% distributed under the conditions listed in the file
% classificationUI2.m and the documentation
parameterName = inputname(2);

if ismember(parameterName,settingsIni)
    settingsIniIndex = find(strcmp(parameterName,settingsIni));
    settingsIniIndex = settingsIniIndex(1);
    if isnumeric(settingsIniIndex(1)+1)
        warning(['settingsIniRead: Parameter ',parameterName,' overwritten by settings.ini!']);
        parameter = str2double(settingsIni{settingsIniIndex+1});
    end
end
end

