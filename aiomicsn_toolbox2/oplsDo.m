function [T_O,P_O,W_O,X_P] = oplsDo(X_old,Y)
% Function OPLS
%
% part of the AI(omics)n distribution 
% https://gitlab.com/ai-omics/ai-omics
% (c) 2023 by the authors
% distributed under the conditions listed in the file
% classificationUI2.m and the documentation

%,T_pcaO,P_pcaO
plsKomp = 10;

%1
W = zeros(size(X_old,2),size(Y,2));
for i=1:size(Y,2)
    W(:,i) = (X_old'*Y(:,i))/(Y(:,i)'*Y(:,i));
end
[P_W,T_W,~] = pca(W,'Centered',false);

%2
i=1;

while i <= size(T_W,2)
    if sum(T_W(:,i).^2)/sum(sum(W.^2)) < 1E-10
        i=i+1;
        break;
    end
    i=i+1;
end

T_W = T_W(:,1:i-1);
P_W = P_W(:,1:i-1);

E_W = W - T_W*P_W';

T_O = [];
P_O = [];
W_O = [];
T_pls = [];
W_pls = [];
P_pls = [];

X = X_old;
%4
for i=1:plsKomp
    u_old = Y(:,1);
    
    while 1
        %5
        w = (X'*u_old)/(u_old'*u_old);
        %6
        w = w/norm(w);
        %7
        t = (X*w)/(w'*w);
        %8
        c = (Y'*t)/(t'*t);
        %9
        u_new = (Y*c)/(c'*c);
        
        if norm(u_new-u_old)/norm(u_new) < 1E-10
            break;
        end
        u_old = u_new;
    end
    
    %10
    p = (X'*t)/(t'*t);
    %11
    for j=1:size(T_W,2)
        p = p-((T_W(:,j)'*p)/(T_W(:,j)'*T_W(:,j)))*T_W(:,j);
    end
    w_O = p;
    %12
    w_O = w_O/norm(w_O);
    %13
    t_O = (X*w_O)/(w_O'*w_O);
    %14
    p_O = (X'*t_O)/(t_O'*t_O);
    %15
    E_O = X-t_O*p_O';
    %16
    T_O = [T_O,t_O];
    P_O = [P_O,p_O];
    W_O = [W_O,w_O];
    X = E_O;
    %17
    %E = X-t*p';
    %F = Y-t*c';
    %T_pls = [T_pls,t];
    %P_pls = [P_pls,p];
    %W_pls = [W_pls,w];
    
    %if i < plsKomp
    %    X = E;
    %    Y = F;
    %end   
end

%18
X_O = T_O*P_O';
X_P = X_old - X_O;
%19
%[P_pcaO,T_pcaO,~] = pca(X_O,'Centered',false);
%E_pcaO = X_O - T_pcaO*P_pcaO';
end