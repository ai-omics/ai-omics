function [preprocessedData, stats] = preProcessing(data, stats)
% Script for data-pretreatment (transformation, then scaling,then
% normalization)
%
% part of the AI(omics)n distribution 
% https://gitlab.com/ai-omics/ai-omics
% (c) 2023,2024 by the authors
% distributed under the conditions listed in the file
% classificationUI2.m and the documentation

%% settings
% originalData = data;      when calling function prepProcessing orignal
% data shall be saved to stats.originalData; proProcessedData shall also be
% saved to data to have comparison. 

smoothDataMethod = 0;
externalScalingBoolean = 0;
unityScalingBoolean = 0;
minmaxScalingBoolean = 0;
regionScalingBoolean = 0;
regionScalingStart = 100;
regionScalingEnd = 200;
transformInversBoolean = 0;
transformLog = 0;
transformPowerBoolean = 0;
transformDeviation = 0;
meanCenteringBoolean = 0;
medianCenteringBoolean = 0;
zScoreScalingBoolean = 0;
zScoreScalingRobustBoolean = 0;
madScalingBoolean = 0;
irqScalingBoolean = 0;
rangeScalingBoolean = 0;
vastScalingBoolean = 0;
paretoScalingBoolean = 1;
levelScalingBoolean = 0;
pqnBoolean = 0;

if isfile('settings.ini')
    settingsIni = textread('settings.ini','%s','commentstyle','shell');  
    settingsIni = strrep(settingsIni,':','');
    smoothDataMethod = settingsIniRead(settingsIni,smoothDataMethod);
    externalScalingBoolean = settingsIniRead(settingsIni,externalScalingBoolean);
    unityScalingBoolean = settingsIniRead(settingsIni,unityScalingBoolean);
    minmaxScalingBoolean = settingsIniRead(settingsIni,minmaxScalingBoolean);
    regionScalingBoolean = settingsIniRead(settingsIni,regionScalingBoolean);
    regionScalingStart = settingsIniRead(settingsIni,regionScalingStart);
    regionScalingStart = settingsIniRead(settingsIni,regionScalingStart);
    transformInversBoolean = settingsIniRead(settingsIni,transformInversBoolean);
    transformLog = settingsIniRead(settingsIni,transformLog);
    transformPowerBoolean = settingsIniRead(settingsIni,transformPowerBoolean);
    transformDeviation =  settingsIniRead(settingsIni,transformDeviation);
    meanCenteringBoolean = settingsIniRead(settingsIni,meanCenteringBoolean);
    medianCenteringBoolean = settingsIniRead(settingsIni,medianCenteringBoolean);
    zScoreScalingBoolean = settingsIniRead(settingsIni,zScoreScalingBoolean);
    zScoreScalingRobustBoolean = settingsIniRead(settingsIni,zScoreScalingRobustBoolean);
    madScalingBoolean = settingsIniRead(settingsIni,madScalingBoolean);
    irqScalingBoolean = settingsIniRead(settingsIni,irqScalingBoolean);
    rangeScalingBoolean = settingsIniRead(settingsIni,rangeScalingBoolean);
    vastScalingBoolean = settingsIniRead(settingsIni,vastScalingBoolean);
    paretoScalingBoolean = settingsIniRead(settingsIni,paretoScalingBoolean);
    levelScalingBoolean = settingsIniRead(settingsIni,levelScalingBoolean);
    pqnBoolean = settingsIniRead(settingsIni,pqnBoolean);
end


%% writing settings
stats.settings.preprocessing.smoothDataMethod = smoothDataMethod; 
stats.settings.preprocessing.externalScalingBoolean = externalScalingBoolean; 
stats.settings.preprocessing.unityScalingBoolean = unityScalingBoolean;   
stats.settings.preprocessing.minmaxScalingBoolean = minmaxScalingBoolean;  
stats.settings.preprocessing.regionScalingBoolean = regionScalingBoolean;   
stats.settings.preprocessing.regionScalingStart = regionScalingStart;
stats.settings.preprocessing.regionScalingEnd = regionScalingEnd;
stats.settings.preprocessing.transformInversBoolean = transformInversBoolean;    
stats.settings.preprocessing.transformLog = transformLog; 
stats.settings.preprocessing.transformPowerBoolean = transformPowerBoolean; 
stats.settings.preprocessing.transformDeviation = transformDeviation;
stats.settings.preprocessing.meanCenteringBoolean = meanCenteringBoolean;     
stats.settings.preprocessing.medianCenteringBoolean = medianCenteringBoolean;    
stats.settings.preprocessing.zScoreScalingBoolean = zScoreScalingBoolean;     
stats.settings.preprocessing.zScoreScalingRobustBoolean = zScoreScalingRobustBoolean;    
stats.settings.preprocessing.madScalingBoolean = madScalingBoolean;  
stats.settings.preprocessing.irqScalingBoolean = irqScalingBoolean;
stats.settings.preprocessing.rangeScalingBoolean = rangeScalingBoolean;     
stats.settings.preprocessing.vastScalingBoolean = vastScalingBoolean;    
stats.settings.preprocessing.paretoScalingBoolean = paretoScalingBoolean;     
stats.settings.preprocessing.levelScalingBoolean = levelScalingBoolean;     
stats.settings.preprocessing.pqnBoolean = pqnBoolean;

%%
preprocessedData = data;

if(~isfield(stats, 'preprocessing'))
    % External scaling vector
    scalingVector = ones(1,size(data,2)); % for the moment ... set to 1
    stats.preprocessing.scalingVector = scalingVector;

    % Calculate commonly used parameters (column-wise)
    dataMin = min(preprocessedData, [], 1);  % min for each column
    stats.preprocessing.dataMin = dataMin;
    dataMax = max(preprocessedData, [], 1);  % max for each column
    stats.preprocessing.dataMax = dataMax;
    dataDelta = dataMax - dataMin;  % range of each column
    stats.preprocessing.dataDelta = dataDelta;
    dataMean = mean(preprocessedData, 1);  % mean of each column
    stats.preprocessing.dataMean = dataMean;
    dataMed = median(preprocessedData, 1);  % median of each column
    stats.preprocessing.dataMed = dataMed;
    dataStd = std(preprocessedData, 0, 1);  % std deviation of each column
    stats.preprocessing.dataStd = dataStd;
    dataMad = mad(preprocessedData, 1, 1);  % median absolute deviation of each column
    stats.preprocessing.dataMad = dataMad;
    dataQuantiles = quantile(preprocessedData, [0.25, 0.75], 1);
    stats.preprocessing.dataQuantiles = dataQuantiles;
else
    scalingVector = stats.preprocessing.scalingVector;

    dataMin = stats.preprocessing.dataMin;
    dataMax = stats.preprocessing.dataMax;
    dataDelta = stats.preprocessing.dataDelta;
    dataMean = stats.preprocessing.dataMean;
    dataMed = stats.preprocessing.dataMed;
    dataStd = stats.preprocessing.dataStd;
    dataMad = stats.preprocessing.dataMad;
    dataQuantiles = stats.preprocessing.dataQuantiles;
end

%% Skip processing without data
if(size(data,1) == 0)
    preprocessedData = data;
    return;
end    
   
%% Operations on rows (sample data sets)
% logical order of preprocessing steps
% first, actions on rows (data set per sample)
% smoothing: in case of noisy data, best on raw data (larger window sizes),
% careful with binned data - small windowsizes only. 
% scaling of rows (to external factor, unity, to range (0 .. 1), region, ...
% second, actions on columns (data point over all samples)
% transformations (log10, log, invers, power)
% normalization

%% smooth data (experimental: use only on large data sets containig noise. Not recommended when using binned data
if smoothDataMethod == 1                   % Movering average, standard settings with fixed window size 5
    windowSize = 5;
    smoothingVector = (1/windowSize)*ones(1,windowSize);
    preprocessedData = filter(smoothingVector,1,preprocessedData,[],2);    
elseif smoothDataMethod == 2                   % requires curve-fitting toolbox; Savitzky-Golay, standard settings with fixed window size 7 and polynomial of order 3
    windowSize = 7;
    savitzkyGolayOrder = 3;
    preprocessedData = sgolayfilt(preprocessedData,savitzkyGolayOrder,windowSize,[],2);
elseif smoothDataMethod == 3                   % requires curve-fitting toolbox; robust Local regression using weighted linear least squares and a 2nd degree polynomial model  
    smoothFraction = 0.1;              % span for filter is 10 % by default
    for i = 1:size(preprocessedData,1)
        preprocessedData(i,:) = smooth(preprocessedData(i,:),smoothFraction,'rloess');
    end
end
        
%% scale row external data vector, e.g., quantref (eretic), pulcon etc.
% this operation needs to come first

if externalScalingBoolean
    preprocessedData = data .* scalingVector;
end

%% Scale row to unity (each dataset sums up to = 1 (x-axis))
if unityScalingBoolean
    preprocessedData = normalize(preprocessedData,2,"norm", 1);
end

%% Scale min-max: scales every sample data set (row) to range between 0 and 1
if minmaxScalingBoolean
    preprocessedData = normalize(preprocessedData,2,"range");
end

%% scale region (row = sample data set)
if regionScalingBoolean
    regionSum = sum(preprocessedData(:, regionScalingStart:regionScalingEnd),2);
    regionSum(regionSum == 0) = 1;
    preprocessedData = preprocessedData./regionSum;
end

%% Deviations
if transformDeviation > 0
   preprocessedData = diff(preprocessedData, transformDeviation,2);
   % Deviation reduce dimensions, fill lost variables with ones
   preprocessedData = [preprocessedData, ones(size(preprocessedData, 1), transformDeviation)];
end

%% Cell-wise operations (on every data point in the spectra)
%% transformation1 = invers 
if transformInversBoolean
    for j = 1:numel(preprocessedData)
        if preprocessedData(j) ~= 0
            preprocessedData(j) = 1/preprocessedData(j);
        end
    end
end
 
%% transform log10 or log-nat
if transformLog == 10
    for j = 1:numel(preprocessedData)
        if preprocessedData(j) > 0
            preprocessedData(j) = log10(preprocessedData(j));
        else
            preprocessedData(j) = 0;
        end
    end
elseif transformLog == 2
    for j = 1:numel(preprocessedData)
        if preprocessedData(j) > 0
            preprocessedData(j) = log(preprocessedData(j));
        else
            preprocessedData(j) = 0;
        end
    end
end

%% transform square root (Power)
if transformPowerBoolean
    for j = 1:numel(preprocessedData)
        if preprocessedData(j) ~= 0
            preprocessedData(j) = sqrt(abs(preprocessedData(j))) * sign(preprocessedData(j));
        end
    end
end

%% Column-wise operations
%% center (mean-centering): Xij = Xij - mean(Xi)
if meanCenteringBoolean
    preprocessedData = preprocessedData - dataMean;
end

%% center to median
if medianCenteringBoolean
    preprocessedData = preprocessedData - dataMed;
end

%% z-score Scaling
if zScoreScalingBoolean
    preprocessedData = (preprocessedData - dataMean) ./ dataStd;
end

%% z-score scaling, robust
if zScoreScalingRobustBoolean
    preprocessedData = (preprocessedData - dataMean) ./ dataMad;
end

%% scale data to have median absolute deviation (MAD) 1
if madScalingBoolean
    preprocessedData = preprocessedData ./ dataMad;
end

%% scale data to have interquartile range (IRQ) 1
if irqScalingBoolean
    preprocessedData = (preprocessedData - dataQuantiles(1, :)) ./ (dataQuantiles(2, :) - dataQuantiles(1, :));
end

%% Range Scaling: all columns range between 0 and 1
if rangeScalingBoolean
    preprocessedData = (preprocessedData - dataMin) ./ (dataMax - dataMin);
end

%% Pareto Scaling
if paretoScalingBoolean
    preprocessedData = (preprocessedData - dataMean) ./ sqrt(dataStd);
end

%% Vast Scaling
if vastScalingBoolean
    for j = 1:size(preprocessedData, 1)
        for k = 1:size(preprocessedData, 2)
            preprocessedData(j, k) = ((preprocessedData(j, k) - dataMean(k))/dataStd(k)) * (dataMean(k)/dataStd(k));
        end
    end
end

%% Level Scaling 
if levelScalingBoolean
    preprocessedData = (preprocessedData - dataMean) ./ dataMean;
end

%% Probabilistic Quotient Normalization
if pqnBoolean
    preprocessedSum = sum(preprocessedData,2);
    factors = 100./preprocessedSum;
    preprocessedData = preprocessedData.*factors;
end
end
