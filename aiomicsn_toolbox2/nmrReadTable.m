function [origin,barcodeData,data,barcodeDataTest,dataTest,originInterval] = nmrReadTable(origin,barcodeData,dataOld,barcodeDataTest,dataTestOld,source,originInterval)
% Function for reading NMR spectra from Excel spreadsheet
addpath('./icoshift_toolbox');

pathTable = source{1};
pathTableTest = source{2};
columnBarcode = source{3};
columnPath = source{4};
experimentNumber = source{5};
binsCount = source{6};
binsInterval = source{7};
binsExclusuion = source{8};
pathSpectra = source{9};
pathSpectraTest = source{10};
icoshiftBoolean = source{11};
pqnBoolean = source{12};
if length(source) < 13
    icoshiftInterval = -1;
else
    icoshiftInterval = source{13};
end
if length(source) < 14
    optimizedBinning = false;
else
    optimizedBinning = source{14};
end

%% Denoising and Scaling options

%% Set parameters
fprintf("Setting parameters NMR-Files denoising and preprocessing options\r\n");
denoisingBoolean = 0;
denoisingSigmaFactor = 3;
denoisingMaxFactor = 2;
denoisingStart = -0,5;
denoisingEnd = -0,75;
paretoScalingBoolean = 0; 

if isfile('settings.ini')
    settingsIni = textread('settings.ini','%s');
    settingsIni = strrep(settingsIni,':','');
    denoisingBoolean = settingsIniRead(settingsIni,denoisingBoolean);
    denoisingSigmaFactor = settingsIniRead(settingsIni,denoisingSigmaFactor);
    denoisingMaxFactor = settingsIniRead(settingsIni,denoisingMaxFactor);
    denoisingStart = settingsIniRead(settingsIni,denoisingStart);
    denoisingEnd = settingsIniRead(settingsIni,denoisingEnd);
    paretoScalingBoolean = settingsIniRead(settingsIni,paretoScalingBoolean);
end

stats.NMRpreprocessing.denoisingBoolean = denoisingBoolean;
stats.NMRpreprocessing.denoisingSigmaFactor = denoisingSigmaFactor;
stats.NMRpreprocessing.denoisingMaxFactor = denoisingMaxFactor;
stats.NMRpreprocessing.denoisingStart = denoisingStart;
stats.NMRpreprocessing.denoisingEnd = denoisingEnd;
stats.NMRpreprocessing.paretoScalingBoolean = paretoScalingBoolean;

%% Test dataset available?
if length(pathTableTest) < 1
    pathTableTestTrue = false;
else
    pathTableTestTrue = true;
end

%% Simulated spectra?
if strcmp(pathTable,'Simulated')
    simulated = true;
else
    simulated = false;
end

%% Read table
if ~simulated
    [~,~,table] = xlsread(strrep(pathTable,'"',''));
end
if pathTableTestTrue
    [~,~,tableTest] = xlsread(strrep(pathTableTest,'"',''));
end

%% Extract barcodes
if ~simulated
    barcode = barcodeRead(table,columnBarcode);
else
    barcode = (1:size(pathSpectra,1))';
end
if pathTableTestTrue
    barcodeTest = barcodeRead(tableTest,columnBarcode);
end

%% Generate file list
if ~simulated
    filelist = filelistGenerate(table(2:end,columnPath),pathSpectra,experimentNumber);
end
if pathTableTestTrue
    filelistTest = filelistGenerate(tableTest(2:end,columnPath),pathSpectraTest,experimentNumber);
end

%% Check spectra for existence
if ~simulated
    existingBoolean = find(isfile(filelist));
    barcode = barcode(existingBoolean);
    filelist = filelist(existingBoolean);
end
if pathTableTestTrue
    existingBooleanTest = find(isfile(filelistTest));
    barcodeTest = barcodeTest(existingBooleanTest);
    filelistTest = filelistTest(existingBooleanTest);
else
    barcodeTest = [];
end

%% Read spectra
if ~simulated
    spectra = nmrRead1DList(filelist);
else
    spectra = pathSpectra;
end
if pathTableTestTrue
    spectraTest = nmrRead1DList(filelistTest);
end

%% Apply probabilistic quotient normalization
if pqnBoolean
    spectraData = zeros(length(spectra),length(spectra(1).Data));
    for j = 1:length(spectra)
        spectraData(j,:) = spectra(j).Data;
    end
    
    spectraSum = sum(spectraData,2);
    spectraFactors = 100./spectraSum;
    spectraDataNormalized = spectraData.*spectraFactors;
    
    for j = 1:length(spectra)
        spectra(j).Data = spectraDataNormalized(j,:);
    end
    
    if pathTableTestTrue
        spectraTestData = zeros(length(spectraTest),length(spectraTest(1).Data));
        for j = 1:length(spectraTest)
            spectraTestData(j,:) = spectraTest(j).Data;
        end
        
        spectraTestSum = sum(spectraTestData,2);
        spectraTestFactos = 100./spectraTestSum;
        spektraTestDataNormalized = spectraTestData.*spectraTestFactos;
        
        for j = 1:length(spectraTest)
            spectraTest(j).Data = spektraTestDataNormalized(j,:);
        end
    end
end

%% Denoising
if denoisingBoolean
    for i = 1:length(spectra)
        spectraPpm = nmrPpm1D(spectra(i));
        idxNoise = spectraPpm >= denoisingStart & spectraPpm <= denoisingEnd
        meanNoise = nanmean(spectra(i).Data(idxNoise));
        stdNoise = nanstd(spectra(i).Data(idxNoise), 0);
        maxIntensity = max(abs(spectra(i).Data(idxNoise)));

        % Kriterien f�r das Eliminieren von Rauschen
        eliminateCriteria = (spectra(i).Data < (denoisingSigmaFactor * stdNoise)) | ...
                            (abs(spectra(i).Data) < (denoisingMaxFactor * maxIntensity));
        spectra(i).Data(eliminateCriteria) = 0;
    end
end

%% Apply icoshift
if icoshiftBoolean
    spectraData = zeros(length(spectra),length(spectra(1).Data));
    for j = 1:length(spectra)
        spectraData(j,:) = spectra(j).Data;
    end
    
    if icoshiftInterval(1) == -1
        [spectraDataShifted,icoshiftIntervals,icoshiftIndices,icoshiftTargetVector] = icoshift('average',spectraData,binsCount/2);
    else
        spectraPpm = nmrPpm1D(spectra(1));
        for j = 1:length(icoshiftInterval)
            spectraPpmIndex = find(spectraPpm < icoshiftInterval(j));
            icoshiftInterval(j) = spectraPpmIndex(1);
        end
        [spectraDataShifted,icoshiftIntervals,icoshiftIndices,icoshiftTargetVector] = icoshift('average',spectraData,30);
        if input('Plot spectra after first shifting attempt')
            figure;
            plot(spectraPpm,spectraDataShifted');
        end
        [spectraDataShifted,icoshiftIntervals2,icoshiftIndices2,icoshiftTargetVector2] = icoshift('average',spectraDataShifted,icoshiftInterval);
    end
    
    for j = 1:length(spectra)
        spectra(j).Data = spectraDataShifted(j,:);
    end
    
    if pathTableTestTrue
        spectraTestData = zeros(length(spectraTest),length(spectraTest(1).Data));
        for j = 1:length(spectraTest)
            spectraTestData(j,:) = spectraTest(j).Data;
        end
        
        if icoshiftInterval(1) == -1
            spectraTestDataShifted = icoshift(icoshiftTargetVector,spectraTestData,binsCount/2);
        else
            spectraTestDataShifted = icoshift(icoshiftTargetVector,spectraTestData,30);
            spectraTestDataShifted = icoshift(icoshiftTargetVector2,spectraTestDataShifted,icoshiftInterval);
        end
        
        for j = 1:length(spectraTest)
            spectraTest(j).Data = spectraTestDataShifted(j,:);
        end
    end
end

%% Binning
bins = nmrBin1D(spectra,'ppm',binsInterval,'bins',binsCount,'exclusions',binsExclusuion,'deleteExclusions',true,'optimizedBinning',optimizedBinning);
data = bins.DATA;
ppm = bins.PPM;

if pathTableTestTrue
    binsTest = nmrBin1D(spectraTest,'ppm',binsInterval,'bins',binsCount,'exclusions',binsExclusuion,'deleteExclusions',true,'optimizedBinning',bins.BinList);
    dataTest = binsTest.DATA;
else
    dataTest = NaN(1,size(data,2));
end

%% Pareto-Scaling of binned data
if paretoScalingBoolean
    for i = 1:size(data, 1)
        stdDev = std(data(i, :));
        data(i, :) = data(i, :) / sqrt(stdDev);
    end
    
    if pathTableTestTrue
        for i = 1:size(dataTest, 1)
            stdDevTest = std(dataTest(i, :));
            dataTest(i, :) = dataTest(i, :) / sqrt(stdDevTest);
        end
    end
end

%% Save origin
originTemp = cell(3,size(data,2));
originTemp(1,:) = {'NMR'};
originTemp(2,:) = {experimentNumber};
originTemp(3,:) = num2cell(ppm);

%% Append data
[origin,barcodeData,data,barcodeDataTest,dataTest,originInterval] = dataAppend(origin,originTemp,barcodeData,dataOld,barcode,data,barcodeDataTest,dataTestOld,barcodeTest,dataTest,originInterval);
end