function [stats,model] = classificationRaw(data,groups,stats,varargin)
%% AI(omics)^n - a versatile tool for assessing data set characteristics, data-fusion and multi-method modelling of large data sets
% 
% Developed by Felix Ruell under the supervision of Stephan Schwarzinger
% North Bavarian NMR-Centre at the University of Bayreuth
% Universitaetsstrasse 30, 95447 Bayreuth, Germany
% Contact:  s.schwarzinger@uni-bayreuth.de
% With contributions by Peter Kolb, Stefan Bindereif, Simon Steidele,
% Sandra Haupt and kind support by Birk Schuetz (Bruker BioSpin).
% (c) 2023 by the authors.
%
% License: Creative Commons CC BY-NC-SA; in case of loophole GNU-GPL for non-commercial
% applications only and with the extension that reference must be made to the original work 
% and any resulting work is relicensed alike. 
% For any commercial application a license must be obtained. For this purpose, please contact NBNC@uni-bayreuth.de
%
% The development of the software and the associated demonstration data set was in part supported by funds 
% of the Federal Ministry of Food and Agriculture (BMEL) based on a decision of the Parliament of the Federal Republic of Germany 
% via the Federal Office for Agriculture and Food (BLE) under the innovation support program (Reference number: 2816502414). 
%
% Script for classifications via raw values/LDA

%% Model
model = {};
model.type = 'raw';
model.groups = groups;

%% Parse varargin
fprintf("Parsing arguments\r\n");
plotBoolean = false;
if ismember('plot',varargin)
    plotBoolean = true;
end
ldaBoolean = false;
if ismember('lda',varargin)
    ldaBoolean = true;
end
qdaBoolean = false;
if ismember('qda',varargin)
    qdaBoolean = true;
end
edcBoolean = false;
if ismember('edc',varargin)
    edcBoolean = true;
end
knnBoolean = false;
if ismember('knn',varargin)
    knnBoolean = true;
end
textBoolean = false;
if ismember('text',varargin)
    textBoolean = true;
end

if ~(ldaBoolean | qdaBoolean | edcBoolean | knnBoolean)
    warning('No classification method specified. Failback to edc');
    edcBoolean = true;
end

model.plotBoolean = plotBoolean;
model.ldaBoolean = ldaBoolean;
model.qdaBoolean = qdaBoolean;
model.edcBoolean = edcBoolean;
model.knnBoolean = knnBoolean;
model.textBoolean = textBoolean;

%% Set parameters
fprintf("Setting parameters\r\n");
enableFeatureSelect = 1;
methodFeatureSelect = 1;
settingFeatureSelectNearestNeighbors = 10;
dimensionsFeatureSelect = 50;
dimensionsPcaMin = 3;
dimensionsPcaMax = 18;
dimensionsPcaVar = 0.95;
enableMahalanobis = 1;
crossvalidationMethod = 2;
crossvalidationStepsClassification = 100;
crossvalidationShareClassification = 10;
outliersCutoff = 3;
predictOutliersDescriptive = 95;
predictOutliersPredictive = 99;
attributeSamplingConfidence = 95;
sigmaEllipsoid = 2;
displayCalibration = 1;

if isfile('settings.ini')
    settingsIni = textread('settings.ini','%s','commentstyle','shell');
    settingsIni = strrep(settingsIni,':','');
    enableFeatureSelect = settingsIniRead(settingsIni,enableFeatureSelect);
    methodFeatureSelect = settingsIniRead(settingsIni,methodFeatureSelect);
    settingFeatureSelectNearestNeighbors = settingsIniRead(settingsIni,settingFeatureSelectNearestNeighbors);
    dimensionsFeatureSelect = settingsIniRead(settingsIni,dimensionsFeatureSelect);
    dimensionsPcaMin = settingsIniRead(settingsIni,dimensionsPcaMin);
    dimensionsPcaMax = settingsIniRead(settingsIni,dimensionsPcaMax);
    dimensionsPcaVar = settingsIniRead(settingsIni,dimensionsPcaVar);
    enableMahalanobis = settingsIniRead(settingsIni,enableMahalanobis);
    crossvalidationMethod = settingsIniRead(settingsIni,crossvalidationMethod);
    crossvalidationStepsClassification = settingsIniRead(settingsIni,crossvalidationStepsClassification);
    crossvalidationShareClassification = settingsIniRead(settingsIni,crossvalidationShareClassification);
    outliersCutoff = settingsIniRead(settingsIni,outliersCutoff);
    predictOutliersDescriptive = settingsIniRead(settingsIni,predictOutliersDescriptive);
    predictOutliersPredictive = settingsIniRead(settingsIni,predictOutliersPredictive);
    attributeSamplingConfidence = settingsIniRead(settingsIni,attributeSamplingConfidence);
    sigmaEllipsoid = settingsIniRead(settingsIni,sigmaEllipsoid);
    displayCalibration = settingsIniRead(settingsIni,displayCalibration);
end

if dimensionsPcaMax > size(data,2)
    dimensionsPcaMax = size(data,2);
end
if dimensionsFeatureSelect > size(data,2)
    dimensionsFeatureSelect = size(data,2);
end

dimensionsPca = NaN; % since raw ...
stats.dimensionsPca = dimensionsPca;

%% writing settings
stats.settings.enableMahalanobis = enableMahalanobis;
stats.settings.enableFeatureSelect = enableFeatureSelect;
stats.settings.dimensionsFeatureSelect = dimensionsFeatureSelect;
stats.settings.methodFeatureSelect = methodFeatureSelect;
stats.settings.settingFeatureSelectNearestNeighbors = settingFeatureSelectNearestNeighbors;
stats.settings.dimensionsPcaMin = dimensionsPcaMin;
stats.settings.dimensionsPcaMax = dimensionsPcaMax;
stats.settings.dimensionsPcaVar = dimensionsPcaVar;
stats.settings.crossvalidationMethod = crossvalidationMethod;
stats.settings.crossvalidationStepsClassification = crossvalidationStepsClassification;
stats.settings.crossvalidationShareClassification = crossvalidationShareClassification;
stats.settings.outliersCutoff = outliersCutoff;
stats.settings.predictOutliersDescriptive = predictOutliersDescriptive;
stats.settings.predictOutliersPredictive = predictOutliersPredictive;
stats.settings.attributeSamplingConfidence = attributeSamplingConfidence;
stats.settings.displayCalibration = displayCalibration;
stats.settings.sigmaEllipsoid = sigmaEllipsoid;

%% Compile inforamtion about groups
groupsDistinct = tabulate(groups);
[~,groupsOrder] = sort(groupsDistinct(:,1));
groupsDistinct = groupsDistinct(groupsOrder,:);
model.groupsDistinct = groupsDistinct;
stats.data.groupsDistinct = groupsDistinct;
stats.data.groupsN = size(groupsDistinct, 1);
stats.data.groupsSize = cell2mat(groupsDistinct(:, 2));
stats.data.groupsSizeMin = min(stats.data.groupsSize);
stats.data.groupsSizeMax = max(stats.data.groupsSize);

%% Secondary dimension adjustment based on minimum group size
% Only needed for classification of raw data with no dimension reduction step like PCA or PLS
dimensions = size(data,2);
if(enableFeatureSelect > 0)
    dimensions = dimensionsFeatureSelect;
    dimensionsMax = stats.data.groupsSizeMin - round(stats.data.groupsSizeMin*stats.settings.crossvalidationShareClassification/100);
    
    if dimensionsMax < dimensions
        dimensions = dimensionsMax;
        dimensionsFeatureSelect = dimensionsMax;
        stats.settings.dimensionsFeatureSelect = dimensionsFeatureSelect;
        fprintf("Too many dimensions with respect to smallest group size - automatically reduced!\r\n");
    end
end

%% Error when not enough data
% For the necessary calculation of a covariance matrix further in the
% process the dataset must contain at least as many samples (in the reduced
% crossvalidation datasaet) as dimensions. 
if (crossvalidationMethod == 1 && size(data,1) - 1 < dimensions) || (crossvalidationMethod > 1 && floor((1 - crossvalidationShareClassification/100) * size(data,1)) < dimensions)
    error("Not enough data points for selected dimensions and cross-validation method! Possible reason: Check/ reduce number of Kruskal-Wallis variables!")
end

% If Mahalanobis calculation is enabled or EDC or QDA is used this 
% condition has to be fulfilled for each group.
if(enableMahalanobis == 1 || edcBoolean || qdaBoolean)
    for j = 1:size(groupsDistinct,1)
        if (crossvalidationMethod == 1 && groupsDistinct{j,2} - 1 < dimensions) || (crossvalidationMethod > 1 && floor((1 - crossvalidationShareClassification/100) * groupsDistinct{j,2}) < dimensions)
            error("Not enough data points for selected classification method or statistics!")
        end
    end
end

%% Feature Selection
if methodFeatureSelect == 4   % mrmr    
        fprintf("Mrmr - feature selection \r\n");
    elseif methodFeatureSelect == 3  % Chi2
        fprintf("Chi2 - feature selection \r\n");
    elseif methodFeatureSelect == 2   % relieff   
        fprintf("Relieff - feature selection \r\n");
    elseif methodFeatureSelect == 1  % Kruskal Wallis Selection as default
        fprintf("Kruskal-Wallis - feature selection \r\n");
    else 
        fprintf("No feature selection - all features used! \r\n");
end

if enableFeatureSelect > 0
    featureSelectionScore = featureSelectP(data,groups,methodFeatureSelect);
    [~,b] = sort(featureSelectionScore);
    data = data(:,b(1:dimensionsFeatureSelect));
    stats.featureSelectionSorting = b(1:dimensionsFeatureSelect);
    stats.featureSelectionScore = featureSelectionScore(b(1:dimensionsFeatureSelect));

    figure('Name',strcat('Scores of Selected Features: Method ',methodFeatureSelect));
    hold on
    plot(1:dimensionsFeatureSelect,stats.featureSelectionScore,'-bo');    % evt. x-Achse noch invertieren
    xlabel('Feature');
    ylabel('FS Score');
    hold off;
else
    stats.featureSelectionSorting = [1:size(data,2)]';
    stats.featureSelectionScore = zeros(size(data,2),1);
end

%% Cross validation
fprintf("Beginning with cross validation\r\n");

% Leave one out
if crossvalidationMethod == 1
    crossvalidationStepsClassification = length(data(:,1));
    crossvalidationShareClassification = 1/crossvalidationStepsClassification;
    testLength = 1;
else
    testLength = round(length(data(:,1))*crossvalidationShareClassification/100);
end

% Output
fprintf("Steps: %3.0f, share of test spectra: %3.0f percent\r\n", crossvalidationStepsClassification, crossvalidationShareClassification);

% Set up test sets
testSets = cell(crossvalidationStepsClassification,1);
switch crossvalidationMethod
    % Leave one out
    case 1
        for j = 1:crossvalidationStepsClassification
            testSets{j} = j;
        end
    % Monte Carlo random
    case 2
        for j = 1:crossvalidationStepsClassification
            testSets{j} = randperm(length(data(:,1)),testLength);
        end
    % Monte Carlo pseudo-random
    case 3
        testSet = [1:length(data(:,1))];
        for j = 1:crossvalidationStepsClassification
            if length(testSet) > testLength
                testSets{j} = testSet(randperm(length(testSet),testLength));
                testSet = setdiff(testSet,testSets{j});
            else
                testSets{j} = testSet;
                testSet = randperm(length(data(:,1)),testLength - length(testSets{j}));
                testSets{j} = [testSets{j}, testSet];
                testSet = setdiff(1:length(data(:,1)), testSet);
            end
        end
    % Monte Carlo block-wise
    case 4
        testSet = [1:length(data(:,1))];
        for j = 1:crossvalidationStepsClassification
            if length(testSet) > testLength
                testSets{j} = testSet(1:testLength);
                testSet = testSet(testLength+1:end);
            else
                testSets{j} = [testSet, 1:testLength-length(testSet)];
                testSet = [testLength-length(testSet)+1:length(data(:,1))];
            end
        end
end

% Save information about cross validation
stats.data.nModelTotal = size(data, 1);
stats.data.nCvModelTesting = testLength;
stats.data.nCvModelBuilding = size(data, 1) - testLength;

% Attribute sampling: estimation of reliability based on number of samples
% used for crossvalidation for the entire dataset
stats.attributeSampling.confidence = attributeSamplingConfidence;
stats.attributeSampling.reliabilityCv = exp(log((100 - attributeSamplingConfidence) / 100) / stats.data.nCvModelTesting) * 100;
% Assuming that CV samples are equally destributed gives the best case of
% reliability for smallest/largest group, resepectively, or for the case of 
% Leave-one-out validation (then only depending on attributeSamplingConfidence)
stats.attributeSampling.reliabilityCvMinGroup = exp(log((100 - attributeSamplingConfidence) / 100) / (stats.data.nCvModelTesting * (stats.data.groupsSizeMin / stats.data.nModelTotal))) * 100;
stats.attributeSampling.reliabilityCvMaxGroup = exp(log((100 - attributeSamplingConfidence) / 100) / (stats.data.nCvModelTesting * (stats.data.groupsSizeMax / stats.data.nModelTotal))) * 100;
stats.attributeSampling.relaiabilityLoo = exp(log((100 - attributeSamplingConfidence) / 100)) * 100; 

result = cell(testLength*crossvalidationStepsClassification,3+size(groupsDistinct,1));

reverseStr = '';
for crossvalidationStep=1:crossvalidationStepsClassification
    msg = sprintf('Step %3.0f/%3.0f', crossvalidationStep, crossvalidationStepsClassification);
    fprintf([reverseStr, msg]);
    reverseStr = repmat(sprintf('\b'), 1, length(msg));
    
    % Testsatz und Modellsatz generieren
    testSet = testSets{crossvalidationStep};
    modelSet = setdiff(1:length(data(:,1)),testSet);
    
    modelData = data(modelSet(:),:);
    modelGroups = groups(modelSet(:));
    testData = data(testSet(:),:);
    testGroups = groups(testSet(:));
    
    if ldaBoolean | qdaBoolean | knnBoolean
    % Unit-variance scaling
    s = std(modelData);
    modelData = modelData ./repmat(s,size(modelData,1),1);
    
    % Meancentering
    m = mean(modelData);
    modelData = modelData - repmat(m,size(modelData,1),1);
    end
    
    %LDA/QDA
    if ldaBoolean
        modelDa = fitcdiscr(modelData,modelGroups);
    elseif qdaBoolean
        modelDa = fitcdiscr(modelData,modelGroups,'DiscrimType','quadratic');
    elseif knnBoolean
        modelDa = fitcknn(modelData,modelGroups,'NumNeighbors',10);
    end
    
    if ldaBoolean | qdaBoolean | knnBoolean
    % unit-variance scaling
    testData = testData ./ repmat(s,size(testData,1),1);
    
    % subtract mean
    testData = testData - repmat(m,size(testData,1),1);
    end
    
    % EDC
    if edcBoolean
        % Calculation of model-means and distribution
        modelMean = NaN(size(groupsDistinct,1),size(modelData,2));
        modelMeanCov = cell(size(groupsDistinct,1),1);
        
        for i=1:size(groupsDistinct,1)
            modelMean(i,:) = mean(modelData(ismember(modelGroups,groupsDistinct{i,1}),:));
            modelMeanCov{i} = cov(modelData(ismember(modelGroups,groupsDistinct{i,1}),:));
        end
        
        % Predict test samples
        testDistributionEuclidean = pdist2(testData,modelMean,'euclidean');
        [~,testPrediction]=min(testDistributionEuclidean,[],2);
        testProbability = NaN(length(testSet),size(groupsDistinct,1));
        for i = 1:size(groupsDistinct,1)
            testProbability(:,i) = mvnpdf(testData,modelMean(i,:),modelMeanCov{i});
        end
        
        % Compare real group vs. predicted group
        result((crossvalidationStep-1)*testLength+1:crossvalidationStep*testLength,:) = [cellstr(testGroups) groupsDistinct(testPrediction,1) num2cell(testSet') num2cell(testProbability)];
    end
    % LDA/QDA/KNN
    if ldaBoolean | qdaBoolean | knnBoolean
        [testPrediction,testProbability,~] = predict(modelDa,testData);
        result((crossvalidationStep-1)*testLength+1:crossvalidationStep*testLength,:) = [cellstr(testGroups) testPrediction num2cell(testSet') num2cell(testProbability)];
    end
end
fprintf("\r\n");

% Evaluation of cross validation
fprintf("Begin evaluation of cross validation\r\n");

% Probability of correct assignment
crossvalidationEvaluation = zeros(crossvalidationStepsClassification,length(groupsDistinct(:,1))*2);
for i = 1:crossvalidationStepsClassification
    for ii = 1:length(groupsDistinct(:,1))
        for iii = 1+testLength*(i-1):testLength*i
            if strcmp(result(iii,1),groupsDistinct(ii,1))
                crossvalidationEvaluation(i,1+(ii-1)*2)=crossvalidationEvaluation(i,1+(ii-1)*2) + 1;
            end
            if strcmp(result(iii,1),groupsDistinct(ii,1)) && strcmp(result(iii,2),groupsDistinct(ii,1))
                crossvalidationEvaluation(i,2+(ii-1)*2)=crossvalidationEvaluation(i,2+(ii-1)*2) + 1;
            end
        end
    end
end

crossvalidationEvaluationRelative = zeros(crossvalidationStepsClassification,length(groupsDistinct(:,1)));
for i = 1:crossvalidationStepsClassification
    for ii = 1:length(groupsDistinct(:,1))
        crossvalidationEvaluationRelative(i,ii)=crossvalidationEvaluation(i,2+(ii-1)*2)/crossvalidationEvaluation(i,1+(ii-1)*2);
        if crossvalidationEvaluation(i,1+(ii-1)*2) == 0
            crossvalidationEvaluationRelative(i,ii) = 1;
        end
    end
end

crossvalidationEvaluationStatistics = cell(3,length(groupsDistinct(:,1)));
for i = 1:length(groupsDistinct(:,1))
    crossvalidationEvaluationStatistics(1,i) = groupsDistinct(i,1);
    crossvalidationEvaluationStatistics(2,i) = num2cell(mean(crossvalidationEvaluationRelative(:,i)));
    crossvalidationEvaluationStatistics(3,i) = num2cell(std(crossvalidationEvaluationRelative(:,i)));
end

% Probabilities per sample
crossvalidationProbabilites = cell(size(data,1),1);

for j = 1:size(result,1)
    tempProbabilites = crossvalidationProbabilites{result{j,3},1};
    if size(tempProbabilites,1) == 0
        crossvalidationProbabilites{result{j,3},1} = cell2mat(result(j,4:3+size(groupsDistinct,1)));
    else
        tempProbabilites(size(tempProbabilites,1)+1,:) = cell2mat(result(j,4:3+size(groupsDistinct,1)));
        crossvalidationProbabilites{result{j,3},1} = tempProbabilites;
    end
end

crossvalidationProbabilitesStd = cell(size(data,1),1);
for j = 1:size(data,1)
    if isempty(crossvalidationProbabilites{j})
        error(['Sample ', num2str(j), ' not predicted during crossvalidation']);
    end
    
    crossvalidationProbabilitesStd{j} = std(crossvalidationProbabilites{j},0,1);
    crossvalidationProbabilites{j} = mean(crossvalidationProbabilites{j},1);
end
crossvalidationProbabilitesStd = cell2mat(crossvalidationProbabilitesStd);
crossvalidationProbabilites = cell2mat(crossvalidationProbabilites);

% Probabilities full table
crossvalidationProbabilitesFull = cell(crossvalidationStepsClassification,1);
for i = 1:crossvalidationStepsClassification
    crossvalidationProbabilitesFullTemp = NaN(size(data,1),size(groupsDistinct,1));
    for k = 1:testLength
        crossvalidationProbabilitesFullTemp(result{(i-1)*testLength+k,3},:) = cell2mat(result((i-1)*testLength+k,4:end));
    end
    crossvalidationProbabilitesFull{i} = crossvalidationProbabilitesFullTemp;
end

% Confusion matrix
crossvalidationConfusionMatrix = zeros(size(groupsDistinct,1),size(groupsDistinct,1),crossvalidationStepsClassification);
for i = 1:crossvalidationStepsClassification
    resultTemp = result(1+testLength*(i-1):testLength*i,:);
    for ii = 1:size(groupsDistinct,1)
        n = ismember(resultTemp(:,1),groupsDistinct(ii,1));
        for iii = 1:size(groupsDistinct,1)
            crossvalidationConfusionMatrix(iii,ii,i) = sum(ismember(resultTemp(n,2),groupsDistinct(iii,1)));
        end
    end
    crossvalidationConfusionMatrix(:,:,i) = normalizeConfusionMatrix(squeeze(crossvalidationConfusionMatrix(:,:,i)),2);
end

crossvalidationConfusionMatrixMean = mean(crossvalidationConfusionMatrix, 3, 'omitnan').*cell2mat(groupsDistinct(:,2))';
crossvalidationConfusionMatrixStd = std(crossvalidationConfusionMatrix, 0, 3, 'omitnan').*cell2mat(groupsDistinct(:,2))';
[crossvalidationMatthewsCorrelationCoefficient(1),crossvalidationMatthewsCorrelationCoefficient(2)] = mcmcc(crossvalidationConfusionMatrixMean,crossvalidationConfusionMatrixStd);
[crossvalidationCohensKappa(1),crossvalidationCohensKappa(2)] = cohensKappa(crossvalidationConfusionMatrixMean,crossvalidationConfusionMatrixStd);

crossvalidationConfusionMatrixMean = crossvalidationConfusionMatrixMean./size(data,1);
crossvalidationConfusionMatrixStd = crossvalidationConfusionMatrixStd./size(data,1);

crossvalidationConfusionMatrix = cell(1+size(groupsDistinct,1));
for j = 1:size(groupsDistinct,1)
    crossvalidationConfusionMatrix{1,j+1} = ['True ',groupsDistinct{j,1}];
    crossvalidationConfusionMatrix{1+j,1} = ['Predicted ',groupsDistinct{j,1}];
    
    for k = 1:size(groupsDistinct,1)
        crossvalidationConfusionMatrix{1+j,1+k} = [crossvalidationConfusionMatrixMean(j,k);crossvalidationConfusionMatrixStd(j,k)];
    end
end

[TP,TN,FP,FN,crossvalidationSensitivityValues,crossvalidationSpecitivityValues,crossvalidationPrecisionValues,crossvalidationAccuracyValues,crossvalidationPrevalenceThresholdValues,~,~,~,~,crossvalidationSensitivityStd,crossvalidationSpecitivityStd,crossvalidationPrecisionStd,crossvalidationAccuracyStd,crossvalidationPrevalenceThresholdStd] = multiclassMetrics(crossvalidationConfusionMatrixMean,crossvalidationConfusionMatrixStd);

% False Positive Fraction = FPF = FP, for selection criterion for model
%crossvalidationFp = cell(size(groupsDistinct,1),2);
crossvalidationFpMean = 1;
for j = 1:size(groupsDistinct,1)
    crossvalidationFp{j,1} = groupsDistinct{j,1};
    crossvalidationFp{j,2} = FP(j);  % will allow selection to minimize FP for paricular group.
    crossvalidationFpMean = mean(FP); % will allow seletion for overall smallest FP
end

% False Negative Fraction = FNF = FN; for selection critierion for model
%crossvalidationFn = cell(size(groupsDistinct,1),2);
crossvalidationFnMean = 1;
for j = 1:size(groupsDistinct,1)
    crossvalidationFn{j,1} = groupsDistinct{j,1};
    crossvalidationFn{j,2} = FN(j);
   crossvalidationFnMean = mean(FN);
end

% Sensitivity = recall = hit rate = true positve rate TPR
crossvalidationSensitivity = cell(size(groupsDistinct,1),3);
for j = 1:size(groupsDistinct,1)
    crossvalidationSensitivity{j,1} = groupsDistinct{j,1};
    crossvalidationSensitivity{j,2} = crossvalidationSensitivityValues(j);
    crossvalidationSensitivity{j,3} = crossvalidationSensitivityStd(j);
end

% Specificity = selectivity = true negative rate TNR
crossvalidationSpecitivity = cell(size(groupsDistinct,1),3);
for j = 1:size(groupsDistinct,1)
    crossvalidationSpecitivity{j,1} = groupsDistinct{j,1};
    crossvalidationSpecitivity{j,2} = crossvalidationSpecitivityValues(j);
    crossvalidationSpecitivity{j,3} = crossvalidationSpecitivityStd(j);
end

% Precision = positive predictive value PPV
crossvalidationPrecision = cell(size(groupsDistinct,1),3);
for j = 1:size(groupsDistinct,1)
    crossvalidationPrecision{j,1} = groupsDistinct{j,1};
    crossvalidationPrecision{j,2} = crossvalidationPrecisionValues(j);
    crossvalidationPrecision{j,3} = crossvalidationPrecisionStd(j);
end

% Accuracy ACC
crossvalidationAccuracy = cell(size(groupsDistinct,1),3);
for j = 1:size(groupsDistinct,1)
    crossvalidationAccuracy{j,1} = groupsDistinct{j,1};
    crossvalidationAccuracy{j,2} = crossvalidationAccuracyValues(j);
    crossvalidationAccuracy{j,3} = crossvalidationAccuracyStd(j);
end

% Prevalence Threshold PT
crossvalidationPrevalenceThreshold = cell(size(groupsDistinct,1),3);
for j = 1:size(groupsDistinct,1)
    crossvalidationPrevalenceThreshold{j,1} = groupsDistinct{j,1};
    crossvalidationPrevalenceThreshold{j,2} = crossvalidationPrevalenceThresholdValues(j);
    crossvalidationPrevalenceThreshold{j,3} = crossvalidationPrevalenceThresholdStd(j);
end

% ROC
groupsNumber = zeros(length(groups),1);
for i = 1:length(groups)
    groupsNumber(i) = find(strcmp(groups(i),groupsDistinct(:,1)));
end
crossvalidationRoc = mcroc(crossvalidationProbabilites,groupsNumber);

% Misallocation
crossvalidationMisallocation = result(~strcmp(result(:,1),result(:,2)),3);
if size(crossvalidationMisallocation,1) > 0
    crossvalidationMisallocation = tabulate(cell2mat(crossvalidationMisallocation));
    crossvalidationMisallocation = crossvalidationMisallocation(crossvalidationMisallocation(:,2)>0,:);
else
    crossvalidationMisallocation = num2cell(zeros(1,3));
end

fprintf("Statistics:\n");
fprintf("cvACC\n");
disp(crossvalidationEvaluationStatistics);
fprintf("cvMCC\n");
disp(crossvalidationMatthewsCorrelationCoefficient);

stats.crossvalidation.crossvalidationEvaluationStatistics = crossvalidationEvaluationStatistics;
stats.crossvalidation.crossvalidationConfusionMatrix = crossvalidationConfusionMatrix';
stats.crossvalidation.crossvalidationConfusionMatrixMean = crossvalidationConfusionMatrixMean';
stats.crossvalidation.crossvalidationConfusionMatrixStd = crossvalidationConfusionMatrixStd';
stats.crossvalidation.crossvalidationMatthewsCorrelationCoefficient = crossvalidationMatthewsCorrelationCoefficient;
stats.crossvalidation.crossvalidationCohensKappa = crossvalidationCohensKappa;
stats.crossvalidation.crossvalidationRoc = crossvalidationRoc;
stats.crossvalidation.crossvalidationMisallocation = crossvalidationMisallocation;
stats.crossvalidation.crossvalidationSensitivity = crossvalidationSensitivity;
stats.crossvalidation.crossvalidationSpecitivity = crossvalidationSpecitivity;
stats.crossvalidation.crossvalidationPrecision = crossvalidationPrecision;
stats.crossvalidation.crossvalidationAccuracy = crossvalidationAccuracy;
stats.crossvalidation.crossvalidationPrevalenceThreshold = crossvalidationPrevalenceThreshold;
stats.crossvalidation.crossvalidationProbabilites = crossvalidationProbabilites;
stats.crossvalidation.crossvalidationProbabilitesStd = crossvalidationProbabilitesStd;
stats.crossvalidation.crossvalidationProbabilitesFull = crossvalidationProbabilitesFull;

stats.crossvalidation.crossvalidationFp = crossvalidationFp;
stats.crossvalidation.crossvalidationFpMean = crossvalidationFpMean;
stats.crossvalidation.crossvalidationFn = crossvalidationFn;
stats.crossvalidation.crossvalidationFnMean = crossvalidationFnMean;

% In case of not more than 2 groups calculate additional benchmarks
if size(groupsDistinct,1) == 2
    % [TP,TN,FP,FN,crossvalidationSensitivityValues,crossvalidationSpecitivityValues,crossvalidationPrecisionValues,crossvalidationAccuracyValues,crossvalidationPrevalenceThresholdValues,~,~,~,~,crossvalidationSensitivityStd,crossvalidationSpecitivityStd,crossvalidationPrecisionStd,crossvalidationAccuracyStd,crossvalidationPrevalenceThresholdStd] = multiclassMetrics(crossvalidationConfusionMatrixMean,crossvalidationConfusionMatrixStd);
    [negativePredictiveValue,missRate,fallOut,falseDiscoveryRate,falseOmissonRate,positiveLikelyhoodRatio,negativeLikelyhoodRatio,threatScore,f1Score,fowlkesMellowsIndex,informedness,markedness,diagnosticOddsRatio,relativeRisc] = binaryMetrics(crossvalidationConfusionMatrixMean);
    stats.crossvalidation.crossvalidationMeanNPV = negativePredictiveValue;
    stats.crossvalidation.crossvalidationMeanMissRate = missRate;
    stats.crossvalidation.crossvalidationMeanFallOut = fallOut;
    stats.crossvalidation.crossvalidationMeanFDR = falseDiscoveryRate;
    stats.crossvalidation.crossvalidationMeanFOR = falseOmissonRate;
    stats.crossvalidation.crossvalidationMeanPLR = positiveLikelyhoodRatio;
    stats.crossvalidation.crossvalidationMeanNLR = negativeLikelyhoodRatio;
    stats.crossvalidation.crossvalidationMeanThreatScore = threatScore;
    stats.crossvalidation.crossvalidationMeanF1 = f1Score;
    stats.crossvalidation.crossvalidationMeanFMI = fowlkesMellowsIndex;
    stats.crossvalidation.crossvalidationMeanInformedness = informedness;
    stats.crossvalidation.crossvalidationMeanMarkedness = markedness;
    stats.crossvalidation.crossvalidationMeanDOR = diagnosticOddsRatio;
    stats.crossvalidation.crossvalidationMeanRelativeRisc = relativeRisc; 
end

%% Predictive model

fprintf("Generating predictive model\r\n");

if ldaBoolean | qdaBoolean | knnBoolean
% Unit-variance scaling
s = std(data);
data = data ./repmat(s,size(data,1),1);
model.s = s;

% Meancentering
m = mean(data);
data = data - repmat(m,size(data,1),1);
model.m = m;
end

%LDA/QDA/KNN
if ldaBoolean
    modelDa = fitcdiscr(data,groups);
    model.modelDa = modelDa;
elseif qdaBoolean
    modelDa = fitcdiscr(data,groups,'DiscrimType','quadratic');
    model.modelDa = modelDa;
elseif knnBoolean
    modelDa = fitcknn(data,groups,'NumNeighbors',10);
    model.modelDa = modelDa;
end

if edcBoolean
    % Calculation of model-means
    modelMean = NaN(size(groupsDistinct,1),size(data,2));
    modelMeanCov = cell(size(groupsDistinct,1),1);
    
    for i=1:size(groupsDistinct,1)
        modelMean(i,:) = mean(data(ismember(groups,groupsDistinct{i,1}),:));
        modelMeanCov{i} = cov(data(ismember(groups,groupsDistinct{i,1}),:));
    end
    
    model.modelMean = modelMean;
    modelPlotScore = data;
    model.modelPlotScore = modelPlotScore;
    
    % Predict model samples
    modelDistributionEuclideanPrediction = pdist2(data,modelMean,'euclidean');
    [~,modelPrediction]=min(modelDistributionEuclideanPrediction,[],2);
    modelPredictionProbabilities = NaN(size(data,1),size(groupsDistinct,1));
    for i = 1:size(groupsDistinct,1)
        modelPredictionProbabilities(:,i) = mvnpdf(data,modelMean(i,:),modelMeanCov{i});
    end
    modelPrediction = groupsDistinct(modelPrediction,1);
    
    model.modelMeanCov = modelMeanCov;
end
if ldaBoolean | qdaBoolean | knnBoolean
    % Predict model samples
    [modelPrediction,modelPredictionProbabilities,~] = predict(modelDa,data);
end

if ldaBoolean
    % Calculate eigenvector matrix
    [ldaEigenvectors,~] = eig(modelDa.Sigma,modelDa.BetweenSigma);
    modelPlotScore = data*ldaEigenvectors;
    
    model.ldaEigenvectors = ldaEigenvectors;
    model.modelPlotScore = modelPlotScore;
end
if qdaBoolean
    % Calculate eigenvector matrix per group
    ldaEigenvectors = zeros(size(data,2),size(data,2),size(groupsDistinct,1));
    
    for j = 1:size(groupsDistinct,1)
        [ldaEigenvectors(:,:,j),~] = eig(squeeze(modelDa.Sigma(:,:,j)),modelDa.BetweenSigma);
        modelPlotScore(ismember(groups,groupsDistinct{j,1}),:) = data(ismember(groups,groupsDistinct{j,1}),:)*squeeze(ldaEigenvectors(:,:,j));
    end
    
    model.ldaEigenvectors = ldaEigenvectors;
    model.modelPlotScore = modelPlotScore;
end
if knnBoolean
    modelPlotScore = data;
    model.modelPlotScore = modelPlotScore;
end
if ldaBoolean | qdaBoolean | knnBoolean
    % Calculation of model-means
    modelMean = NaN(size(groupsDistinct,1),size(modelPlotScore,2));
    
    for i=1:size(groupsDistinct,1)
        modelMean(i,:) = mean(modelPlotScore(ismember(groups,groupsDistinct{i,1}),:));
    end
    
    model.modelMean = modelMean;
end

% Test for outliers
modelOutliers = [];
if(enableMahalanobis == 1)
    fprintf("Test for outliers\r\n");
    modelDistributionEuclidean = zeros(size(modelPlotScore,1),1);
    modelDistributionMahalanobis = zeros(size(modelPlotScore,1),1);
    for i=1:size(modelPlotScore,1)
        ii = find(ismember(groupsDistinct(:,1),groups(i)));
        modelDistributionEuclidean(i) = pdist2(modelPlotScore(i,:),modelMean(ii,:),'euclidean');
        ii = find(ismember(groups,groupsDistinct{ii,1}));
        modelDistributionMahalanobis(i) = mahal(modelPlotScore(i,:),modelPlotScore(ii,:));
    end
    modelDistributionMahalanobisIntervals = zeros(size(groupsDistinct,1),1);
    modelDistributionEuclideanIntervals = zeros(size(groupsDistinct,1),1);
    for i=1:size(groupsDistinct,1)
        ii = find(ismember(groups,groupsDistinct{i,1}));
        modelDistributionMahalanobisIntervals(i) = prctile(modelDistributionMahalanobis(ii),predictOutliersDescriptive);
        modelDistributionEuclideanIntervals(i) = prctile(modelDistributionEuclidean(ii),predictOutliersDescriptive);
    end
    for i=1:size(modelPlotScore,1)
        ii = find(ismember(groupsDistinct(:,1),groups(i)));
        if abs(modelDistributionMahalanobis(i)) > outliersCutoff*modelDistributionMahalanobisIntervals(ii);
            modelOutliers = [modelOutliers;i];
        end
    end
end

if enableMahalanobis == 1
    stats.outliers = modelOutliers;
    % Descriptive
    stats.distances.mahalanobis = modelDistributionMahalanobis;
    stats.distances.mahalanobisMean = mean(modelDistributionMahalanobis);
    stats.distances.mahalanobisStandardDeviation = std(modelDistributionMahalanobis);
    stats.distances.mahalanobisMedian = median(modelDistributionMahalanobis);
    stats.distances.mahalanobis90Percentile = prctile(modelDistributionMahalanobis,90);
    stats.distances.mahalanobis95Percentile = prctile(modelDistributionMahalanobis,95);
    stats.distances.mahalanobis975Percentile = prctile(modelDistributionMahalanobis,97.5);
    stats.distances.mahalanobis99Percentile = prctile(modelDistributionMahalanobis,99);
    stats.distances.euclidean = modelDistributionEuclidean;
    stats.distances.euclideanMean = mean(modelDistributionEuclidean);
    stats.distances.euclideanStandardDeviation = std(modelDistributionEuclidean);
    stats.distances.euclideanMedian = median(modelDistributionEuclidean);
    stats.distances.euclidean90Percentile = prctile(modelDistributionEuclidean,90);
    stats.distances.euclidean95Percentile = prctile(modelDistributionEuclidean,95);
    stats.distances.euclidean975Percentile = prctile(modelDistributionEuclidean,97.5);
    stats.distances.euclidean99Percentile = prctile(modelDistributionEuclidean,99);
    % Predictive
    stats.outliers = NaN;
    stats.crossvalidation.distances.mahalanobis = NaN;
    stats.crossvalidation.distances.mahalanobisMean = NaN;
    stats.crossvalidation.distances.mahalanobisStandardDeviation = NaN;
    stats.crossvalidation.distances.mahalanobisMedian = NaN;
    stats.crossvalidation.distances.mahalanobis90Percentile = NaN;
    stats.crossvalidation.distances.mahalanobis95Percentile = NaN;
    stats.crossvalidation.distances.mahalanobis975Percentile = NaN;
    stats.crossvalidation.distances.mahalanobis99Percentile = NaN;
    stats.crossvalidation.distances.euclidean = NaN;
    stats.crossvalidation.distances.euclideanMean = NaN;
    stats.crossvalidation.distances.euclideanStandardDeviation = NaN;
    stats.crossvalidation.distances.euclideanMedian = NaN;
    stats.crossvalidation.distances.euclidean90Percentile = NaN;
    stats.crossvalidation.distances.euclidean95Percentile = NaN;
    stats.crossvalidation.distances.euclidean975Percentile = NaN;
    stats.crossvalidation.distances.euclidean99Percentile = NaN;
else
    stats.outliers = NaN;
    % Descriptive
    stats.distances.mahalanobis = NaN;
    stats.distances.mahalanobisMean = NaN;
    stats.distances.mahalanobisStandardDeviation = NaN;
    stats.distances.mahalanobisMedian = NaN;
    stats.distances.mahalanobis90Percentile = NaN;
    stats.distances.mahalanobis95Percentile = NaN;
    stats.distances.mahalanobis975Percentile = NaN;
    stats.distances.mahalanobis99Percentile = NaN;
    stats.distances.euclidean = NaN;
    stats.distances.euclideanMean = NaN;
    stats.distances.euclideanStandardDeviation = NaN;
    stats.distances.euclideanMedian = NaN;
    stats.distances.euclidean90Percentile = NaN;
    stats.distances.euclidean95Percentile = NaN;
    stats.distances.euclidean975Percentile = NaN;
    stats.distances.euclidean99Percentile = NaN;
    % Predictive
    stats.outliers = NaN;
    stats.crossvalidation.distances.mahalanobis = NaN;
    stats.crossvalidation.distances.mahalanobisMean = NaN;
    stats.crossvalidation.distances.mahalanobisStandardDeviation = NaN;
    stats.crossvalidation.distances.mahalanobisMedian = NaN;
    stats.crossvalidation.distances.mahalanobis90Percentile = NaN;
    stats.crossvalidation.distances.mahalanobis95Percentile = NaN;
    stats.crossvalidation.distances.mahalanobis975Percentile = NaN;
    stats.crossvalidation.distances.mahalanobis99Percentile = NaN;
    stats.crossvalidation.distances.euclidean = NaN;
    stats.crossvalidation.distances.euclideanMean = NaN;
    stats.crossvalidation.distances.euclideanStandardDeviation = NaN;
    stats.crossvalidation.distances.euclideanMedian = NaN;
    stats.crossvalidation.distances.euclidean90Percentile = NaN;
    stats.crossvalidation.distances.euclidean95Percentile = NaN;
    stats.crossvalidation.distances.euclidean975Percentile = NaN;
    stats.crossvalidation.distances.euclidean99Percentile = NaN;
end


% Quality of separation
modelQuality = NaN(size(groupsDistinct,1));

if(enableMahalanobis == 1)
    modelDistributionMahalanobisMeans = zeros(size(groupsDistinct,1),1);
    for j = 1:size(groupsDistinct,1)
        modelDistributionMahalanobisMeans(j) = mean(modelDistributionMahalanobis(ismember(groups,groupsDistinct{j,1})));
    end

    for j = 1:size(groupsDistinct,1)
        for k = 1:j
            modelQuality(j,k)= (mahal(modelMean(k,:),modelPlotScore(ismember(groups,groupsDistinct{j,1}),:))+mahal(modelMean(j,:),modelPlotScore(ismember(groups,groupsDistinct{k,1}),:)))/(modelDistributionMahalanobisMeans(j)+modelDistributionMahalanobisMeans(k));
        end
    end
end

stats.quality = cell(size(groupsDistinct,1)+1);
for j = 1:size(groupsDistinct,1)
    stats.quality(j+1,1)=groupsDistinct(j,1);
    stats.quality(1,j+1)=groupsDistinct(j,1);
end
stats.quality(2:end,2:end) = num2cell(modelQuality);

% Confusion matrix
for i = 1:size(groupsDistinct,1)
    confusionMatrix(1,i+1) = groupsDistinct(i,1);
    confusionMatrix(i+1,1) = groupsDistinct(i,1);
end
for i = 1:size(groupsDistinct,1)
    n = find(ismember(groups,groupsDistinct(i,1)));
    for ii = 1:size(groupsDistinct,1)
        confusionMatrix(1+ii,1+i) = num2cell(sum(ismember(modelPrediction(n),groupsDistinct(ii,1))));
    end
end
stats.confusionMatrix = confusionMatrix;

% Matthews corrleation coefficient
stats.matthewsCorrelationCoefficient = mcmcc(cell2mat(confusionMatrix(2:size(groupsDistinct,1)+1,2:size(groupsDistinct,1)+1)));

% Cohen's Kappa
stats.cohensKappa = cohensKappa(cell2mat(confusionMatrix(2:size(groupsDistinct,1)+1,2:size(groupsDistinct,1)+1)));

%% Intervals
ellipsoidValues = ellipsoidValuesCalculate(modelPlotScore,groups,sigmaEllipsoid);
model.ellipsoidValues = ellipsoidValues;

%% Plot
if plotBoolean
    CM = lines(size(groupsDistinct,1));
    
    if(enableMahalanobis == 1)
        fprintf("Plotting test for outliers\r\n");
        for i=1:size(groupsDistinct,1)
            figure('Name',strcat('Calibration - Outliers test: group ',num2str(i),' (descriptive)'));
            hold on
            ii = find(ismember(groups,groupsDistinct(i,1)));
            patch([log(min(modelDistributionMahalanobis(ii))),log(modelDistributionMahalanobisIntervals(i)),log(modelDistributionMahalanobisIntervals(i)),log(min(modelDistributionMahalanobis(ii)))],[log(min(modelDistributionEuclidean(ii))),log(min(modelDistributionEuclidean(ii))),log(modelDistributionEuclideanIntervals(i)),log(modelDistributionEuclideanIntervals(i))],'b','FaceAlpha',0.5,'EdgeColor','none','LineStyle','none');
            patch([log(min(modelDistributionMahalanobis(ii))),log(modelDistributionMahalanobisIntervals(i)),log(modelDistributionMahalanobisIntervals(i)),log(min(modelDistributionMahalanobis(ii)))],[log(modelDistributionEuclideanIntervals(i)),log(modelDistributionEuclideanIntervals(i)),log(max(modelDistributionEuclidean(ii))),log(max(modelDistributionEuclidean(ii)))],'r','FaceAlpha',0.5,'EdgeColor','none','LineStyle','none');
            patch([log(modelDistributionMahalanobisIntervals(i)),log(max(modelDistributionMahalanobis(ii))),log(max(modelDistributionMahalanobis(ii))),log(modelDistributionMahalanobisIntervals(i))],[log(min(modelDistributionEuclidean(ii))),log(min(modelDistributionEuclidean(ii))),log(modelDistributionEuclideanIntervals(i)),log(modelDistributionEuclideanIntervals(i))],'r','FaceAlpha',0.5,'EdgeColor','none','LineStyle','none');
            patch([log(modelDistributionMahalanobisIntervals(i)),log(max(modelDistributionMahalanobis(ii))),log(max(modelDistributionMahalanobis(ii))),log(modelDistributionMahalanobisIntervals(i))],[log(modelDistributionEuclideanIntervals(i)),log(modelDistributionEuclideanIntervals(i)),log(max(modelDistributionEuclidean(ii))),log(max(modelDistributionEuclidean(ii)))],'r','FaceAlpha',0.5,'EdgeColor','none','LineStyle','none');
            plot(log(modelDistributionMahalanobis(ii)),log(modelDistributionEuclidean(ii)),'ko','MarkerFaceColor',CM(i,:));
            if textBoolean
                text(log(modelDistributionMahalanobis(ii)),log(modelDistributionEuclidean(ii)),num2str(ii));
            end
            hold off;
        end
    end
    
    fprintf("Plotting prediction\r\n");
    figure('Name','Calibration - Model (descriptive)');
    hold on;
    for i=1:size(groupsDistinct,1)
        ii = find(ismember(groups,groupsDistinct(i,1)));
        plot3(modelPlotScore(ii,1),modelPlotScore(ii,2),modelPlotScore(ii,3),'ko','MarkerFaceColor',CM(i,:));
    end
    for i=1:size(groupsDistinct,1)
        surf(squeeze(ellipsoidValues(i,1,:,:)),squeeze(ellipsoidValues(i,2,:,:)),squeeze(ellipsoidValues(i,3,:,:)),'EdgeColor',CM(i,:),'LineStyle',':','FaceColor',CM(i,:),'FaceAlpha',0.3);
    end
    if textBoolean
        text(modelPlotScore(:,1),modelPlotScore(:,2),modelPlotScore(:,3),num2str([1:1:size(modelPlotScore,1)]'));
    end
    
    axis vis3d;
    box on;
    grid on;
    legend(groupsDistinct(:,1));
    xlabel('PC1');
    ylabel('PC2');
    zlabel('PC3');
    view(30,10);
    hold off;

    fprintf("Plotting ROC graphs\r\n");
    for i=1:size(groupsDistinct,1)
        figure('Name',strcat('Calibration - ROC: group ',num2str(i)));
        hold on
        
        plot(crossvalidationRoc(i).roc(:,2),crossvalidationRoc(i).roc(:,1),'-','MarkerFaceColor',CM(i,:),'LineWidth',2);
        
        xlabel('False positive rate');
        ylabel('True positive rate');
        xlim([0,1]);
        ylim([0,1]);
        hold off;
    end

    fprintf("Plotting Confusion Table (from cross-validation) \r\n");
    plotConfusion(groupsDistinct,stats.crossvalidation.crossvalidationConfusionMatrixMean,stats.data.nModelTotal,stats.settings.crossvalidationStepsClassification,stats.settings.crossvalidationShareClassification,1);
end
end