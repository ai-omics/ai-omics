function filelist = filelistGenerate(paths,pathSpectra,experimentNumber)
% Generates file list from table
%
% part of the AI(omics)n distribution 
% https://gitlab.com/ai-omics/ai-omics
% (c) 2024 by the authors
% distributed under the conditions listed in the file
% classificationUI2.m and the documentation

pathSpectra = split(pathSpectra,',');
if size(pathSpectra,1) > 1
    if size(pathSpectra,2) == 1
        pathSpectra = pathSpectra';
    end
    for j = 1:size(pathSpectra,1)
        paths = strrep(paths,pathSpectra{j,1},pathSpectra{j,2});
    end
end
if ispc
    filelist = strcat(paths, '\', num2str(experimentNumber), '\pdata\1\1r');
else
    filelist = strcat(paths, '/', num2str(experimentNumber), '/pdata/1/1r');
end
end