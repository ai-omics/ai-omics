function spec = nmrRead1D(inputFile, procsFile, varargin)
% This function reads the intensities of corrreponding ppm values and
% additional parameters from Bruker 1d NMR files.
%
% part of the AI(omics)n distribution 
% https://gitlab.com/ai-omics/ai-omics
% (c) 2023 by the authors
% distributed under the conditions listed in the file
% classificationUI2.m and the documentation

%% Parse varargin
p = inputParser;
p.addParameter('imaginary',false);
p.parse(varargin{:});
pars = p.Results;

%% Read procs values
[nc_proc,ofs,sf,si,sw] = internalProcessProcs(procsFile);
spec.sw = sw;
spec.maxppm = ofs;
spec.minppm = ofs - sw;
spec.size = si;
spec.SF = sf;

%% Read 1r file
fid = fopen(inputFile, 'rb');
if fid<1
    error('File not found %s\n', inputFile);
else
    intensityReal = fread(fid, 'int');
end
fclose(fid);

if pars.imaginary
    if ispc
        inputFileImaginary = strrep(inputFile,'\1r','\1i');
    else
        inputFileImaginary = strrep(inputFile,'/1r','/1i');
    end
    fid = fopen(inputFileImaginary, 'rb');
    if fid<1
        error('File not found %s\n', inputFileImaginary);
    else
        intensityImaginary = fread(fid, 'int');
    end
    fclose(fid);
else
    intensityImaginary = [];
end

spec.Data = intensityReal.*(2^(nc_proc));
spec.Imag = intensityImaginary.*(2^(nc_proc));
end

function [nc_proc,ofs,sf,si,sw] = internalProcessProcs(procsFile)
fid = fopen(procsFile, 'r');
0
if fid < 1
    error('Could not open the procs file %s!',procsFile);
end

nc_proc = 0;
ofs = 0;
sf = 0;
si = 0;
sw_p = 0;

tline = fgetl(fid);
satisfied = false;
while ~satisfied
    if ~isempty(strfind(tline, '##$NC_proc= '))
        tline = strrep(tline, '##$NC_proc= ', '');
        nc_proc = str2double(tline);
    end
    if ~isempty(strfind(tline, '##$SW_p= '))
        tline = strrep(tline, '##$SW_p= ', '');
        sw_p = str2double(tline);
    end
    if ~isempty(strfind(tline, '##$SI= '))
        tline = strrep(tline, '##$SI= ', '');        
        si = str2double(tline);
    end
    if ~isempty(strfind(tline, '##$SF= '))
        tline = strrep(tline, '##$SF= ', '');
        sf = str2double(tline);
    end
    if ~isempty(strfind(tline, '##$OFFSET= '))
        tline = strrep(tline, '##$OFFSET= ', '');
        ofs = str2double(tline);
    end
    tline = fgetl(fid);
    if ~ischar(tline) || (sw_p~=0 && si ~= 0 && ofs ~= 0 && sf ~= 0 && nc_proc ~= 0)
        satisfied = true;
    end
end
fclose(fid);

sw = sw_p/sf;
end