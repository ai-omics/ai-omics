function pascalTriangleRow = pascalTriangleRow(n)
%
% part of the AI(omics)n distribution 
% https://gitlab.com/ai-omics/ai-omics
% (c) 2023 by the authors
% distributed under the conditions listed in the file
% classificationUI2.m and the documentation

    pascalTriangle(1, 1) = 1;
    pascalTriangle(2, 1 : 2) = [1 1];

    % If only two rows are requested, then exit
    if n < 2
        pascalTriangleRow = 1;
        return
    end
    if n < 3
        pascalTriangleRow = pascalTriangle(n,:);
        return
    end

    for r = 3 : n
        % The first element of every row is always 1
        pascalTriangle(r, 1) = 1;

        % Every element is the addition of the two elements
        % on top of it. That means the previous row.
        for c = 2 : r-1
            pascalTriangle(r, c) = pascalTriangle(r-1, c-1) + pascalTriangle(r-1, c);
        end

        % The last element of every row is always 1
        pascalTriangle(r, r) = 1;
    end
    
    pascalTriangleRow = pascalTriangle(n,:);
end