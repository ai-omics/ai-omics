function specs = irReadList(filelist, spectrumType, spectrumTypeIndex)
% Wrapper for reading multiple IR spectra from a file list
%
% part of the AI(omics)n distribution 
% https://gitlab.com/ai-omics/ai-omics
% (c) 2023 by the authors
% distributed under the conditions listed in the file
% classificationUI2.m and the documentation

specs = struct([]);
filelistLength = length(filelist);

reverseStr = '';

for j=1:filelistLength
    path = filelist{j};
    
    spec = irReadOpus(path, spectrumType, spectrumTypeIndex);
    specs = [specs;spec];
    
    percentDone = 100 * j / filelistLength;
	msg = sprintf('Reading: %3.1f percent done', percentDone);
	fprintf([reverseStr, msg]);
	reverseStr = repmat(sprintf('\b'), 1, length(msg));
end
fprintf('\r\n');
end
