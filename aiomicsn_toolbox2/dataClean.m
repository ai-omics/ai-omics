function [dataCleaned,barcodeCleaned] = dataClean(data,barcode)
% Function to clean up data
%
% part of the AI(omics)n distribution 
% https://gitlab.com/ai-omics/ai-omics
% (c) 2023 by the authors
% distributed under the conditions listed in the file
% classificationUI2.m and the documentation

rowsWithData = false(size(barcode,1),1);

for j = 1:size(barcode,1)
    if sum(isnan(data(j,:))) == 0
        rowsWithData(j) = true;
    end
end

dataCleaned = data(rowsWithData,:);
barcodeCleaned = barcode(rowsWithData);
end

