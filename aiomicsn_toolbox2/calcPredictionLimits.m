function stats = calcPredictionLimits(model, stats)
% Function for calculating and plotting of prediction limits
%
% part of the AI(omics)n distribution 
% https://gitlab.com/ai-omics/ai-omics
% (c) 2023 by the authors
% distributed under the conditions listed in the file
% classificationUI2.m and the documentation

colors = lines(size(model.groupsDistinct, 1));
otherColor = [0.7, 0.7, 0.7];

for j = 1:size(model.groupsDistinct, 1)
    groupMembers = strcmp(model.groups, model.groupsDistinct{j, 1});
    groupProbabilities = stats.crossvalidation.crossvalidationProbabilites(groupMembers, j);
    otherProbabilities = stats.crossvalidation.crossvalidationProbabilites(~groupMembers, j);
    
    % Create histogram

    edges = [0;0.01;0.025;0.05;0.1;0.2;0.3;0.4;0.5;0.6;0.7;0.8;0.9;0.95;0.975;0.99;1];

    groupHist = calcHist(groupProbabilities, edges) ./ length(groupProbabilities);
    otherHist = calcHist(otherProbabilities, edges) ./ length(otherProbabilities);

    % Calculate intervals

    groupLower = prctile(groupProbabilities, 5);
    otherUpper = prctile(otherProbabilities, 95);
    
    % Save in struct
    
    groupLimits = struct();
    groupLimits.group = model.groupsDistinct{j, 1};
    groupLimits.groupLowerLimit = groupLower;
    groupLimits.otherUpperLimit = otherUpper;
    groupLimits.groupHistogram = groupHist;
    groupLimits.otherHistogram = otherHist;
    probabilityLimits(j) = groupLimits;
    
    % Plot histograms

    figure('Name', strcat('Calibration - Prediction Limits:', 32, model.groupsDistinct{j, 1}));
    hold on;

    set(gca,'TickDir','out');

    patch('XData', [NaN, NaN, NaN, NaN], 'YData', [NaN, NaN, NaN, NaN], 'FaceColor', otherColor, 'FaceAlpha', 1, 'EdgeColor', 'none');
    patch('XData', [NaN, NaN, NaN, NaN], 'YData', [NaN, NaN, NaN, NaN], 'FaceColor', colors(j,:), 'FaceAlpha', 1, 'EdgeColor', 'none');

    patch('XData', [otherUpper, groupLower, groupLower, otherUpper], 'YData', [0, 0, 1, 1], 'FaceColor', 'r', 'FaceAlpha', 0.3, 'EdgeColor', 'none');

    
    for k = 1:length(otherHist)
        patch('XData', [edges(k), edges(k+1), edges(k+1), edges(k)], 'YData', [0, 0, otherHist(k), otherHist(k)], 'FaceColor', otherColor, 'FaceAlpha', 1, 'EdgeColor', 'k');
    end
    for k = 1:length(groupHist)
        patch('XData', [edges(k), edges(k+1), edges(k+1), edges(k)], 'YData', [otherHist(k), otherHist(k), otherHist(k) + groupHist(k), otherHist(k) + groupHist(k)], 'FaceColor', colors(j,:), 'FaceAlpha', 1, 'EdgeColor', 'k');
    end

    xline(otherUpper, '-', {'95th percentile', strcat('non-', model.groupsDistinct{j, 1}, ' samples')}, 'Color', 'r', 'LineWidth', 2, 'LabelHorizontalAlignment', 'left');
    xline(groupLower, '-', {'5th percentile', strcat(model.groupsDistinct{j, 1}, ' samples')}, 'Color', 'r', 'LineWidth', 2);

    legend(strcat('non-', model.groupsDistinct{j, 1}, ' samples'), strcat(model.groupsDistinct{j, 1}, ' samples'));
    xlabel('Probability belonging to class')
    ylabel('Relative frequency per class');

    ylim([0 1]);
    hold off;
end

stats.crossvalidation.crossvalidationProbabilityLimits = probabilityLimits;
end

%% Supportive functions

function hist = calcHist(data, edges)
hist = zeros(length(edges) - 1,1);
for j = 1:length(edges) - 1
    temp = data >= edges(j);
    if j == length(edges) - 1
        temp = temp & data < edges(j + 1);
    else
        temp = temp & data <= edges(j + 1);
    end
    hist(j) = sum(temp);
end
end