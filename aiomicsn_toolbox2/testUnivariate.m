function [uniVariateViolationsDataTest] = testUnivariate(dataTraining, dataTest)
% Script for testing test data sets for univariate deviations from training
% data set (min-max method or percentile method)
%
% part of the AI(omics)n distribution 
% https://gitlab.com/ai-omics/ai-omics
% (c) 2023,2024 by the authors
% distributed under the conditions listed in the file
% classificationUI2.m and the documentation

%% future improvement
% 1 determine noise bandwidth and add noise bandwidth to min and max,
% respectively
% 2 find extra treatment for noise regions to be not too sensitive, -->
% e.g. add factor 2 (or 3) for noise region to select only signals reliably
% detected.

uniVariateViolationsDataTest = zeros[1:size(dataTest,1);1:size(dataTest,2)];

if minmax1VarBoolean
    univariateMin = min(dataTraining);     % min for each column (datapoint)
    univariateMax = max(dataTraining);     % max for each column
    %Future version: determine (automatically) noise bandwidth (e.g. 3x
    %Stdev(noise) and add this to min/ max, respectively --> will reduce
    %number of false positive univariate deviation. 
    for i = 1:size(dataTest,2)
        for ii = 1:size(dataTest,1)
            if dataTest(ii,i) < univariateMin(i) && dataTest(ii,i) > univariateMax(i)
                uniVariateViolationsDataTest(ii,i) = dataTest(ii,i);
                fprintf("Attention - Univariate violation in data set ",ii," at position ",i,"! \r\n");
            end
        end
    end
else
    univariatePerctile = 0.5;            % read in in future version ... at present: covers 99% percentile from 0.5 % to 99.5%
    univariateMin = prctile(dataTraining,univariatePerctile);     % min for each column (datapoint)
    univariateMax = prctile(dataTraining,100-univariatePerctile);     % max for each column
    % future versions: find algorith that (ideally) automatically determines noise
    % level in the data set. Write out those noise positition. 
    % When encountering a noise position  switch to minmax version in order
    % to avoid extensive number of false positive results.
    univariateNoiseLevel = 0;
    for i = 1:size(dataTest,2)
        for ii = 1:size(dataTest,1)
            % if not noise then ...
            if dataTest(ii,i) < (univariateMin(i)-univariateNoiseLevel) && dataTest(ii,i) > (univariateMax(i)-univariateNoiseLevel
                uniVariateViolationsDataTest(ii,i) = dataTest(ii,i);
                fprintf("Attention - Univariate violation in data set ",ii," at position ",i,"! \r\n");
            end
            % elseif noisy data
            %switch to alternative algorithm
            %end if
        end
    end
end