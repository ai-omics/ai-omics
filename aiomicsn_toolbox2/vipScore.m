function vipScores = vipScore(loadings,explainedVariance)
%Fuction for calculation of VIP (variable influence on projection) scores
%
% part of the AI(omics)n distribution 
% https://gitlab.com/ai-omics/ai-omics
% (c) 2023 by the authors
% distributed under the conditions listed in the file
% classificationUI2.m and the documentation
%
% according to S. Wold et al. / Chemometrics and Intelligent Laboratory
% Systems 58 (2001) 109-130
%   loadings:           mxn loadings matrix with m PLS/PCA components and 
%                       n variables
%   explainedVariance:  mx1 matrix containing explained variance per
%                       component with m PLS/PCA components 

% Sqaure loadings for positive only values
loadingsSqared = loadings.^2;

% Check explainedVariance for correct dimensions
if (size(explainedVariance,2) >  1) || (size(explainedVariance,1) ~=  size(loadings,1))
    error('vipScore: number of components in loadings and explainedVaraince not matching');
end

% Weight components according to explained variance
loadingsWeighted = loadingsSqared.*explainedVariance;

% Sum per variable
vipScores = sum(loadingsWeighted,1);

% Scale to 100
vipScores = vipScores/max(vipScores)*100;
end