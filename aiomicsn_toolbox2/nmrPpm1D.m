function ppm = nmrPpm1D(spec)
% Return ppm values corresponding to intensities in spec.Data
%
% part of the AI(omics)n distribution 
% https://gitlab.com/ai-omics/ai-omics
% (c) 2023 by the authors
% distributed under the conditions listed in the file
% classificationUI2.m and the documentation 

ppm = [spec.maxppm:-(spec.maxppm - spec.minppm)/(spec.size - 1):spec.minppm];
end

