function [groupsCleaned,dataCleaned,barcodeCleaned] = groupsClean(groups,barcodeGroups,data,barcode)
% Function for cleaning up group assignments
%
% part of the AI(omics)n distribution 
% https://gitlab.com/ai-omics/ai-omics
% (c) 2023 by the authors
% distributed under the conditions listed in the file
% classificationUI2.m and the documentation

[dataCleaned,barcodeCleaned] = dataClean(data,barcode);

[~,barcodeGroupsCommon,barcodeCommon] = intersect(barcodeGroups,barcodeCleaned);

barcodeCleaned = barcodeGroups(barcodeGroupsCommon);
groupsCleaned = groups(barcodeGroupsCommon);
dataCleaned = dataCleaned(barcodeCommon,:);
end