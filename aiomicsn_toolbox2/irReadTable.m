function [origin,barcodeData,data,barcodeDataTest,dataTest,originInterval] = irReadTable(origin,barcodeData,dataOld,barcodeDataTest,dataTestOld,source,irType,originInterval)
% Function for reading IR spectra from Excel spreadsheet
%
% part of the AI(omics)n distribution 
% https://gitlab.com/ai-omics/ai-omics
% (c) 2023 by the authors
% distributed under the conditions listed in the file
% classificationUI2.m and the documentation

pathTable = source{1};
pathTableTest = source{2};
columnBarcode = source{3};
columnData = source{4};
binsCount = source{5};
binsInterval = source{6};
binsExclusuion = source{7};
if(length(source) >= 8)
    spectrumType = source{8};
else
    spectrumType = 'AB';
end
if(length(source) >= 9)
    spectrumTypeIndex = source{9};
else
    spectrumTypeIndex = -1;
end

%% Test dataset available?
if length(pathTableTest) < 1
    pathTableTestTrue = false;
else
    pathTableTestTrue = true;
end

%% Simulated spectra?
if ismember('Simuliert',pathTable(1))
    simulated = true;
else
    simulated = false;
end

%% Read tables
if ~simulated
[~,~,table] = xlsread(strrep(pathTable,'"',''));
end
if pathTableTestTrue
    [~,~,tableTest] = xlsread(strrep(pathTableTest,'"',''));
end

%% Extract barcodes
if ~simulated
    barcode = barcodeRead(table,columnBarcode);
else
    spectra = pathTable{1,2};
    barcode = (1:size(spectra,1)-1)';
end
if pathTableTestTrue
    barcodeTest = barcodeRead(tableTest,columnBarcode);
else
    barcodeTest = [];
end

%% Read spectra
if ~simulated
    if length(columnData) > 1
        waveNumber = cell2mat(table(1,columnData));
        spectra = table(2:end,columnData);
        for j = 1:size(spectra,1)
            for k = 1:size(spectra,2)
                if ~isnumeric(spectra{j,k})
                    spectra{j,k} = NaN;
                end
            end
        end
        spectra = cell2mat(spectra);
    else
        % Generate filelist
        filelist = table(2:end,columnData);
        
        % Exclude not existing files
        existingBoolean = find(isfile(filelist));
        barcode = barcode(existingBoolean);
        filelist = filelist(existingBoolean);
        
        % Read spectra from files
        spectraRaw = irReadList(filelist, spectrumType, spectrumTypeIndex);
        
        % Get spectra size
        spectraSize = zeros(size(spectraRaw,1),1);
        for j = 1:size(spectraRaw,1)
            spectraSize(j) = length(spectraRaw(j).wavenumber);
        end
        
        % Get common spectrum length
        spectraSizeUnique = tabulate(spectraSize);
        [~,spectraSizeCommon] = max(spectraSizeUnique(:,2));
        
        % Exclude spectra off common size
        barcode = barcode(spectraSize == spectraSizeCommon);
        spectraRaw = spectraRaw(spectraSize == spectraSizeCommon);
        
        % Get wave numbers
        waveNumber = spectraRaw(1).wavenumber;
        
        % Combine multiple measurements
        barcodeUnique = tabulate(barcode);
        barcodeUnique = barcodeUnique(barcodeUnique(:,2)>0,:);
        
        spectra = zeros(size(barcodeUnique,1),length(waveNumber));
        
        for j = 1:size(barcodeUnique,1)
            barcodeIndex = find(barcode == barcodeUnique(j,1));
            for k = 1:length(barcodeIndex)
                spectra(j,:) = spectra(j,:) + spectraRaw(barcodeIndex(k)).intensity;
            end
            spectra(j,:) = spectra(j,:)/length(barcodeIndex);
        end
        
        barcode = barcodeUnique(:,1);
    end
else
    waveNumber = spectra(1,:);
    spectra = spectra(2:size(spectra,1),:);
end
if pathTableTestTrue
    if length(columnData) > 1
        spectraTest = cell2mat(tableTest(2:end,columnData));
    else
        % Generate filelist
        filelistTest = tableTest(2:end,columnData);
        
        % Exclude not existing files
        existingBoolean = find(isfile(filelistTest));
        barcodeTest = barcodeTest(existingBoolean);
        filelistTest = filelistTest(existingBoolean);
        
        % Read spectra from files
        spectraTestRaw = irReadList(filelistTest);
        
        % Get spectum size
        spectraTestSize = zeros(size(spectraTestRaw,1),1);
        for j = 1:size(spectraTestRaw,1)
            spectraTestSize(j) = length(spectraTestRaw(j).wavenumber);
        end
        
        % Exclude spectra off common size
        barcodeTest = barcodeTest(spectraTestSize == spectraSizeCommon);
        spectraTestRaw = spectraTestRaw(spectraTestSize == spectraSizeCommon);
        
        % Combine multiple measurements
        barcodeTestUnique = tabulate(barcodeTest);
        barcodeTestUnique = barcodeTestUnique(barcodeTestUnique(:,2)>0,:);
        
        spectraTest = zeros(size(barcodeTestUnique,1),length(waveNumber));
        
        for j = 1:size(barcodeTestUnique,1)
            barcodeTestIndex = find(barcodeTest == barcodeTestUnique(j,1));
            for k = 1:length(barcodeTestIndex)
                spectraTest(j,:) = spectraTest(j,:) + spectraTestRaw(barcodeTestIndex(k)).intensity;
            end
            spectraTest(j,:) = spectraTest(j,:)/length(barcodeTestIndex);
        end
        
        barcodeTest = barcodeTestUnique(:,1);
    end
end

%% Binning
data = NaN(size(spectra,1),binsCount);
if pathTableTestTrue
    dataTest = NaN(size(spectraTest,1),binsCount);
else
    dataTest = NaN(1,binsCount);
end

if length(binsInterval) > 0
    binsSize = (binsInterval(2)-binsInterval(1))/binsCount;
else
    binsSize = (max(waveNumber)-min(waveNumber))/binsCount;
end
binsWaveNumber = zeros(1,binsCount);
binsList = zeros(2,binsCount);

for j = 1:binsCount
    if length(binsInterval) > 0
        binsList(1,j) = binsSize*(j-1) + binsInterval(1);
        binsList(2,j) = binsSize*j + binsInterval(1);
    else
        binsList(1,j) = binsSize*(j-1) + min(waveNumber);
        binsList(2,j) = binsSize*j + min(waveNumber);
    end
    
    binsWaveNumber(j) = (binsList(1,j)+binsList(2,j))/2;
    
    binsIndex = waveNumber > binsList(1,j) & waveNumber <= binsList(2,j);
    
    for k = 1:size(spectra,1)
        data(k,j) = sum(spectra(k,binsIndex))/sum(binsIndex);
    end
    if pathTableTestTrue
        for k = 1:size(spectraTest,1)
            dataTest(k,j) = sum(spectraTest(k,binsIndex))/sum(binsIndex);
        end
    end
end

%% Apply exclusions
if length(binsExclusuion) > 0
    binsExluded = (binsWaveNumber >= binsExclusuion(:,1)) & (binsWaveNumber <= binsExclusuion(:,2));
    binsExluded = sum(binsExluded,1) > 0;
    
    data = data(:,~binsExluded);
    dataTest = dataTest(:,~binsExluded);
    binsWaveNumber = binsWaveNumber(~binsExluded);
end

%% Save origin
originTemp = cell(3,size(data,2));
originTemp(1,:) = {'IR'};
originTemp(2,:) = {irType};
originTemp(3,:) = num2cell(binsWaveNumber);

%% Append data
[origin,barcodeData,data,barcodeDataTest,dataTest,originInterval] = dataAppend(origin,originTemp,barcodeData,dataOld,barcode,data,barcodeDataTest,dataTestOld,barcodeTest,dataTest,originInterval);
end

