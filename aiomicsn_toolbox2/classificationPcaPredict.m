function [groupsTest,groupsTestProbability,stats] = classificationPcaPredict(model,stats,dataTest,varargin)
% Script for classification predictions via PCA/LDA
%
% part of the AI(omics)n distribution 
% https://gitlab.com/ai-omics/ai-omics
% (c) 2023 by the authors
% distributed under the conditions listed in the file
% classificationUI2.m and the documentation

%% Apply feature selection
dataTest = dataTest(:,stats.featureSelectionSorting);

%% PCA/LDA predictive
% Apply to testdata
% unit-variance scaling
dataTest = dataTest ./ repmat(model.s,size(dataTest,1),1);

% subtract mean
dataTest = dataTest - repmat(model.m,size(dataTest,1),1);

% apply PCA
testPcaScore = dataTest/stats.loadingsX(1:stats.dimensionsPca,:);

if model.edcBoolean
    % Predicting test samples
    fprintf("Predicting Test samples\r\n");
    testDistributionEuclidean = pdist2(testPcaScore,model.modelMean,'euclidean');
    [~,testPrediction]=min(testDistributionEuclidean,[],2);
    groupsTestProbability = NaN(size(dataTest,1),size(model.groupsDistinct,1));
    for i = 1:size(model.groupsDistinct,1)
        groupsTestProbability(:,i) = mvnpdf(testPcaScore,model.modelMean(i,:),model.modelMeanCov{i});
    end
    groupsTest = model.groupsDistinct(testPrediction,1);
    testPlotScore = testPcaScore;
end
if model.ldaBoolean | model.qdaBoolean | model.knnBoolean
    % Predict test samples
    fprintf("Predicting Test samples\r\n");
    [groupsTest,groupsTestProbability,~] = predict(model.modelDa,testPcaScore);
    %stats.groupsTestProbability = groupsTestProbability;
end
stats.prediction.groups = groupsTest;
stats.prediction.probabilities = groupsTestProbability;

if model.ldaBoolean
    testPlotScore = testPcaScore*model.ldaEigenvectors;
end
if model.qdaBoolean
    for j = 1:size(model.groupsDistinct,1)
        testPlotScore(ismember(groupsTest,model.groupsDistinct{j,1}),:) = testPcaScore(ismember(groupsTest,model.groupsDistinct{j,1}),:)*squeeze(model.ldaEigenvectors(:,:,j));
    end
end
if model.knnBoolean
    testPlotScore = testPcaScore;
end

% Calculate distances
testDistributionEuclidean = zeros(size(dataTest,1), size(model.groupsDistinct,1));
testDistributionMahalanobis = zeros(size(dataTest,1), size(model.groupsDistinct,1));
if stats.settings.enableMahalanobis == 1
    for j = 1:size(model.groupsDistinct,1)
        n = ismember(model.groups,model.groupsDistinct{j,1});
        testDistributionEuclidean(:, j) = pdist2(testPlotScore, mean(model.cvPlotScore(n, :), 1),'euclidean');
        testDistributionMahalanobis(:, j) = mahal(testPlotScore, model.cvPlotScore(n,:));
    end
end
stats.prediction.distributionEuclidean = testDistributionEuclidean;
stats.prediction.distributionMahalanobis = testDistributionMahalanobis;

%% Plot
if model.plotBoolean
    CM = lines(size(model.groupsDistinct,1));
    
    fprintf("Plotting prediction\r\n");
    figure('Name','Model - Prediction Testset');
    hold on;
    if stats.settings.displayCalibration == 1
      for i=1:size(model.groupsDistinct,1)
          ii = find(ismember(model.groups,model.groupsDistinct(i,1)));
          plot3(model.cvPlotScore(ii,1),model.cvPlotScore(ii,2),model.cvPlotScore(ii,3),'ko','MarkerFaceColor',CM(i,:));
      end
    end
    for i=1:size(model.groupsDistinct,1)
        surf(squeeze(model.cvEllipsoidValues(i,1,:,:)),squeeze(model.cvEllipsoidValues(i,2,:,:)),squeeze(model.cvEllipsoidValues(i,3,:,:)),'EdgeColor',CM(i,:),'LineStyle',':','FaceColor',CM(i,:),'FaceAlpha',0.3);
    end
    if model.textBoolean && stats.settings.displayCalibration == 1
        text(model.cvPlotScore(:,1),model.cvPlotScore(:,2),model.cvPlotScore(:,3),num2str([1:1:size(model.cvPlotScore,1)]'));
    end
    for i=1:size(model.groupsDistinct,1)
        ii = find(ismember(groupsTest,model.groupsDistinct(i,1)));
        plot3(testPlotScore(ii,1),testPlotScore(ii,2),testPlotScore(ii,3),'ks','MarkerFaceColor',CM(i,:),'MarkerSize',14);
    end
    if model.textBoolean
        text(testPlotScore(:,1),testPlotScore(:,2),testPlotScore(:,3),num2str([1:1:size(testPlotScore,1)]'));
    end
    
    axis vis3d;
    box on;
    grid on;
    legend(model.groupsDistinct(:,1));
    xlabel('PC1');
    ylabel('PC2');
    zlabel('PC3');
    view(30,10);
    hold off;
end
end