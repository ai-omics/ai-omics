function [origin,barcodeData,data,barcodeDataTest,dataTest,originInterval] = otherReadTable(origin,barcodeData,dataOld,barcodeDataTest,dataTestOld,source,originInterval)
% Function for reading other data from Excel spreadsheet
%
% part of the AI(omics)n distribution 
% https://gitlab.com/ai-omics/ai-omics
% (c) 2023 by the authors
% distributed under the conditions listed in the file
% classificationUI2.m and the documentation

pathTable = source{1};
pathTableTest = source{2};
columnBarcode = source{3};
columnData = source{4};
dataType = source{5};
if length(source) < 6
    expansionBoolean = false;
else
    expansionBoolean = source{6};
end

%% Test dataset available?
if length(pathTableTest) < 1
    pathTableTestTrue = false;
else
    pathTableTestTrue = true;
end

%% Read table
[~,~,table] = xlsread(strrep(pathTable,'"',''));
if pathTableTestTrue
    [~,~,tableTest] = xlsread(strrep(pathTableTest,'"',''));
end

%% Extract barcodes
barcode = barcodeRead(table,columnBarcode);
if pathTableTestTrue
    barcodeTest = barcodeRead(tableTest,columnBarcode);
else
    barcodeTest = [];
end

%% Read data
dataHeader = table(1,columnData);
dataHeader = cellfun(@num2str, dataHeader, 'uniformoutput', false);
data = cell2mat(table(2:end,columnData));
if pathTableTestTrue
    dataTest = cell2mat(tableTest(2:end,columnData));
else
    dataTest = NaN(1,size(data,2));
end

%% Expand variables
if expansionBoolean
    dataSizeSource = size(data,2);
    
    % A/B
    for j = 1:dataSizeSource
        for k = j+1:dataSizeSource
            dataSize = size(data,2);
            data(:,dataSize+1) = data(:,j)./data(:,k);
            data(isinf(data(:,dataSize+1)),dataSize+1) = 1; % Replace Inf from division by 0 with 1
            if pathTableTestTrue
                dataTest(:,dataSize+1) = dataTest(:,j)./dataTest(:,k);
                dataTest(isinf(dataTest(:,dataSize+1)),dataSize+1) = 1;
            end
            dataHeader{dataSize+1} = strcat(dataHeader{j},'/',dataHeader{k});
        end
    end
    
    % A*B
    for j = 1:dataSizeSource
        for k = j+1:dataSizeSource
            dataSize = size(data,2);
            data(:,dataSize+1)=data(:,j).*data(:,k);
            if pathTableTestTrue
                dataTest(:,dataSize+1) = dataTest(:,j).*dataTest(:,k);
            end
            dataHeader{dataSize+1} = strcat(dataHeader{j},'*',dataHeader{k});
        end
    end
    
    % 1/A
    dataSizeSource = size(data,2);
    
    for j = 1:dataSizeSource
        dataSize = size(data,2);
        data(:,dataSize+1) = 1./data(:,j);
        data(isinf(data(:,dataSize+1)),dataSize+1) = 1; % Replace Inf from division by 0 with 1
        if pathTableTestTrue
            dataTest(:,dataSize+1) = 1./dataTest(:,j);
            dataTest(isinf(dataTest(:,dataSize+1)),dataSize+1) = 1;
        end
        dataHeader{dataSize+1} = strcat('1/(',dataHeader{j},')');
    end
    
    % A^2
%     for j = 1:dataSizeSource
%         dataSize = size(data,2);
%         data(:,dataSize+1) = data(:,j).^2;
%         if pathTableTestTrue
%             dataTest(:,dataSize+1) = dataTest(:,j).^2;
%             dataTest(isinf(dataTest(:,dataSize+1)),dataSize+1) = 1;
%         end
%         dataHeader{dataSize+1} = strcat('(',dataHeader{j},')^2');
%    end
    
    % log(A)
    for j = 1:dataSizeSource
        dataSize = size(data,2);
        data(:,dataSize+1) = log(abs(data(:,j)));
        data(isinf(data(:,dataSize+1)),dataSize+1) = 0; % Replace Inf from log of negative values with 0
        if pathTableTestTrue
            dataTest(:,dataSize+1) = log(abs(dataTest(:,j)));
            dataTest(isinf(dataTest(:,dataSize+1)),dataSize+1) = 1;
        end
        dataHeader{dataSize+1} = strcat('log(',dataHeader{j},')');
    end
    
    % Correct dataTest size, if no test samples are used
    if ~pathTableTestTrue
        dataTest = NaN(1,size(data,2));
    end
end
%% Save origin
originTemp = cell(3,size(data,2));
originTemp(1,:) = {'other'};
originTemp(2,:) = {dataType};
originTemp(3,:) = dataHeader;

%% Daten anf�gen
[origin,barcodeData,data,barcodeDataTest,dataTest,originInterval] = dataAppend(origin,originTemp,barcodeData,dataOld,barcode,data,barcodeDataTest,dataTestOld,barcodeTest,dataTest,originInterval);
end

