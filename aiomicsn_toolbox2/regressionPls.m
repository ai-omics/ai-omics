function [stats,valuesTest,valuesMean,valuesTestLinear] = regressionPls(data,values,dataTest,varargin)
% Script for regressions via (O)PLS
%
% part of the AI(omics)n distribution 
% https://gitlab.com/ai-omics/ai-omics
% (c) 2023 by the authors
% distributed under the conditions listed in the file
% classificationUI2.m and the documentation

%% Stats
stats = {};

%% Parese varargin
fprintf("Parsing arguments\r\n");

p = inputParser;
p.addParameter('plot',false);
p.addParameter('opls',false);
p.addParameter('featureSelect',false);
p.addParameter('simulated',false);
p.parse(varargin{:});
pars = p.Results;

plotBoolean = pars.plot;
oplsBoolean = pars.opls;
featureSelectBoolean = pars.featureSelect;
if pars.simulated == false
    simulatedBoolean = false;
    originSimulation = [];
else
    simulatedBoolean = true;
    originSimulation = pars.simulated;
end

%% Set parameters
fprintf("Setting parameters\r\n");
dimensionsRegressionMax = 18;
dimensionsRegressionMin = 3;
dimensionsGeneticAlgorithm = 50;
populationGeneticAlgorithm = 32;
iterationsGeneticAlgorithm = 100;
mutationRateGeneticAlgorithm = 2;
crossvalidationStepsRegression = 1000;
crossvalidationShareRegression = 5;
outliersCutoff = 3;
loqK = 3;

if isfile('settings.ini')
    settingsIni = textread('settings.ini','%s','commentstyle','shell');
    settingsIni = strrep(settingsIni,':','');
    
    dimensionsRegressionMax = settingsIniRead(settingsIni,dimensionsRegressionMax);
    dimensionsRegressionMin = settingsIniRead(settingsIni,dimensionsRegressionMin);
    dimensionsGeneticAlgorithm = settingsIniRead(settingsIni,dimensionsGeneticAlgorithm);
    populationGeneticAlgorithm = settingsIniRead(settingsIni,populationGeneticAlgorithm);
    iterationsGeneticAlgorithm = settingsIniRead(settingsIni,iterationsGeneticAlgorithm);
    mutationRateGeneticAlgorithm = settingsIniRead(settingsIni,mutationRateGeneticAlgorithm);
    crossvalidationStepsRegression = settingsIniRead(settingsIni,crossvalidationStepsRegression);
    crossvalidationShareRegression = settingsIniRead(settingsIni,crossvalidationShareRegression);
    outliersCutoff = settingsIniRead(settingsIni,outliersCutoff);
    loqK = settingsIniRead(settingsIni,loqK);
end

if dimensionsRegressionMax >= size(data,2)
    dimensionsRegressionMax = size(data,2)-1;
end
if dimensionsGeneticAlgorithm > size(data,2)
    dimensionsGeneticAlgorithm = size(data,2);
end

stats.dimensionsGeneticAlgorithm = dimensionsGeneticAlgorithm;
stats.populationGeneticAlgorithm = populationGeneticAlgorithm;
stats.iterationsGeneticAlgorithm = iterationsGeneticAlgorithm;
stats.mutationRateGeneticAlgorithm = mutationRateGeneticAlgorithm;
stats.crossvalidationStepsRegression = crossvalidationStepsRegression;
stats.crossvalidationShareRegression = crossvalidationShareRegression;
stats.outliersCutoff = outliersCutoff;
stats.loqK = loqK;

%% Feature selection
if featureSelectBoolean
    % Initialize
    fprintf("Selection of relevant bins\r");
    dimensionsRegression = 3;
    featureGenes = false(populationGeneticAlgorithm*4,size(data,2));
    featureRmsecv = zeros(populationGeneticAlgorithm*4,1);
    
    for j = 1:populationGeneticAlgorithm*4
        featureGenes(j,randperm(size(data,2),dimensionsGeneticAlgorithm)) = true;
    end
    
    % Genetic algorithm
    reverseStr = '';
    for j = 1:iterationsGeneticAlgorithm
        msg = sprintf('\nStep %3.0f/%3.0f', j, iterationsGeneticAlgorithm);
        fprintf([reverseStr, msg]);
        reverseStr = repmat(sprintf('\b'), 1, length(msg));
                
        % Evaluate genes
        parfor k = 1:populationGeneticAlgorithm*4
            [~,~,~,~,~,~,featurePlsMse] = plsregress(data(:,featureGenes(k,:)),values,dimensionsRegression,'cv',10,'mcreps',10,'options',statset('UseParallel',true));
            featureRmsecv(k) = min(featurePlsMse(2,:));
        end
        
        % Keep best quarter of genes
        [~,featureRmsecvMinIdx] = sort(featureRmsecv,'ascend');
        featureGenes(1:populationGeneticAlgorithm,:) = featureGenes(featureRmsecvMinIdx(1:populationGeneticAlgorithm),:);
        
        if j == iterationsGeneticAlgorithm
            break;
        end
        
        % Generate 2nd quarter of genes by combining
        for k = populationGeneticAlgorithm+1:populationGeneticAlgorithm*2
            tempCuttingSite = randperm(size(data,2),1);
            tempCombinedGenes = randperm(populationGeneticAlgorithm,2);
            featureGenes(k,1:tempCuttingSite-1) = featureGenes(tempCombinedGenes(1),1:tempCuttingSite-1);
            featureGenes(k,tempCuttingSite:size(data,2)) = featureGenes(tempCombinedGenes(2),tempCuttingSite:size(data,2));
        end
        
        % Mutate
        featureGenes(populationGeneticAlgorithm*2+1:end,:) = featureGenes(1:populationGeneticAlgorithm*2,:);
        for k = populationGeneticAlgorithm*2+1:populationGeneticAlgorithm*4
            tempMutationSite = randperm(size(data,2),mutationRateGeneticAlgorithm);
            featureGenes(k,tempMutationSite) = ~featureGenes(k,tempMutationSite);
        end
        
        % Keep dimensions at selected value
        featureGenesSum = sum(featureGenes,2);
        for k = 1:populationGeneticAlgorithm*4
            if featureGenesSum(k) > dimensionsGeneticAlgorithm
                tempTrue = find(featureGenes(k,:));
                featureGenes(k,tempTrue(randperm(length(tempTrue),featureGenesSum(k)-dimensionsGeneticAlgorithm))) = false;
            elseif featureGenesSum(k) < dimensionsGeneticAlgorithm
                tempFalse = find(~featureGenes(k,:));
                featureGenes(k,tempFalse(randperm(length(tempFalse),dimensionsGeneticAlgorithm-featureGenesSum(k)))) = true;
            end
        end
    end
    
    % Apply selected features
    fprintf("\r\nApplying selected bins\r\n");
    data = data(:,featureGenes(1,:));
    dataTest = dataTest(:,featureGenes(1,:));
    stats.selectedBins = find(featureGenes(1,:));
end

%% Mean-Centering and univariate scaling
fprintf("Scaling data\r\n");
dataStd = std(data);
dataStd(dataStd==0) = 1;
dataScaled = data ./repmat(dataStd,size(data,1),1);
dataTestScaled = dataTest ./repmat(dataStd,size(dataTest,1),1);
dataMean = mean(dataScaled);
dataScaled = dataScaled - repmat(dataMean,size(dataScaled,1),1);
dataTestScaled = dataTestScaled - repmat(dataMean,size(dataTestScaled,1),1);

%% Setting PLS components
fprintf("Determine the number of required PLS components\r\n");
dimensionsRegression = 100;

if(min(size(dataScaled))<=100)
    dimensionsRegression = min(size(dataScaled))-1;
end

[~,~,~,~,~,plsVariance] = plsregress(dataScaled,values,dimensionsRegression,'cv',10,'mcreps',10);
dimensionsRegression = plsVariance(2,2:end) > plsVariance(2,1:end-1);
dimensionsRegression = find(dimensionsRegression(4:end));
if isempty(dimensionsRegression)
    dimensionsRegression = 46;
end
dimensionsRegression = dimensionsRegression(1)+3;

if dimensionsRegression > dimensionsRegressionMax
    dimensionsRegression = dimensionsRegressionMax;
end

if dimensionsRegression < dimensionsRegressionMin
    dimensionsRegression = dimensionsRegressionMin;
end

if min(size(dataScaled)) <= dimensionsRegression
    dimensionsRegression = min(size(dataScaled))-1;
end

if dimensionsRegression < 4
    error('Not enough samples or variables!');
end

stats.plsComponents = dimensionsRegression;
stats.dimensionsRegression = dimensionsRegression;

%% Data pretreatment
fprintf("Performing pretreatment of data\r\n");
methods = ["No pretreatment";"1. derivation";"2. derivation";"Vector normalization";"Pareto scaling"];
stats.methods = methods;
dataTreated = zeros(length(methods),size(dataScaled,1),size(dataScaled,2));
dataTestTreated = zeros(length(methods),size(dataTestScaled,1),size(dataTestScaled,2));

% No pretreatment
dataTreated(1,:,:) = dataScaled;
dataTestTreated(1,:,:) = dataTestScaled;

% 1. derivation
for i = 1:length(dataScaled(:,1))
    dataTreated(2,i,:) = [diff(dataScaled(i,:)),0];
end
for i = 1:length(dataTestScaled(:,1))
    dataTestTreated(2,i,:) = [diff(dataTestScaled(i,:)),0];
end

% 2. derivation
temp = squeeze(dataTreated(2,:,:));
for i = 1:length(dataScaled(:,1))
    dataTreated(3,i,:) = [diff(temp(i,:)),0];
end
temp = squeeze(dataTestTreated(2,:,:));
for i = 1:length(dataTestScaled(:,1))
    dataTestTreated(3,i,:) = [diff(temp(i,:)),0];
end

% Vector normalization
for i = 1:length(dataScaled(:,1))
    dataTreated(4,i,:) = dataScaled(i,:)/norm(dataScaled(i,:));
end
for i = 1:length(dataTestScaled(:,1))
    dataTestTreated(4,i,:) = dataTestScaled(i,:)/norm(dataTestScaled(i,:));
end

% Pareto Scaling
for i = 1:length(dataScaled(:,1))
    dataTreated(5,i,:) = (dataScaled(i,:)-mean(dataScaled(i,:)))/sqrt(std(dataScaled(i,:)));
end
for i = 1:length(dataTestScaled(:,1))
    dataTestTreated(5,i,:) = (dataTestScaled(i,:)-mean(dataTestScaled(i,:)))/sqrt(std(dataTestScaled(i,:)));
end

%% Monte Carlo cross validation for different pretreatment methods
deviationStd = zeros(length(methods),1);
deviationRmsecv = zeros(length(methods),1);

testLength = round(size(dataTreated,2)*crossvalidationShareRegression/100);

RSSCV = zeros(crossvalidationStepsRegression,length(methods));
TSSCV = zeros(crossvalidationStepsRegression,length(methods));

cvResults = cell(length(methods),1);

parfor i = 1:length(methods)
    fprintf("Testing data pretreatment: %s\r\n", methods(i));
    data = squeeze(dataTreated(i,:,:));
    fprintf("Beginning with cross validation\r\n");
    
    fprintf("Steps: %3.0f, share of test spectra: %3.0f percent\r", crossvalidationStepsRegression, crossvalidationShareRegression);
    
    reverseStr = '';
    for crossvalidationStep=1:crossvalidationStepsRegression
        msg = sprintf('\nStep %3.0f/%3.0f', crossvalidationStep, crossvalidationStepsRegression);
        fprintf([reverseStr, msg]);
        reverseStr = repmat(sprintf('\b'), 1, length(msg));
        
        % Generate model and test dataset
        testSet = randperm(length(data(:,1)),testLength);
        modelSet = setdiff(1:length(data(:,1)),testSet);
        
        modelData = data(modelSet(:),:);
        modelValues = values(modelSet(:));
        testData = data(testSet(:),:);
        testValues = values(testSet(:));
        
        % Remove spectra from same origin when using simulated spectra
        if simulatedBoolean
            testOrigin = originSimulation(testSet,:);
            testOriginBoolean = sum((testOrigin == testOrigin(1,1) | testOrigin == testOrigin(1,2)),2) > 1;
            modelOrigin = originSimulation(modelSet,:);
            modelOriginBoolean = ~(sum((modelOrigin == testOrigin(1,1) | modelOrigin == testOrigin(1,2)),2) > 0);
            
            modelData = modelData(modelOriginBoolean,:);
            modelValues = modelValues(modelOriginBoolean);
            modelSet = modelSet(modelOriginBoolean);
            testData = testData(testOriginBoolean,:);
            testValues = testValues(testOriginBoolean);
            testSet = testSet(testOriginBoolean);
        end
        
        % OPLS
        if oplsBoolean
            [~,P_O,W_O,modelData] = oplsDo(modelData,modelValues);
        end
        
        % Generate PLS model
        [~,loadingsY,scoresX,~,plsModel] = plsregress(modelData,modelValues,dimensionsRegression,'cv',10);
        
        % Statistics
        RSSCV(crossvalidationStep,i) = sum((bsxfun(@minus, modelValues, mean(modelValues))-scoresX*loadingsY').^2);
        TSSCV(crossvalidationStep,i) = sum((modelValues-mean(values)).^2);
        
        % OPLS
        if oplsBoolean
            testData = oplsApply(testData,P_O,W_O);
        end
    
        % Apply PLS model
        testPrediction  = [ones(size(testData,1),1) testData]*plsModel;
        
        % Compare real group vs. predicted group
        if crossvalidationStep==1
            result = [testSet(:),testValues,testPrediction];
        else
            result = [result;testSet(:),testValues,testPrediction];
        end
    end
    
    cvResults{i} = result;
    
    fprintf("\r\nCalculating standard deviation\r\n");
    deviation = result(:,3)-result(:,2);
    deviationStd(i) = std(deviation);
    deviationRmsecv(i) = mean(sqrt(deviation.^2));
    
end

stats.deviationStd = deviationStd;
stats.deviationRmsecv = deviationRmsecv;

%% Select best model
bestModel = find(deviationStd == min(deviationStd));
data = squeeze(dataTreated(bestModel,:,:));
dataTest = squeeze(dataTestTreated(bestModel,:,:));
if size(dataTest,2) == 1
    dataTest = dataTest';
end
result = cvResults{bestModel};

%% Predictive PLS model

% Generate PLS model
fprintf("Generating predictive PLS model\r\n");
if oplsBoolean
    [~,P_O,W_O,data] = oplsDo(data,values);
end
[loadingsX,loadingsY,~,~,plsModel,~,~,plsStats] = plsregress(data,values,dimensionsRegression,'cv',10);
stats.loadingsX = loadingsX;

% Statistics (TSS: total sum of squares, RSS: residual sum of squares)
TSS = sum((values-mean(values)).^2);
RSS = sum(plsStats.Yresiduals.^2);
stats.R2 = 1-RSS/TSS;

Q2 = zeros(crossvalidationStepsRegression,1);
for i=1:crossvalidationStepsRegression
    Q2(i) = 1-RSSCV(i,bestModel)/TSSCV(i,bestModel);
end
stats.Q2 = mean(Q2);

% Apply PLS model
fprintf("Applying PLS model\r\n");
if oplsBoolean
    dataTest = oplsApply(dataTest,P_O,W_O);
end
valuesPrediction = [ones(size(data,1),1) data]*plsModel;
valuesTest  = [ones(size(dataTest,1),1) dataTest]*plsModel;

%% Linear fit
fprintf("Correcting predicted values using a linear fit\r\n");

% Calculate y vlaues with x values
linearMdlPred = fitlm(result(:,3),result(:,2),'y~x1-1');
valuesTestLinear = predict(linearMdlPred,valuesTest);

% Calculate x vlaues with y values
linearMdlDesc = fitlm(result(:,2),result(:,3),'y~x1-1');

valuesMax = max(values);
valuesMin = min(values);
if valuesMin > 0
    valuesMin = 0;
end
valuesDelta = round((valuesMax-valuesMin)/100,3,'significant');
valuesMax = valuesMax+valuesDelta;
valuesFit = (valuesMin:valuesDelta:valuesMax)';

[linearPrediction,linearPredictionInterval] = predict(linearMdlDesc,valuesFit,'Prediction','observation','Simultaneous',false);

%% Calculate LOD and LOQ using DIN 32645 method and Küster, Thiel: Analytik
linearMdlDescSlope = linearMdlDesc.Coefficients{1,1};
linearMdlDescIntercept = 0;

linearMdlDescResidualStandardError = sqrt(linearMdlDesc.SSE/(size(result,1)-2));
linearMdlDescQxx = std(result(:,2))^2*(size(result,1)-1);

lod = linearMdlDescResidualStandardError/linearMdlDescSlope*tinv(0.95,size(result,1)-2)*sqrt(1 + 1/size(result,1) + mean(result(:,2))^2/linearMdlDescQxx);

loqKappa = loqK*linearMdlDescResidualStandardError*tinv(0.95,size(result,1)-2)/linearMdlDescSlope;
loqEpsilon = size(result,1)*1*(linearMdlDescQxx - loqKappa^2);
loqZeta = 2*loqKappa^2*size(result,1)*1*mean(result(:,2));
loqEta = loqKappa^2*(linearMdlDescQxx*size(result,1)+linearMdlDescQxx+size(result,1)*1*mean(result(:,2)));
loq = (-loqZeta + sqrt(loqZeta^2 + 4*loqEpsilon*loqEta))/(2*loqEpsilon);

stats.lod = lod;
stats.loq = loq;

%% Test for outliers
fprintf("Test for outliers\r\n");
outliers = zeros(size(data,1),1);
for i = 1:size(data,1)
    if abs(valuesPrediction(i)-values(i)) > 3*deviationStd(bestModel)
        outliers(i) = 1;
    end
end
outliers = find(outliers);
stats.outliers = outliers;

%% Plot
if plotBoolean
    fprintf("Calculating mean values of cross validation\r\n");
    testSets = tabulate(result(:,1));
    testSets = testSets(:,1);
    
    valuesMean = zeros(length(testSets),3);
    
    for i=1:length(testSets)
        testSet = result(find(result(:,1) == testSets(i)),:);
        if size(testSet,1) > 0
            valuesMean(i,1)=testSet(1,2);
            valuesMean(i,2)=mean(testSet(:,3));
            valuesMean(i,3)=std(testSet(:,3));
        end
    end
    
    valuesMean = valuesMean(valuesMean(:,2) ~= 0,:);
    
    fprintf("Plotting Loadings\r\n");
    figure('Name','x-loadings plot');
    plot(1:size(loadingsX,1),loadingsX(:,1:4));
    xlabel('Bins');
    legend('X_1','X_2','X_3','X_4');
    
    fprintf("Plotting prediction\r\n");
    figure('Name','Prediction of cross validation');
    hold on;
    plot(result(:,2),result(:,3),'k.');
    errorbar(valuesMean(:,1),valuesMean(:,2),valuesMean(:,3),'rx');
    plot(valuesFit,linearPrediction,'b-');
    plot(valuesFit,linearPredictionInterval,'b--');
    hold off;
    xlabel('Known values');
    ylabel('Predicted values');
    
    figure('Name','Means of cross validation');
    hold on;
    plot(valuesMean(:,1),valuesMean(:,2),'k.');
    plot(valuesFit,linearPrediction,'b-');
    plot(valuesFit,linearPredictionInterval,'b--');
    hold off;
    xlabel('Known values');
    ylabel('Predicted values');
    
    figure('Name','Prediction of final model');
    hold on;
    plot(values,valuesPrediction,'k.');
    plot(valuesFit,linearPrediction,'b-');
    plot(valuesFit,linearPredictionInterval,'b--');
    hold off;
    xlabel('Known values');
    ylabel('Predicted values');
    
    figure('Name','Residuals');
    hold on;
    bar(sort(valuesMean(:,2)-valuesMean(:,1)));
    hold off;
    ylabel('Divergence of predicted and known values');
end
end