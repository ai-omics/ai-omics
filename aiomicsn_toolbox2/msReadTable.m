function [origin,barcodeData,data,barcodeDataTest,dataTest,originInterval] = msEinlesen(origin,barcodeData,dataOld,barcodeDataTest,dataTestOld,source,originInterval)
% Function for reading MS spectra from Excel spreadsheet
%
% part of the AI(omics)n distribution 
% https://gitlab.com/ai-omics/ai-omics
% (c) 2023 by the authors
% distributed under the conditions listed in the file
% classificationUI2.m and the documentation

addpath('./icoshift_toolbox'); % for Copyright and conditions for redistribution of icoShift --> see IcoShift web site

pathTable = source{1};
pathTableTest = source{2};
columnBarcode = source{3};
columnPath = source{4};
binsCount = source{5};
binsInterval = source{6};
binsExclusion = source{7};
binsIntensityMin = source{8};
pathSpectra = source{9};
pathSpectraTest = source{10};
icoshiftBoolean = source{11};
pqnBoolean = source{12};

%% Test dataset available?
if length(pathTableTest) < 1
    pathSpectraTestTrue = false;
else
    pathSpectraTestTrue = true;
end

%% Read table
[~,~,table] = xlsread(strrep(pathTable,'"',''));
if pathSpectraTestTrue
    [~,~,tableTest] = xlsread(strrep(pathTableTest,'"',''));
end

%% Extract barcodes
barcode = barcodeRead(table,columnBarcode);
if pathSpectraTestTrue
    barcodeTest = barcodeRead(tableTest,columnBarcode);
end

%% Generate file lists
filelist = msFilelistGenerate(table(2:end,columnPath),pathSpectra);
if pathSpectraTestTrue
    filelistTest = msFilelistGenerate(tableTest(2:end,columnPath),pathSpectraTest);
end

%% Check spectra for existence
existingBoolean = find(isfile(filelist));
barcode = barcode(existingBoolean);
filelist = filelist(existingBoolean);
if pathSpectraTestTrue
    existingBooleanTest = find(isfile(filelistTest));
    barcodeTest = barcodeTest(existingBooleanTest);
    filelistTest = filelistTest(existingBooleanTest);
else
    barcodeTest = [];
end

%% Read spectra
spectra = struct;
timeLengthMax = 0;
timeMinMax = 1000;
timeMaxMin = 0;
for j = 1:size(filelist,1)
    spectrum = csvread(filelist{j},1,0);
    spectra(j).Time = spectrum(:,1);
    spectra(j).Intensity = spectrum(:,2);
    if timeLengthMax < length(spectra(j).Time)
        timeLengthMax = length(spectra(j).Time);
    end
    if timeMinMax > max(spectra(j).Time)
        timeMinMax = max(spectra(j).Time);
    end
    if timeMaxMin < min(spectra(j).Time)
        timeMaxMin = min(spectra(j).Time);
    end
end
if pathSpectraTestTrue
    spectraTest = struct;
    for j = 1:size(filelistTest,1)
        spectrum = csvread(filelistTest{j},1,0);
        spectraTest(j).Time = spectrum(:,1);
        spectraTest(j).Intensity = spectrum(:,2);
    end
end

%% Interpolate spectra
timeStep = (timeMinMax - timeMaxMin)/(timeLengthMax - 1);
timeInterpolated = timeMaxMin:timeStep:timeMinMax;

spectraData = zeros(size(spectra,2),timeLengthMax);
for j = 1:length(spectra)
    spectraData(j,:) = interp1(spectra(j).Time,spectra(j).Intensity,timeInterpolated);
end
if pathSpectraTestTrue
    spectraTestData = zeros(size(spectraTest,2),timeLengthMax);
    for j = 1:length(spectraTest)
        spectraTestData(j,:) = interp1(spectraTest(j).Time,spectraTest(j).Intensity,timeInterpolated);
    end
end

%% Apply probabilistic quotient normalization
if pqnBoolean
    spectraReference = median(spectraData);
    spectraQuotients = spectraData./spectraReference;
    spectraQuotientsMedian = median(spectraQuotients,2);
    spectraData = spectraData./spectraQuotientsMedian;
    
    if pathSpectraTestTrue        
        spectraTestQuotients = spectraTestData./spectraReference;
        spectraTestQuotientsMedian = median(spectraTestQuotients,2);
        spectraTestData = spectraTestData./spectraTestQuotientsMedian;
    end
end

%% Apply icoshift
if icoshiftBoolean
    [spectraData,icoshiftIntervals,icoshiftIndices,icoshiftTargetVector] = icoshift('average',spectraData,34);
    
    if pathSpectraTestTrue
        spectraTestData = icoshift(icoshiftTargetVector,spectraTestData,binsCount/2);
    end
end

%% Binning
data = NaN(size(spectraData,1),binsCount);
if pathSpectraTestTrue
    dataTest = NaN(size(spectraTestData,1),binsCount);
else
    dataTest = NaN(1,binsCount);
end

binsSize = (binsInterval(2)-binsInterval(1))/binsCount;
binsTime = zeros(1,binsCount);
binsList = zeros(2,binsCount);

for j = 1:binsCount
    binsList(1,j) = binsSize*(j-1) + binsInterval(1) - binsSize/2;
    binsList(2,j) = binsSize*j + binsInterval(1) - binsSize/2;
    binsTime(j) = (binsList(1,j)+binsList(2,j))/2;
end

for j = 1:binsCount
    binsIndex = timeInterpolated > binsList(1,j) & timeInterpolated <= binsList(2,j);
    data(:,j) = sum(spectraData(:,binsIndex),2)/sum(binsIndex);
end

if pathSpectraTestTrue
    for j = 1:binsCount
        binsIndex = timeInterpolated > binsList(1,j) & timeInterpolated <= binsList(2,j);
        dataTest(:,j) = sum(spectraTestData(:,binsIndex),2)/sum(binsIndex);
    end
end

if length(binsExclusion) > 0
    binsExclusion = sum(binsTime >= binsExclusion(:,1) & binsTime <= binsExclusion(:,2),1) > 0;
    
    binsTime = binsTime(~binsExclusion);
    data = data(:,~binsExclusion);
    dataTest = dataTest(:,~binsExclusion);
end

if binsIntensityMin > 0
    binsIntensityMax = max(max(data));
    binsIntensityMin = binsIntensityMax * binsIntensityMin / 100;
    binsExclusion = max(data) < binsIntensityMin;
    
    binsTime = binsTime(~binsExclusion);
    data = data(:,~binsExclusion);
    dataTest = dataTest(:,~binsExclusion);
end

%% Save origin
originTemp = cell(3,size(data,2));
originTemp(1,:) = {'MS'};
originTemp(2,:) = {'MS'};
originTemp(3,:) = num2cell(binsTime);

%% Daten anf�gen
[origin,barcodeData,data,barcodeDataTest,dataTest,originInterval] = dataAppend(origin,originTemp,barcodeData,dataOld,barcode,data,barcodeDataTest,dataTestOld,barcodeTest,dataTest,originInterval);
end