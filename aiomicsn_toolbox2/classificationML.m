function [stats,groupsTest] = classificationML(data,groups,dataTest,varargin)
% Script for classifications via shallow neural network
%
% EXPERIMENTAL
%
% part of the AI(omics)n distribution 
% https://gitlab.com/ai-omics/ai-omics
% (c) 2023 by the authors
% distributed under the conditions listed in the file
% classificationUI2.m and the documentation

%% Stats
stats = {};

%% Parese varargin
fprintf("Parsing arguments\r\n");
plotBoolean=false;
if ismember('plot',varargin)
    plotBoolean = true;
end

textBoolean = false;
if ismember('text',varargin)
    textBoolean = true;
end

%% Set parameters
fprintf("Setting parameters\r\n");
dimensionsKruskal = 50;
outliersCutoff = 3;

if isfile('einstellungen.ini')
    settingsIni = textread('einstellungen.ini','%s');
    settingsIni = strrep(settingsIni,':','');
    
    dimensionsKruskal = settingsIniRead(settingsIni,dimensionsKruskal);
    outliersCutoff = settingsIniRead(settingsIni,outliersCutoff);
end

if dimensionsKruskal > size(data,2)
    dimensionsKruskal = size(data,2);
end

stats.dimensionsKruskal = dimensionsKruskal;
stats.outliersCutoff = outliersCutoff;

%% Numerical groups
fprintf("Generating logical groups\r\n");
groupsDistinct = tabulate(groups);
groupsNumeric = false(size(groups,1),size(groupsDistinct,1));

for j=1:size(groupsDistinct,1)
    groupsNumeric(:,j) = ismember(groups,groupsDistinct(j,1));
end

%% Kruskal-Wallis selection
kruskalP = kruskalWallisP(data,groups);
[~,b] = sort(kruskalP);
data = data(:,b(1:dimensionsKruskal));
stats.kruskalPSortierung = b(1:dimensionsKruskal);

% Unit-variance scaling
s = std(data);
data = data ./repmat(s,size(data,1),1);

% Meancentering
m = mean(data);
data = data - repmat(m,size(data,1),1);


%% Neurales Netzwerk erzeugen
% Create a Pattern Recognition Network
fprintf("Generate neural network\r\n");
modelTrainFcn = 'trainscg';
modelHiddenLayerSize = 10;
modelNet = patternnet(modelHiddenLayerSize, modelTrainFcn);

% Setup Division of Data for Training, Validation, Testing
modelNet.divideParam.trainRatio = 95/100;
modelNet.divideParam.valRatio = 5/100;
modelNet.divideParam.testRatio = 0/100;

% Train the Network
fprintf("Train neural network\r\n");
[modelNet,modelNetTraining] = train(modelNet,data',groupsNumeric');
stats.modelNetTraining = modelNetTraining;

%% Apply model
fprintf("Apply neural network\r\n");

%Kruskal-Wallis-selection of significant variables
dataTest = dataTest(:,b(1:dimensionsKruskal));

% unit-variance scaling
dataTest = dataTest ./ repmat(s,size(dataTest,1),1);

% subtract mean
dataTest = dataTest - repmat(m,size(dataTest,1),1);

% Vohersage treffen
modelPredictionNumeric = modelNet(data')';
testPredictionNumeric = modelNet(dataTest')';

% Konfusionsmatrix
[~,modelPrediction] = max(modelPredictionNumeric,[],2);
for j = 1:size(groupsDistinct,1)
    confusionMatrix(1,j+1) = groupsDistinct(j,1);
    confusionMatrix(j+1,1) = groupsDistinct(j,1);
end
for j = 1:size(groupsDistinct,1)
    n = find(ismember(groups,groupsDistinct(j,1)));
    for jj = 1:size(groupsDistinct,1)
        confusionMatrix(1+jj,1+j) = num2cell(sum(ismember(groupsDistinct(modelPrediction(n),1),groupsDistinct(jj,1))));
    end
end
stats.confusionMatrix = confusionMatrix;

% Predicting Test samples
fprintf("Predicting Test samples\r\n");
[~,testPrediction] = max(testPredictionNumeric,[],2);
groupsTest = groupsDistinct(testPrediction,1);

%% Outliers
stats.outliers = [];

%% Plot
if plotBoolean
    %Nothing to plot
end
end