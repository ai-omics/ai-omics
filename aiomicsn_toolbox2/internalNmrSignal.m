function internalNmrSignal = internalNmrSignal(spektralbereich, signalPosition, signalBreite, anteilGauss, spektrometerFrequenz)
%
% part of the AI(omics)n distribution 
% https://gitlab.com/ai-omics/ai-omics
% (c) 2023 by the authors
% distributed under the conditions listed in the file
% classificationUI2.m and the documentation

    % Lorentz
    internalNmrSignal = (1./(1+((spektralbereich-signalPosition)/(signalBreite/spektrometerFrequenz)*2).^2));
    % Gauss
    internalNmrSignal = internalNmrSignal*(1-anteilGauss) + exp(-log(2)*((spektralbereich-signalPosition)).^2/((0.5*signalBreite/spektrometerFrequenz)^2)).*anteilGauss;
end