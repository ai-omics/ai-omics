function specs = nmrRead1DList(filelist)
%Wrapper for reading multiple 1D NMR spectra from a file list
%
% part of the AI(omics)n distribution 
% https://gitlab.com/ai-omics/ai-omics
% (c) 2023 by the authors
% distributed under the conditions listed in the file
% classificationUI2.m and the documentation

specs = struct([]);
filelistLength = length(filelist);

reverseStr = '';

for j=1:filelistLength
    path1r = filelist{j};
    pathDir = path1r(1:(end-3));
    if ispc
        pathProcs = [pathDir '\procs'];
    else
        pathProcs = [pathDir '/procs'];
    end
    spec = nmrRead1D(path1r, pathProcs);
    specs = [specs;spec];
    
    percentDone = 100 * j / filelistLength;
	msg = sprintf('Reading: %3.1f percent done', percentDone);
	fprintf([reverseStr, msg]);
	reverseStr = repmat(sprintf('\b'), 1, length(msg));
end
fprintf('\r\n');
end

