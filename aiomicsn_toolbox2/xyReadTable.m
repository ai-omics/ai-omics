function [origin,barcodeData,data,barcodeDataTest,dataTest,originInterval] = xyReadTable(origin,barcodeData,dataOld,barcodeDataTest,dataTestOld,source,originInterval)
% Function for reading generic xy data from Excel spreadsheet
%
% part of the AI(omics)n distribution 
% https://gitlab.com/ai-omics/ai-omics
% (c) 2023 by the authors
% distributed under the conditions listed in the file
% classificationUI2.m and the documentation

pathTable = source{1};
pathTableTest = source{2};
columnBarcode = source{3};
columnData = source{4};
binsCount = source{5};
binsInterval = source{6};
binsExclusuion = source{7};
dataType = source{8};

%% Test dataset available?
pathTableTestTrue = length(pathTableTest) > 0;

%% Simulated dataRaw?
if ismember('Simuliert',pathTable(1))
    simulated = true;
else
    simulated = false;
end

%% Read tables
if ~simulated
[~,~,table] = xlsread(strrep(pathTable,'"',''));
end
if pathTableTestTrue
    [~,~,tableTest] = xlsread(strrep(pathTableTest,'"',''));
end

%% Extract barcodes
if ~simulated
    barcode = barcodeRead(table,columnBarcode);
else
    dataRaw = pathTable{1,2};
    barcode = (1:size(dataRaw,1)-1)';
end
if pathTableTestTrue
    barcodeTest = barcodeRead(tableTest,columnBarcode);
else
    barcodeTest = [];
end

%% Read data
if ~simulated
    axis = cell2mat(table(1,columnData));
    dataRaw = table(2:end,columnData);
    for j = 1:size(dataRaw,1)
        for k = 1:size(dataRaw,2)
            if ~isnumeric(dataRaw{j,k})
                dataRaw{j,k} = NaN;
            end
        end
    end
    dataRaw = cell2mat(dataRaw);
else
    axis = dataRaw(1,:);
    dataRaw = dataRaw(2:size(dataRaw,1),:);
end
if pathTableTestTrue
    dataRawTest = cell2mat(tableTest(2:end,columnData));
end

%% Binning
data = NaN(size(dataRaw,1),binsCount);
if pathTableTestTrue
    dataTest = NaN(size(dataRawTest,1),binsCount);
else
    dataTest = NaN(1,binsCount);
end

if length(binsInterval) > 0
    binsSize = (binsInterval(2)-binsInterval(1))/binsCount;
else
    binsSize = (max(axis)-min(axis))/binsCount;
end
binsAxis = zeros(1,binsCount);
binsList = zeros(2,binsCount);

for j = 1:binsCount
    if length(binsInterval) > 0
        binsList(1,j) = binsSize*(j-1) + binsInterval(1);
        binsList(2,j) = binsSize*j + binsInterval(1);
    else
        binsList(1,j) = binsSize*(j-1) + min(axis);
        binsList(2,j) = binsSize*j + min(axis);
    end
    
    binsAxis(j) = (binsList(1,j)+binsList(2,j))/2;
    
    binsIndex = axis > binsList(1,j) & axis <= binsList(2,j);
    
    for k = 1:size(dataRaw,1)
        data(k,j) = sum(dataRaw(k,binsIndex))/sum(binsIndex);
    end
    if pathTableTestTrue
        for k = 1:size(dataRawTest,1)
            dataTest(k,j) = sum(dataRawTest(k,binsIndex))/sum(binsIndex);
        end
    end
end

%% Apply exclusions
if length(binsExclusuion) > 0
    binsExluded = (binsAxis >= binsExclusuion(:,1)) & (binsAxis <= binsExclusuion(:,2));
    binsExluded = sum(binsExluded,1) > 0;
    
    data = data(:,~binsExluded);
    dataTest = dataTest(:,~binsExluded);
    binsAxis = binsAxis(~binsExluded);
end

%% Save origin
originTemp = cell(3,size(data,2));
originTemp(1,:) = {'XY'};
originTemp(2,:) = {dataType};
originTemp(3,:) = num2cell(binsAxis);

%% Append data
[origin,barcodeData,data,barcodeDataTest,dataTest,originInterval] = dataAppend(origin,originTemp,barcodeData,dataOld,barcode,data,barcodeDataTest,dataTestOld,barcodeTest,dataTest,originInterval);
end

