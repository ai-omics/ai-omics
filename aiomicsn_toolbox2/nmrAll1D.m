function filelist = nmrAll1D(path,experiment,proc)
% Find all 1D nmr spectra under folder specified in path
%
% part of the AI(omics)n distribution 
% https://gitlab.com/ai-omics/ai-omics
% (c) 2023 by the authors
% distributed under the conditions listed in the file
% classificationUI2.m and the documentation

if path(end) == '\'
    path = path(1:(end-1));
end

files = dir([path '\**\1r']);

filelist = cell(length(files),1);
for j = 1:length(files)
    filelist{j} = [files(j).folder '\' files(j).name];
end

filelist = filelist(contains(filelist,strcat(string(experiment),'\pdata\',string(proc))));
end

