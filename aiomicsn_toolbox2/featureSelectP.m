% Feature Selection: calculation of P values
%
% part of the AI(omics)n distribution 
% https://gitlab.com/ai-omics/ai-omics
% (c) 2023 by the authors
% distributed under the conditions listed in the file
% classificationUI2.m and the documentation

function P = featureSelectP(data, groups, methodFeatureSelect)
settingFeatureSelectCompare = 0;
settingFeatureSelectNearestNeighbors = 10;

if isfile('settings.ini')
    settingsIni = textread('settings.ini','%s','commentstyle','shell');  
    settingsIni = strrep(settingsIni,':','');
    settingFeatureSelectCompare = settingsIniRead(settingsIni,settingFeatureSelectCompare);
    settingFeatureSelectNearestNeighbors = settingsIniRead(settingsIni,settingFeatureSelectNearestNeighbors);
end

if methodFeatureSelect == 4   % mrmr    
    % fprintf("Mrmr - feature selection: \r\n");
    [idxMrmr,scoresMrmr] = fscmrmr(data,groups);
    P = scoresMrMr';
elseif methodFeatureSelect == 3  % Chi2
    % fprintf("Chi2 - feature selection: \r\n");
    [idxChi2,scoresChi2] = fscchi2(data,groups);
    P = scoresChi2';
elseif methodFeatureSelect == 2   % relieff   
    % fprintf("Relieff - feature selection: \r\n");
    [idxRelieff,scoresRelieff] = relieff(data,groups,settingFeatureSelectNearestNeighbors);
    P = scoresRelieff';
else %if methodFeatureSelect == 1  % Kruskal Wallis Selection as default
    % fprintf("Kruskal-Wallis - feature selection: \r\n");
    P = zeros(size(data,2),1);
    for j = 1:size(data,2)
        P(j) = kruskalwallis(data(:,j),groups,'off');
    end
end

if settingFeatureSelectCompare == 1
    [idxChi2,scoresChi2] = fscchi2(data,groups);
    fprintf("Chi2 - feature selection: \r\n");
    disp(idxChi2);
    %figure('Name',strcat('Variable selection Chi2 filter'));
    %bar(idxChi2,'r');

    [idxMrmr,scoresMrmr] = fscmrmr(data,groups);
    fprintf("Mrmr - feature selection: \r\n");
    disp(idxMrmr);
    %figure('Name',strcat('Variable selection Mrmr filter'));
    %bar(idxMrmr,'g');

    [idxRelieff,scoresRelieff] = relieff(data,groups,settingFeatureSelectNearestNeighbors);
    fprintf("Relieff - feature selection: \r\n");
    disp(idxRelieff);
    %figure('Name',strcat('Variable selection Relieff filter'));
    %bar(idxRelieff,'b');
end
end