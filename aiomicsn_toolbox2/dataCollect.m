function sources = dataCollect()
% function for collecting all model-relavant data
%
% part of the AI(omics)n distribution 
% https://gitlab.com/ai-omics/ai-omics
% (c) 2024 by the authors
% distributed under the conditions listed in the file
% classificationUI2.m and the documentation
firstRun = true;
sources = {};

while firstRun
    dataType = input('What data should be added? (NMR, NIR, MIR, MS, XY, other)\n', 's');
    switch dataType
        case 'NMR'
            pathTable = input('Path to table with NMR spectra of model dataset:\n', 's');
            pathTableTest = input('Path to table with NMR spectra of test dataset:\n', 's');
            columnBarcode = input('Column number with barcode:\n');
            columnPath = input('Column number with path to NMR spectra:\n');
            experimentNumber = input('Experiment number:\n');
            binsCount = input('Number of bins:\n');
            binsInterval = input('Spectral region to be analyzed:\n');
            binsExclusion = input('Spectral regions to be excluded:\n');
            %pathSpectra = input('Path to NMR spectra of model dataset:\n', 's');
            pathSpectra = '';
            %pathSpectraTest = input('Path to NMR spectra of test dataset:\n', 's');
            pathSpectraTest = '';
            icoshiftBoolean = input('Use icoshift?:\n');
            if icoshiftBoolean
                if input('Use standard intervals for icoshift?:\n')
                    icoshiftInterval = -1;
                else
                    icoshiftInterval = input('Specify icoshift intervals:\n');
                end
            else
                icoshiftInterval = -1;
            end
            pqnBoolean = input('Perform probabilistic quotient normalization?:\n');
            if input('Used optimized binning?:\n')
                optimizedBinning = input('Enter slack factor for optimized binning:\n');
            else
                optimizedBinning = false;
            end
            row = size(sources,1);
            sources{row+1,1} = dataType;
            sources{row+1,2} = {pathTable,pathTableTest,columnBarcode,columnPath,experimentNumber,binsCount,binsInterval,binsExclusion,pathSpectra,pathSpectraTest,icoshiftBoolean,pqnBoolean,icoshiftInterval,optimizedBinning};
        case {'NIR','MIR'}
            pathTable = input('Path to table with IR spectra of model dataset:\n', 's');
            pathTableTest = input('Path to table with IR spectra of test dataset:\n', 's');
            columnBarcode = input('Column number with barcode:\n');
            columnData = input('Column number with IR data:\n');
            if(length(columnData) == 1)
                spectrumType = input('Spectrum type of IR data (e.g. AB):\n', 's');
            else
                spectrumType = 0;
            end
            if(length(columnData) == 1)
                spectrumTypeIndex = input('n-th occurrence of this spectrum type (e.g. 2 or -1 for last occurrence):\n');
            else
                spectrumTypeIndex = 0;
            end
            binsCount = input('Number of bins:\n');
            binsInterval = input('Spectral region to be analyzed:\n');
            binsExclusion = input('Spectral regions to be excluded:\n');
            row = size(sources,1);
            sources{row+1,1} = dataType;
            sources{row+1,2} = {pathTable,pathTableTest,columnBarcode,columnData,binsCount,binsInterval,binsExclusion,spectrumType,spectrumTypeIndex};
        case 'MS'
            pathTable = input('Path to table with MS spectra of model dataset:\n', 's');
            pathTableTest = input('Path to table with MS spectra of test dataset:\n', 's');
            columnBarcode = input('Column number with barcode:\n');
            columnPath = input('Column number with path to MS spectra:\n');
            binsCount = input('Number of bins:\n');
            binsInterval = input('Time range to analyze:\n');
            binsExclusion = input('Time range to exclude:\n');
            binsIntensitiyMin = input('Minimum intensity of bins in percent:\n');
            %pathSpectra = input('Path to MS spectra of model dataset:\n', 's');
            pathSpectra = '';
            %pathSpectraTest = input('Path to MS spectra of test dataset:\n', 's');
            pathSpectraTest = '';
            icoshiftBoolean = input('Use icoshift?:\n');
            pqnBoolean = input('Perform probabilistic quotient normalization?:\n');
            row = size(sources,1);
            sources{row+1,1} = dataType;
            sources{row+1,2} = {pathTable,pathTableTest,columnBarcode,columnPath,binsCount,binsInterval,binsExclusion,binsIntensitiyMin,pathSpectra,pathSpectraTest,icoshiftBoolean,pqnBoolean};
        case 'XY'
            pathTable = input('Path to table with XY data of model dataset:\n', 's');
            pathTableTest = input('Path to table with XY data of test dataset:\n', 's');
            dataTypeSub = input('Data type:\n', 's');
            columnBarcode = input('Column number with barcode:\n');
            columnData = input('Column number with XY data:\n');
            binsCount = input('Number of bins:\n');
            binsInterval = input('Spectral region to be analyzed:\n');
            binsExclusion = input('Spectral regions to be excluded:\n');
            row = size(sources,1);
            sources{row+1,1} = dataType;
            sources{row+1,2} = {pathTable,pathTableTest,columnBarcode,columnData,binsCount,binsInterval,binsExclusion,dataTypeSub};
        case 'other'
            pathTable = input('Path to table with other data for model dataset:\n', 's');
            pathTableTest = input('Path to table with other data for test dataset:\n', 's');
            dataTypeSub = input('Data type:\n', 's');
            columnBarcode = input('Column number with barcode:\n');
            columnData = input('Column number with other data:\n');
            expansionBoolean = input('Expand variables?:\n');
            row = size(sources,1);
            sources{row+1,1} = dataType;
            sources{row+1,2} = {pathTable,pathTableTest,columnBarcode,columnData,dataTypeSub,expansionBoolean};
        otherwise
            
    end
    
    if input('Append additional data?\n') ~= true
        firstRun = false;
        break;
    end
end
end

