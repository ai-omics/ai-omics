% Calculation of Kruskal Wallis P values
%
% part of the AI(omics)n distribution 
% https://gitlab.com/ai-omics/ai-omics
% (c) 2023 by the authors
% distributed under the conditions listed in the file
% classificationUI2.m and the documentation

function P = kruskalWallisP(data, groups)
P = zeros(size(data,2),1);

for j = 1:size(data,2)
    P(j) = kruskalwallis(data(:,j),groups,'off');
end
end