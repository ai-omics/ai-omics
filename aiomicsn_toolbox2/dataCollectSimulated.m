function sources = dataCollectSimulated(spectraSimulated)
% Function for collecting all model-relevant data when using simulated spectra 
%
% part of the AI(omics)n distribution 
% https://gitlab.com/ai-omics/ai-omics
% (c) 2023 by the authors
% distributed under the conditions listed in the file
% classificationUI2.m and the documentation

sources = {};

dataType = input('Welche Daten sollen hinzugefügt werden? (NMR, NIR, MIR)\n', 's');
switch dataType
    case 'NMR'
        pathTableTest = input('Path to table with NMR spectra of test dataset:\n', 's');
        columnBarcode = input('Column number with barcode:\n');
        columnPath = input('Column number with path to NMR spectra:\n');
        experimentNumber = input('Experiment number:\n');
        binsCount = input('Number of bins:\n');
        binsInterval = input('Spectral region to be analyzed:\n');
        binsExclusion = input('Spectral regions to be excluded:\n');
        pathSpectraTest = input('Path to NMR spectra of test dataset:\n', 's');
        icoshiftBoolean = input('Use icoshift?:\n');
        if icoshiftBoolean
            if input('Use standard intervals for icoshift?:\n')
                icoshiftInterval = -1;
            else
                icoshiftInterval = input('Specify icoshift intervals:\n');
            end
        else
            icoshiftInterval = -1;
        end
        pqnBoolean = input('Perform probabilistic quotient normalization?:\n');
        row = size(sources,1);
        sources{row+1,1} = dataType;
        sources{row+1,2} = {'Simulated',pathTableTest,columnBarcode,columnPath,experimentNumber,binsCount,binsInterval,binsExclusion,spectraSimulated,pathSpectraTest,icoshiftBoolean,pqnBoolean,icoshiftInterval};
    case {'NIR','MIR'}
        pathTableTest = input('Path to table with IR spectra of test dataset:\n', 's');
        columnBarcode = input('Column number with barcode:\n');
        spalteDaten = input('Column number with IR data:\n');
        binsCount = input('Number of bins:\n');
        binsInterval = input('Spectral region to be analyzed:\n');
        binsExclusion = input('Spectral regions to be excluded:\n');
        row = size(sources,1);
        sources{row+1,1} = dataType;
        sources{row+1,2} = {{'Simuliert',spectraSimulated},pathTableTest,columnBarcode,spalteDaten,binsCount,binsInterval,binsExclusion};
    otherwise
end
end

