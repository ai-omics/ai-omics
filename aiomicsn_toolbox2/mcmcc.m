function [mcmccResult,varargout] = mcmcc(confusionMatrix,varargin)
% Function for calculation of multiclass Matthews correlation coefficient
%
% part of the AI(omics)n distribution 
% https://gitlab.com/ai-omics/ai-omics
% (c) 2023 by the authors
% distributed under the conditions listed in the file
% classificationUI2.m and the documentation
%
% According to 10.1016/j.compbiolchem.2004.09.006

%% Set parameters
[m,n] = size(confusionMatrix);

if m~=n
    error('mcmcc: matrix has to be square');
end

if length(varargin) > 0
    confusionMatrixStd = varargin{1};
end

%% Dividend
kSum = 0;
if length(varargin) > 0
    kSumStd = 0;
end
for k = 1:n
    lSum = 0;
    if length(varargin) > 0
        lSumStd = 0;
    end
    for l = 1:n
        mSum = 0;
        if length(varargin) > 0
            mSumStd = 0;
        end
        for m = 1:n
            mSum = mSum + confusionMatrix(k,k)*confusionMatrix(l,m) - confusionMatrix(k,l)*confusionMatrix(m,k);
            if length(varargin) > 0
                mSumStd = sqrt(mSumStd^2 + confusionMatrix(l,m)^2*confusionMatrixStd(k,k)^2 + confusionMatrix(k,k)^2*confusionMatrixStd(l,m)^2 + confusionMatrix(m,k)^2*confusionMatrixStd(k,l)^2 + confusionMatrix(k,l)^2*confusionMatrixStd(m,k)^2);
            end
        end
        lSum = lSum + mSum;
        if length(varargin) > 0
            lSumStd = sqrt(lSumStd^2 + mSumStd^2);
        end
    end
    kSum = kSum + lSum;
    if length(varargin) > 0
        kSumStd = sqrt(kSumStd^2 + lSumStd^2);
    end
end

dividend = kSum;
if length(varargin) > 0
    dividendStd = kSumStd;
end

%% Divisor part 1
kSum = 0;
if length(varargin) > 0
    kSumStd = 0;
end
for k = 1:n
    lSum = 0;
    if length(varargin) > 0
        lSumStd = 0;
    end
    for l = 1:n
        lSum = lSum + confusionMatrix(k,l);
        if length(varargin) > 0
            lSumStd = sqrt(lSumStd^2 + confusionMatrixStd(k,l)^2);
        end
    end
    lPrimeSum = 0;
    if length(varargin) > 0
        lPrimeSumStd = 0;
    end
    for lPrime = 1:n
        kPrimeSum = 0;
        if length(varargin) > 0
            kPrimeSumStd = 0;
        end
        for kPrime = setdiff(1:n,k)
            kPrimeSum = kPrimeSum + confusionMatrix(kPrime,lPrime);
            if length(varargin) > 0
                kPrimeSumStd = sqrt(kPrimeSumStd^2 + confusionMatrixStd(kPrime,lPrime)^2);
            end
        end
        lPrimeSum = lPrimeSum + kPrimeSum;
        if length(varargin) > 0
            lPrimeSumStd = sqrt(lPrimeSumStd^2 + kPrimeSumStd^2);
        end
    end
    kSum = kSum + lSum*lPrimeSum;
    if length(varargin) > 0
        kSumStd = sqrt(kSumStd^2 + lPrimeSum^2*lSumStd^2 + lSum^2*lPrimeSumStd^2);
    end
end

divisor1 = sqrt(kSum);
if length(varargin) > 0
    divisor1Std = sqrt((1/(sqrt(kSum)))^2*kSumStd^2);
end

%% Divisor part 2
kSum = 0;
if length(varargin) > 0
    kSumStd = 0;
end
for k = 1:n
    lSum = 0;
    if length(varargin) > 0
        lSumStd = 0;
    end
    for l = 1:n
        lSum = lSum + confusionMatrix(l,k);
        if length(varargin) > 0
            lSumStd = sqrt(lSumStd^2 + confusionMatrixStd(l,k)^2);
        end
    end
    lPrimeSum = 0;
    if length(varargin) > 0
        lPrimeSumStd = 0;
    end
    for lPrime = 1:n
        kPrimeSum = 0;
        if length(varargin) > 0
            kPrimeSumStd = 0;
        end
        for kPrime = setdiff(1:n,k)
            kPrimeSum = kPrimeSum + confusionMatrix(lPrime,kPrime);
            if length(varargin) > 0
                kPrimeSumStd = sqrt(kPrimeSumStd^2 + confusionMatrixStd(lPrime,kPrime)^2);
            end
        end
        lPrimeSum = lPrimeSum + kPrimeSum;
        if length(varargin) > 0
            lPrimeSumStd = sqrt(lPrimeSumStd^2 + kPrimeSumStd^2);
        end
    end
    kSum = kSum + lSum*lPrimeSum;
    if length(varargin) > 0
        kSumStd = sqrt(kSumStd^2 + lPrimeSum^2*lSumStd^2 + lSum^2*lPrimeSumStd^2);
    end
end

divisor2 = sqrt(kSum);
if length(varargin) > 0
    divisor2Std = sqrt((1/sqrt(kSum))^2*kSumStd^2);
end

%% Final result 

mcmccResult = dividend/(divisor1*divisor2);
if length(varargin) > 0
    mcmccStd = sqrt((1/(divisor1*divisor2))^2*dividendStd^2 + (dividend/(divisor1^2*divisor2))^2*divisor1Std^2 + (dividend/(divisor1*divisor2^2))^2*divisor2Std^2);
    varargout{1} = mcmccStd;
end
end