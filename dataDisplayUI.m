%% AI(omics)^n - a versatile tool for assessing data set characteristics, data-fusion and multi-method modelling of large data sets
% Script for displaying raw data
% 
% Developed by Felix Ruell under the supervision of Stephan Schwarzinger
% North Bavarian NMR-Centre at the University of Bayreuth
% Universitaetsstrasse 30, 95447 Bayreuth, Germany
% Contact:  s.schwarzinger@uni-bayreuth.de
%           felix.ruell@uni-bayreuth.de
% With contributions by Peter Kolb, Stefan Bindereif, Simon Steidele,
% Sandra Haupt and kind support by Birk Schuetz (Bruker BioSpin)
%
% License: Creative Commons CC BY-NC-SA; in case of loophole GNU-GPL for non-commercial
% applications only and with the extension that reference must be made to the original work 
% and any resulting work is relicensed alike. 
% For any commercial application a license must be obtained. For this purpose, please contact NBNC@uni-bayreuth.de
%
% The development of the software and the associated demonstration data set was in part supported by funds 
% of the Federal Ministry of Food and Agriculture (BMEL) based on a decision of the Parliament of the Federal Republic of Germany 
% via the Federal Office for Agriculture and Food (BLE) under the innovation support program (Reference number: 2816502414). 

%% Adding toolboxes
addpath('./aiomicsn_toolbox2');

%% Collecting data
fprintf("Collecting data\r\n");
if exist('sourceDisplayNumber') ~= 1
    sourceDisplayNumber = input('Number of the source to be displayed?:\n');
end
if exist('rawDataBoolean') ~= 1
    rawDataBoolean = input('Show raw data? (slow!):\n');
end
if exist('quantileBoolean') ~= 1
    quantileBoolean = input('Show quantiles?:\n');
end
if exist('intervalBoolean') ~= 1
    intervalBoolean = input('Show interval?:\n');
end
if exist('loadingsBoolean') ~= 1
    loadingsBoolean = input('Show loadings?:\n');
end
if exist('binsBoolean') ~= 1
    binsBoolean = input('Show selected bins?:\n');
end
if exist('binsColorBoolean') ~= 1
    binsColorBoolean = input('Show color grading for bins according to importance?:\n');
end
if exist('groups') == 1
    groupsBoolean = true;
else
    groupsBoolean = false;
end

%% Raw or binned data
if rawDataBoolean
    %% Reading data
    fprintf("Reading data\r\n");
    
    icoshiftBoolean = false;
    pqnBoolean = false;
    
    if strcmp(sources{sourceDisplayNumber,1},'NMR')
        sourceDisplay = sources{sourceDisplayNumber,2};
        
        pathTable = sourceDisplay{1};
        columnBarcode = sourceDisplay{3};
        cloumnPath = sourceDisplay{4};
        experimentNumber = sourceDisplay{5};
        binsCount = sourceDisplay{6};
        pathSpectra = sourceDisplay{9};
        
        [~,~,table] = xlsread(strrep(pathTable,'"',''));
        barcodeDataDisplay = barcodeRead(table,columnBarcode);
        
        filelist = filelistGenerate(table(2:end,cloumnPath),pathSpectra,experimentNumber);
        
        fileExists = find(isfile(filelist));
        barcodeDataDisplay = barcodeDataDisplay(fileExists);
        filelist = filelist(fileExists);
        
        spectraDisplay = nmrRead1DList(filelist);
        
        xAxis = nmrPpm1D(spectraDisplay(1));
        dataDisplay = zeros(length(spectraDisplay),length(spectraDisplay(1).Data));
        for k = 1:length(spectraDisplay)
            dataDisplay(k,:) = spectraDisplay(k).Data;
        end
    end
    if strcmp(sources{sourceDisplayNumber,1},'NIR') || strcmp(sources{sourceDisplayNumber,1},'MIR')
        sourceDisplay = sources{sourceDisplayNumber,2};
        
        pathTable = sourceDisplay{1};
        columnBarcode = sourceDisplay{3};
        columnData = sourceDisplay{4};
        binsCount = sourceDisplay{5};
        if length(sourceDisplay) >= 8
            spectrumType = sourceDisplay{8};
        else
            spectrumType = 'AB';
        end
        if length(sourceDisplay) >= 9
            spectrumTypeIndex = sourceDisplay{9};
        else
            spectrumTypeIndex = -1;
        end
        
        [~,~,table] = xlsread(strrep(pathTable,'"',''));
        barcodeDataDisplay = barcodeRead(table,columnBarcode);
        
        if length(columnData) > 1
            xAxis = cell2mat(table(1,columnData));
            dataDisplay = table(2:end,columnData);
            for k = 1:size(dataDisplay,1)
                for l = 1:size(dataDisplay,2)
                    if ~isnumeric(dataDisplay{k,l})
                        dataDisplay{k,l} = NaN;
                    end
                end
            end
            dataDisplay = cell2mat(dataDisplay);
        else
            % Generate filelist
            filelist = table(2:end,columnData);
            
            % Exclude not existing files
            fileExists = find(isfile(filelist));
            barcodeDataDisplay = barcodeDataDisplay(fileExists);
            filelist = filelist(fileExists);
            
            % Read spectra from files
            spectraDisplay = irReadList(filelist, spectrumType, spectrumTypeIndex);
            
            % Get spectra size
            spectraDisplaySize = zeros(size(spectraDisplay,1),1);
            for j = 1:size(spectraDisplay,1)
                spectraDisplaySize(j) = length(spectraDisplay(j).wavenumber);
            end
            
            % Get common spectrum length
            spectraDisplaySizeUnique = tabulate(spectraDisplaySize);
            [~,spectraSizeCommon] = max(spectraDisplaySizeUnique(:,2));
            
            % Exclude spectra off common size
            barcodeDataDisplay = barcodeDataDisplay(spectraDisplaySize == spectraSizeCommon);
            spectraDisplay = spectraDisplay(spectraDisplaySize == spectraSizeCommon);
            
            % Get wave numbers
            xAxis = spectraDisplay(1).wavenumber;
            
            % Combine multiple measurements
            barcodeDataDisplayUnique = tabulate(barcodeDataDisplay);
            barcodeDataDisplayUnique = barcodeDataDisplayUnique(barcodeDataDisplayUnique(:,2)>0,:);
            
            dataDisplay = zeros(size(barcodeDataDisplayUnique,1),length(xAxis));
            
            for j = 1:size(barcodeDataDisplayUnique,1)
                barcodeDataDisplayIndex = find(barcodeDataDisplay == barcodeDataDisplayUnique(j,1));
                for k = 1:length(barcodeDataDisplayIndex)
                    dataDisplay(j,:) = dataDisplay(j,:) + spectraDisplay(barcodeDataDisplayIndex(k)).intensity;
                end
                dataDisplay(j,:) = dataDisplay(j,:)/length(barcodeDataDisplayIndex);
            end
            
            barcodeDataDisplay = barcodeDataDisplayUnique(:,1);
        end
    end
    if strcmp(sources{sourceDisplayNumber,1},'XY')
        sourceDisplay = sources{sourceDisplayNumber,2};
        
        pathTable = sourceDisplay{1};
        columnBarcode = sourceDisplay{3};
        columnData = sourceDisplay{4};
        binsCount = sourceDisplay{5};
        
        [~,~,table] = xlsread(strrep(pathTable,'"',''));
        barcodeDataDisplay = barcodeRead(table,columnBarcode);
        
        xAxis = cell2mat(table(1,columnData));
        dataDisplay = table(2:end,columnData);
        for k = 1:size(dataDisplay,1)
            for l = 1:size(dataDisplay,2)
                if ~isnumeric(dataDisplay{k,l})
                    dataDisplay{k,l} = NaN;
                end
            end
        end
        dataDisplay = cell2mat(dataDisplay);
    end
    if strcmp(sources{sourceDisplayNumber,1},'other')
        sourceDisplay = sources{sourceDisplayNumber,2};
        
        pathTable = sourceDisplay{1};
        columnBarcode = sourceDisplay{3};
        columnData = sourceDisplay{4};
        
        [~,~,table] = xlsread(strrep(pathTable,'"',''));
        barcodeDataDisplay = barcodeRead(table,columnBarcode);
        
        dataDisplay = cell2mat(table(2:end,columnData));
        xAxis = 1:size(dataDisplay,2);
    end
    if strcmp(sources{sourceDisplayNumber,1},'MS')
        sourceDisplay = sources{sourceDisplayNumber,2};
        
        pathTable = sourceDisplay{1};
        columnBarcode = sourceDisplay{3};
        cloumnPath = sourceDisplay{4};
        binsCount = sourceDisplay{5};
        pathSpectra = sourceDisplay{9};
        icoshiftBoolean = sourceDisplay{11};
        pqnBoolean = sourceDisplay{12};
        
        [~,~,table] = xlsread(strrep(pathTable,'"',''));
        barcodeDataDisplay = barcodeRead(table,columnBarcode);
        
        filelist = msFilelistGenerate(table(2:end,cloumnPath),pathSpectra);
        
        fileExists = find(isfile(filelist));
        barcodeDataDisplay = barcodeDataDisplay(fileExists);
        filelist = filelist(fileExists);
        
        spectraDisplay = struct;
        xAxisLengthMax = 0;
        xAxisMinMax = 1000;
        xAxisMaxMin = 0;
        
        for j = 1:size(filelist,1)
            spectrum = csvread(filelist{j},1,0);
            spectraDisplay(j).time = spectrum(:,1);
            if length(spectraDisplay(j).time) > xAxisLengthMax
                xAxisLengthMax = length(spectraDisplay(j).time);
            end
            if xAxisMinMax > max(spectraDisplay(j).time)
                xAxisMinMax = max(spectraDisplay(j).time);
            end
            if xAxisMaxMin < min(spectraDisplay(j).time)
                xAxisMaxMin = min(spectraDisplay(j).time);
            end
            spectraDisplay(j).intensity = spectrum(:,2);
        end
        
        xAchseSchritt = (xAxisMinMax - xAxisMaxMin)/(xAxisLengthMax - 1);
        xAxis = xAxisMaxMin:xAchseSchritt:xAxisMinMax;
        
        dataDisplay = NaN(length(spectraDisplay),xAxisLengthMax);
        for j = 1:length(spectraDisplay)
            dataDisplay(j,:) = interp1(spectraDisplay(j).time,spectraDisplay(j).intensity,xAxis);
        end
    end
    
    %% Cleaning data
    fprintf("Cleaning data\r\n");
    
    if groupsBoolean
        [groupsDisplayCleaned,dataDisplayCleaned,barcodeDataDisplayCleaned] = groupsClean(groupsCleaned,barcodeDataCleaned,dataDisplay,barcodeDataDisplay);
    else
        [dataDisplayCleaned,barcodeDataDisplayCleaned] = dataClean(dataDisplay,barcodeDataDisplay);
    end
    
    barcodeDataDisplayExisits = false(size(barcodeDataDisplayCleaned,1),1);
    for j = 1:size(barcodeDataDisplayCleaned,1)
        barcodeDataDisplayExisits(j) = ismember(barcodeDataDisplayCleaned(j),barcodeDataCleaned);
    end
    
    barcodeDataDisplayCleaned = barcodeDataDisplayCleaned(barcodeDataDisplayExisits);
    dataDisplayCleaned = dataDisplayCleaned(barcodeDataDisplayExisits,:);
    if size(xAxis,1) > 1
        xAxis = xAxis(barcodeDataDisplayExisits,:);
    end
    if groupsBoolean
        groupsDisplayCleaned = groupsDisplayCleaned(barcodeDataDisplayExisits);
    end
    
    %% Apply probabilistic quotient normalization
    if pqnBoolean
        dataDisplayCleanedReference = median(dataDisplayCleaned);
        dataDisplayCleanedQuotients = dataDisplayCleaned./dataDisplayCleanedReference;
        dataDisplayCleanedQuotientsMedian = median(dataDisplayCleanedQuotients,2);
        dataDisplayCleaned = dataDisplayCleaned./dataDisplayCleanedQuotientsMedian;
    end
    
    %% icoshift
    if icoshiftBoolean
        [dataDisplayCleaned,icoshiftIntervals,icoshiftIndexes,icoshiftTargetVector] = icoshift('average',dataDisplayCleaned,34);
    end
else
    %% Read data from bins
    dataDisplayCleaned = dataCleaned(:,originInterval(sourceDisplayNumber,1):originInterval(sourceDisplayNumber,2));
    
    if groupsBoolean
        groupsDisplayCleaned = groupsCleaned;
    end
    
    if isnumeric(origin{3,originInterval(sourceDisplayNumber,1)})
        xAxis = cell2mat(origin(3,originInterval(sourceDisplayNumber,1):originInterval(sourceDisplayNumber,2)));
    else
        xAxis = 1:size(dataDisplayCleaned,2);
    end
end

%% Display spectra
fprintf("Generating grafics\r\n");
figure;
hold on;

fprintf("Legend\r\n");
if groupsBoolean
    groupsTabulate = tabulate(groupsDisplayCleaned);
    [~,groupsOrder] = sort(groupsTabulate(:,1));
    groupsTabulate = groupsTabulate(groupsOrder,:);
    groupsColors = lines(size(groupsTabulate,1));
    
    for j = 1:size(groupsTabulate,1)
        plot(NaN,NaN,'ko','MarkerFaceColor',groupsColors(j,:));
    end
end

fprintf("Bins\r\n");
if binsBoolean
    if isfield(stats,'kruskalPSortierung')
        stats.featureSelectionSorting = stats.kruskalPSortierung;
    end
    if isfield(stats,'kruskalPSorting')
        stats.featureSelectionSorting = stats.kruskalPSorting;
    end
    if isfield(stats,'featureSelectionSorting')
        binsSelected = stats.featureSelectionSorting((stats.featureSelectionSorting >= originInterval(sourceDisplayNumber,1)) & (stats.featureSelectionSorting <= originInterval(sourceDisplayNumber,2)));
    elseif isfield(stats,'selectedBins')
        binsSelected = stats.selectedBins((stats.selectedBins >= originInterval(sourceDisplayNumber,1)) & (stats.selectedBins <= originInterval(sourceDisplayNumber,2)));
    else
        error('Wrong stats format');
        quit;
    end
    if isnumeric(origin{3,originInterval(sourceDisplayNumber,1)})
        binsDelta = origin{3,originInterval(sourceDisplayNumber,1)+1} - origin{3,originInterval(sourceDisplayNumber,1)};
    else
        binsDelta = 1;
    end
    
    dataDisplayCleanedMin = min(min(dataDisplayCleaned));
    dataDisplayCleanedMax = max(max(dataDisplayCleaned));
    
    binsColors = zeros(length(binsSelected),3);
    binsColors(:,1) = linspace(1,0,length(binsSelected));
    binsColors(:,3) = linspace(0,1,length(binsSelected));
    
    for j = 1:length(binsSelected)
        if isnumeric(origin{3,originInterval(sourceDisplayNumber,1)})
            xAxisPosition = origin{3,binsSelected(j)};
        else
            xAxisPosition = binsSelected(j)-originInterval(sourceDisplayNumber,1)+1;
        end
        if isfield(stats,'featureSelectionSorting') && binsColorBoolean
            patch([xAxisPosition-binsDelta/2 xAxisPosition+binsDelta/2 xAxisPosition+binsDelta/2 xAxisPosition-binsDelta/2],...
                [dataDisplayCleanedMin dataDisplayCleanedMin dataDisplayCleanedMax dataDisplayCleanedMax],...
                binsColors(j,:),'EdgeColor','none','LineStyle','none','FaceAlpha',0.3);
            colormap(binsColors);
            binsColorbar = colorbar('Direction','Reverse','Ticks',[0,1],'TickLabels',{'Higher','Lower'});
            binsColorbar.Label.String = 'Importance';
        else
            patch([xAxisPosition-binsDelta/2 xAxisPosition+binsDelta/2 xAxisPosition+binsDelta/2 xAxisPosition-binsDelta/2],...
                [dataDisplayCleanedMin dataDisplayCleanedMin dataDisplayCleanedMax dataDisplayCleanedMax],...
                [0 1 1],'EdgeColor','none','LineStyle','none','FaceAlpha',0.3);
        end
    end
end

fprintf("Spectra\r\n");
if groupsBoolean    
    for j = 1:size(groupsTabulate,1)
        k = ismember(groupsDisplayCleaned,groupsTabulate(j,1));
        if size(xAxis,1) > 1
            l = k;
        else
            l = 1;
        end
        if quantileBoolean || intervalBoolean
            plotQuantile(xAxis(l,:),dataDisplayCleaned(k,:),'Color',groupsColors(j,:),'Mean',true,'Quantile',quantileBoolean,'Interval',intervalBoolean);
        else
            plot(xAxis(l,:)',dataDisplayCleaned(k,:)','Color',groupsColors(j,:));
        end
    end
else
    if quantileBoolean || intervalBoolean
            plotQuantile(xAxis,dataDisplayCleaned,'Color',[1 0 0],'Mean',true,'Quantile',quantileBoolean,'Interval',intervalBoolean);
    else
        plot(xAxis',dataDisplayCleaned','r');
    end
end

fprintf("Loadings\r\n");
if loadingsBoolean && isfield(stats,'loadingsX')
    if isnumeric(origin{3,originInterval(sourceDisplayNumber,1)})
        xAxisPosition = cell2mat(origin(3,originInterval(sourceDisplayNumber,1):originInterval(sourceDisplayNumber,2)));
    else
        xAxisPosition = xAxis;
    end
    
    if isfield(stats,'methods')
        loadingsX = stats.loadingsX';
    else
        loadingsX = stats.loadingsX;
    end
    
    loadingsXFull = zeros(size(loadingsX,1),originInterval(sourceDisplayNumber,2)-originInterval(sourceDisplayNumber,1)+1);
    
    if isfield(stats,'featureSelectionSorting') | isfield(stats,'selectedBins')
        if isfield(stats,'featureSelectionSorting')
            binsSelected = stats.featureSelectionSorting((stats.featureSelectionSorting >= originInterval(sourceDisplayNumber,1)) & (stats.featureSelectionSorting <= originInterval(sourceDisplayNumber,2)));
        elseif isfield(stats,'selectedBins')
            binsSelected = stats.selectedBins((stats.selectedBins >= originInterval(sourceDisplayNumber,1)) & (stats.selectedBins <= originInterval(sourceDisplayNumber,2)));
        else
            error('Wrong stats format');
            quit;
        end
        
        loadingsXFull(:,binsSelected-originInterval(sourceDisplayNumber,1)+1) = loadingsX(:,(binsSelected >= originInterval(sourceDisplayNumber,1)) & (binsSelected <= originInterval(sourceDisplayNumber,2)));
    else
        loadingsXFull = loadingsX(:,originInterval(sourceDisplayNumber,1):originInterval(sourceDisplayNumber,2));
    end
    
    loadingsXMax = max(max(loadingsXFull));
    
    plot(xAxisPosition,loadingsXFull(1:3,:)/loadingsXMax*max(max(dataDisplayCleaned)));
end

if ~strcmp(sources{sourceDisplayNumber,1},{'MS', 'XY', 'other'})
    set(gca,'XDir','reverse');
end

if groupsBoolean    
    legend(groupsTabulate(:,1));
end
    
hold off;

fprintf("Done\r\n");
