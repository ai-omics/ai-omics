%% AI(omics)^n - a versatile tool for assessing data set characteristics, data-fusion and multi-method modelling of large data sets
% Script for regressions using PLS
% 
% Developed by Felix Ruell under the supervision of Stephan Schwarzinger
% North Bavarian NMR-Centre at the University of Bayreuth
% Universitaetsstrasse 30, 95447 Bayreuth, Germany
% Contact:  s.schwarzinger@uni-bayreuth.de
%           felix.ruell@uni-bayreuth.de
% With contributions by Peter Kolb, Stefan Bindereif, Simon Steidele,
% Sandra Haupt and kind support by Birk Schuetz (Bruker BioSpin).
% (c) 2023 by the authors.
%
% License: Creative Commons CC BY-NC-SA; in case of loophole GNU-GPL for non-commercial
% applications only and with the extension that reference must be made to the original work 
% and any resulting work is relicensed alike. 
% For any commercial application a license must be obtained. For this purpose, please contact NBNC@uni-bayreuth.de
%
% The development of the software and the associated demonstration data set was in part supported by funds 
% of the Federal Ministry of Food and Agriculture (BMEL) based on a decision of the Parliament of the Federal Republic of Germany 
% via the Federal Office for Agriculture and Food (BLE) under the innovation support program (Reference number: 2816502414). 

%% Adding toolboxes
addpath('./aiomicsn_toolbox2');

%% Collecting data
fprintf("Collecting data\r\n");
if exist('firstRun') ~= 1
    firstRun = true;
end
if exist('spectraSimulated') ~= 1
    simulated = false;
else
    simulated = true;
end
if ~simulated
    if exist('pathTableValues') ~= 1
        pathTableValues = input('Path to table with regression values:\n', 's');
    end
    if exist('columnValues') ~= 1
        columnValues = input('Column number of regression values:\n');
    end
    if exist('columnValuesBarcode') ~= 1
        columnValuesBarcode = input('Column number with barcode for regression values:\n');
    end
end
if firstRun
    if simulated
        sources = dataCollectSimulated(spectraSimulated);
    else
        sources = dataCollect();
    end
    firstRun = false;
end

%% Reading data
fprintf("Reading data\r\n");
if simulated
    barcodeValues = (1:size(values))';
    if isnumeric(values(1))
        values = string(num2str(values));
    end
else
    [barcodeValues,values] = groupsRead(pathTableValues,columnValues,columnValuesBarcode);
end
[origin,barcodeData,data,barcodeDataTest,dataTest,originInterval] = dataRead(sources);

%% Cleaning data
fprintf("Cleaning data\r\n");
[valuesCleaned,dataCleaned,barcodeDataCleaned] = groupsClean(values,barcodeValues,data,barcodeData);
valuesCleaned = str2double(valuesCleaned);
[dataTestCleaned,barcodeDataTestCleaned] = dataClean(dataTest,barcodeDataTest);

%% Loop for removal of outliers
continueBoolean = true;
clearvars statsOld;

while continueBoolean
    %% PLS
    fprintf("Performing regression\r\n");
    if simulated
        simulatedParameter = originSimulation;
    else
        simulatedParameter = false;
    end
    [stats,valuesTest,~,valuesTestLinear] = regressionPls(dataCleaned,valuesCleaned,dataTestCleaned,'plot',true,'featureSelect',true,'simulated',simulatedParameter); %Um feature Selection auszuschalten, einfach ,'featureSelect' aus dieser Zeile entfernen.
            
    %% Output
    fprintf("Generating output format\r\n");
    if size(dataTestCleaned,1) > 0
        output = cell(size(dataTestCleaned,1)+1,3);
        output(1,:) = {'Barcode','Predicted value by PLS','Linear adjusted value'};
        output(2:end,1) = num2cell(barcodeDataTestCleaned);
        output(2:end,2) = num2cell(valuesTest);
        output(2:end,3) = num2cell(valuesTestLinear);
    end
    
    %% Removing outliers
    if input('Remove outliers?\n')
        stats.barcodeOutliers = barcodeDataCleaned(stats.outliers);
        
        if ~exist('statsOld')
            statsOld = stats;
        else
            statsOld = [statsOld;stats];
        end
        
        valuesCleaned = valuesCleaned(setxor((1:size(barcodeDataCleaned,1))',stats.outliers));
        if input('Predict outliers?\n')
            dataTestCleaned = [dataTestCleaned;dataCleaned(stats.outliers,:)];
            barcodeDataTestCleaned = [barcodeDataTestCleaned;barcodeDataCleaned(stats.outliers)];
        end
        dataCleaned = dataCleaned(setxor((1:size(barcodeDataCleaned,1))',stats.outliers),:);
        barcodeDataCleaned = barcodeDataCleaned(setxor((1:size(barcodeDataCleaned,1))',stats.outliers),:);
    else
        continueBoolean = false;
    end
end

%% Save
if input('Save?\n')
    saveName = input('File name?\n','s');
    savePath = input('Path to parent directory?\n','s');
    
    if isfolder(savePath)
        save(strcat(savePath,'\',saveName,'.mat'))
        figs = findobj(allchild(0), 'flat', 'Type', 'figure');
        for j = 1:length(figs)
            figHandle = figs(j);
            figName   = get(figHandle, 'Name');
            savefig(figHandle, strcat(savePath,'\',saveName,'_',figName,'.fig'));
        end
    end
end